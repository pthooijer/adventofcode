package nl.pthooijer.util;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class JoinIterator<T, A, R> implements Iterator<R> {

    private final Iterator<T> delegate;
    private final Predicate<? super T> separatorTest;
    private final SeparatorPolicy separatorPolicy;
    private final Collector<? super T, A, ? extends R> collector;

    private T joinWithNextElement = null;

    public JoinIterator(Iterator<T> delegate,
                        Predicate<? super T> separatorTest,
                        SeparatorPolicy separatorPolicy,
                        Collector<? super T, A, ? extends R> collector) {
        this.delegate = delegate;
        this.separatorTest = separatorTest;
        this.separatorPolicy = separatorPolicy;
        this.collector = collector;
    }

    @Override
    public boolean hasNext() {
        return delegate.hasNext();
    }

    @Override
    public R next() {
        A aContainer = collector.supplier().get();
        if (joinWithNextElement != null) {
            collector.accumulator().accept(aContainer, joinWithNextElement);
            joinWithNextElement = null;
        }

        whileLoop:
        while (delegate.hasNext()) {
            T t = delegate.next();
            if (!separatorTest.test(t)) {
                collector.accumulator().accept(aContainer, t);
            } else {
                switch (separatorPolicy) {
                    case JOIN_WITH_PREVIOUS:
                        collector.accumulator().accept(aContainer, t);
                        break whileLoop;
                    case JOIN_WITH_NEXT:
                        joinWithNextElement = t;
                        break whileLoop;
                    case DROP:
                        break whileLoop;
                }
            }

        }

        return collector.finisher().apply(aContainer);
    }

}
