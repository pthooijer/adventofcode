package nl.pthooijer.util;

import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public final class ArrayUtils {

    public static <T> boolean isInBounds(T[][] array, int i, int j) {
        return i >= 0 && i < array.length &&
                j >= 0 && j < array[0].length;
    }

    public static boolean isInBounds(char[][] array, int i, int j) {
        return i >= 0 && i < array.length &&
                j >= 0 && j < array[0].length;
    }

    public static boolean isInBounds(long[][] array, int i, int j) {
        return i >= 0 && i < array.length &&
                j >= 0 && j < array[0].length;
    }

    public static boolean isInBounds(int[][] array, int i, int j) {
        return i >= 0 && i < array.length &&
                j >= 0 && j < array[0].length;
    }

    public static boolean isInBounds(char[][][] array, int x, int y, int z) {
        return x >= 0 && x < array.length &&
                y >= 0 && y < array[0].length &&
                z >= 0 && z < array[0][0].length;
    }

    public static boolean isInBounds(char[][][][] array, int x, int y, int z, int w) {
        return x >= 0 && x < array.length &&
                y >= 0 && y < array[0].length &&
                z >= 0 && z < array[0][0].length &&
                w >= 0 && w < array[0][0][0].length;
    }

    public static int countChars(char[] input, char c) {
        int count = 0;
        for (char arrC : input) {
            if (arrC == c) {
                count++;
            }
        }
        return count;
    }

    public static char[][] transpose(char[][] original) {
        char[][] result = new char[original[0].length][];
        for (int i = 0; i < original[0].length; i++) {
            result[i] = new char[original.length];
            for (int j = 0; j < original.length; j++) {
                result[i][j] = original[j][i];
            }
        }
        return result;
    }

    public static char[][] rotateCCW(char[][] original) {
        int iL = original[0].length;
        int jL = original.length;
        char[][] result = new char[iL][];
        for (int i = 0; i < iL; i++) {
            result[i] = new char[jL];
            for (int j = 0; j < jL; j++) {
                result[i][j] = original[j][iL - 1 - i];
            }
        }
        return result;
    }

    public static char[][] rotateCW(char[][] original) {
        int iL = original[0].length;
        int jL = original.length;
        char[][] result = new char[iL][];
        for (int i = 0; i < iL; i++) {
            result[i] = new char[jL];
            for (int j = 0; j < jL; j++) {
                result[i][j] = original[jL - 1 - j][i];
            }
        }
        return result;
    }

    public static int countChars(char[][] input, char c) {
        int count = 0;
        for (char[] chars : input) {
            for (char arrC : chars) {
                if (arrC == c) {
                    count++;
                }
            }
        }
        return count;
    }

    public static Stream<Character> stream(char[] array) {
        return IntStream.range(0, array.length)
                .mapToObj(i -> array[i]);
    }
}
