package nl.pthooijer.util;

import lombok.Getter;
import nl.pthooijer.util.point.Point;

import java.util.Arrays;

@Getter
public enum Direction {
    NORTH(new Point(-1, 0)),
    EAST(new Point(0, 1)),
    SOUTH(new Point(1, 0)),
    WEST(new Point(0, -1));

    public final Point move;

    Direction(Point move) {
        this.move = move;
    }

    public Direction getOpposite() {
        return switch (this) {
            case NORTH -> SOUTH;
            case EAST -> WEST;
            case SOUTH -> NORTH;
            case WEST -> EAST;
        };
    }

    public static Direction fromMove(Point p) {
        return Arrays.stream(Direction.values())
                .filter(dir -> dir.getMove().equals(p))
                .findFirst().orElseThrow();
    }
}
