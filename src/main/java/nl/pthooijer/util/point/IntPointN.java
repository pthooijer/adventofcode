package nl.pthooijer.util.point;

import java.util.Arrays;

public interface IntPointN<P extends IntPointN<P>> extends Comparable<P> {

    int[] asArray();

    P plus(P other);

    P minus(P other);

    P times(int amount);

    P times(P other);

    P floorMod(P other);

    default double magnitude() {
        double r2norm = Arrays.stream(asArray())
                .mapToDouble(i -> i * i)
                .sum();
        return Math.sqrt(r2norm);
    }

    default int taxiMagnitude() {
        return Arrays.stream(asArray())
                .map(Math::abs)
                .sum();
    }

    default int chessMagnitude() {
        return Arrays.stream(asArray())
                .map(Math::abs)
                .max()
                .orElseThrow();
    }

    @Override
    default int compareTo(P o) {
        int[] thisArr = asArray();
        int[] otherArr = o.asArray();
        for (int i = 0; i < thisArr.length; i++) {
            int result = Integer.compare(thisArr[i], otherArr[i]);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
}
