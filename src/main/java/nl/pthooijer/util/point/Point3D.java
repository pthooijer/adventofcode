package nl.pthooijer.util.point;

import lombok.Value;

@Value
public class Point3D implements IntPointN<Point3D>, Comparable<Point3D> {
    public int x;
    public int y;
    public int z;

    @Override
    public int[] asArray() {
        return new int[]{x, y, z};
    }

    @Override
    public Point3D plus(Point3D other) {
        return new Point3D(this.x + other.x, this.y + other.y, this.z + other.z);
    }

    @Override
    public Point3D minus(Point3D other) {
        return new Point3D(this.x - other.x, this.y - other.y, this.z - other.z);
    }

    @Override
    public Point3D times(int amount) {
        return new Point3D(this.x * amount, this.y * amount, this.z * amount);
    }

    @Override
    public Point3D times(Point3D other) {
        return new Point3D(this.x * other.x, this.y * other.y, this.z * other.z);
    }

    @Override
    public Point3D floorMod(Point3D other) {
        return new Point3D(Math.floorMod(this.x, other.x), Math.floorMod(this.y, other.y), Math.floorMod(this.z, other.z));
    }

    @Override
    public String toString() {
        return x + "," + y + "," + z;
    }
}
