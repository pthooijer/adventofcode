package nl.pthooijer.util.point;

import lombok.Value;

@Value
public class Point implements IntPointN<Point> {
    public int x;
    public int y;

    @Override
    public int[] asArray() {
        return new int[]{x, y};
    }

    @Override
    public Point plus(Point other) {
        return new Point(this.x + other.x, this.y + other.y);
    }

    @Override
    public Point minus(Point other) {
        return new Point(this.x - other.x, this.y - other.y);
    }

    @Override
    public Point times(int amount) {
        return new Point(this.x * amount, this.y * amount);
    }

    @Override
    public Point times(Point other) {
        return new Point(this.x * other.x, this.y * other.y);
    }

    @Override
    public Point floorMod(Point other) {
        return new Point(Math.floorMod(this.x, other.x), Math.floorMod(this.y, other.y));
    }

    @Override
    public String toString() {
        return x + "," + y;
    }
}
