package nl.pthooijer.util.functional;

public interface IntTriFunction<T> {
    T apply(int a, int b, int c);
}
