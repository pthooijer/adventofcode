package nl.pthooijer.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public abstract class AStarPathfinding<N> {

    public abstract boolean isGoal(N node);

    public record NeighbourCost<N>(N neighbour, long cost) {
    }

    public abstract Collection<NeighbourCost<N>> getNeighboursAndCost(N node);

    /**
     * The heuristic to the goal. If this always returns 0, then this is a Dijkstra's algorithm
     *
     * @return Estimated cost from fromNode to goal. It should never overestimate
     */
    public long heuristicToGoal(N fromNode) {
        return 0L;
    }

    public Result<N> aStartAlgorithm(N start) {
        Map<N, N> cameFrom = new HashMap<>();
        Map<N, Long> gScore = new HashMap<>(); // default Long.MAX;
        Map<N, Long> fScore = new HashMap<>();
        PriorityQueue<N> openSet = new PriorityQueue<>(Comparator.comparingLong(fScore::get)); //lowest fScore first

        gScore.put(start, 0L);
        fScore.put(start, heuristicToGoal(start));
        openSet.add(start);

        while (!openSet.isEmpty()) {
            N current = openSet.remove();
            if (isGoal(current)) {
                List<N> totalPath = reconstructPath(cameFrom, current);
                long cost = gScore.get(current);
                return new Result<>(totalPath, cost);
            }

            for (NeighbourCost<N> neighbourCost : getNeighboursAndCost(current)) {
                N neighbour = neighbourCost.neighbour;
                long cost = neighbourCost.cost;
                long tentativeGScore = gScore.get(current) + cost;
                if (tentativeGScore < gScore.getOrDefault(neighbour, Long.MAX_VALUE)) {
                    cameFrom.put(neighbour, current);
                    gScore.put(neighbour, tentativeGScore);
                    fScore.put(neighbour, tentativeGScore + heuristicToGoal(neighbour));
                    if (!openSet.contains(neighbour)) {
                        openSet.add(neighbour);
                    }
                }
            }
        }
        return null;
    }

    private List<N> reconstructPath(Map<N, N> cameFrom, N target) {
        LinkedList<N> totalPath = new LinkedList<>();
        totalPath.addFirst(target);

        N current = target;
        while (cameFrom.containsKey(current)) {
            current = cameFrom.get(current);
            totalPath.addFirst(current);
        }
        return new ArrayList<>(totalPath);
    }

    public record Result<N>(List<N> path, long cost) {
    }

}
