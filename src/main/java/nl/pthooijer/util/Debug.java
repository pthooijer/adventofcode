package nl.pthooijer.util;

import com.google.common.base.Stopwatch;

import java.util.function.Supplier;

public final class Debug {

    private Debug() {
        // hidden construtor
    }

    public static String toString(char[][] arr) {
        StringBuilder builder = new StringBuilder(arr.length * (arr[0].length + 1));
        for (char[] arr1 : arr) {
            for (char a : arr1) {
                builder.append(a == 0 ? '?' : a);
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    public static String toStringTransposed(char[][] arr) {
        StringBuilder builder = new StringBuilder(arr.length * (arr[0].length + 1));
        for (int i=0; i< arr[0].length; i++) {
            for (int j=0; j<arr.length; j++) {
                char a = arr[j][i];
                builder.append(a == 0 ? '?' : a);
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    public static String toString(long[][] arr) {
        StringBuilder builder = new StringBuilder(arr.length * (arr[0].length + 1));
        for (long[] arr1 : arr) {
            for (long a : arr1) {
                builder.append(a);
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    public static String toString(int[][] arr) {
        StringBuilder builder = new StringBuilder(arr.length * (arr[0].length + 1));
        for (int[] arr1 : arr) {
            for (int a : arr1) {
                builder.append(a);
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    public static void measureAndPrint(Supplier<String> command) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String result = command.get();
        System.out.println(stopwatch.elapsed() + "\t" + result);
    }

}
