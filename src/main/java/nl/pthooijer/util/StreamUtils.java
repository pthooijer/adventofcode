package nl.pthooijer.util;

import java.util.function.BinaryOperator;
import java.util.stream.Collector;

public final class StreamUtils {

    public static <T> BinaryOperator<T> throwingMerger() {
        return (t1, t2) -> {
            throw new UnsupportedOperationException("Merging not supported");
        };
    }

    public static Collector<Integer, ?, String> joiningCodePoint() {
        return Collector.of(
                StringBuilder::new,
                StringBuilder::appendCodePoint,
                StringBuilder::append,
                StringBuilder::toString);
    }
}
