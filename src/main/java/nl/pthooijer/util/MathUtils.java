package nl.pthooijer.util;

import com.google.common.math.LongMath;

import java.util.Arrays;

public class MathUtils {

    // Formula from https://en.wikipedia.org/wiki/Least_common_multiple#Using_the_greatest_common_divisor
    public static long lcm(long a, long b) {
        return LongMath.checkedMultiply(Math.abs(a), (Math.abs(b) / LongMath.gcd(a, b))); // |a| * (|b| / gcd(a, b))
    }

    public static long gcd(long... longs) {
        if (longs.length == 0) {
            throw new IllegalArgumentException();
        }
        return Arrays.stream(longs)
                .reduce(LongMath::gcd) // works because gcd(a, b, c) = gcd(gcd(a, b), c) = gcd(a, gcd(b, c))
                .orElseThrow();
    }

    public static long lcm(long... longs) {
        if (longs.length == 0) {
            throw new IllegalArgumentException();
        }
        return Arrays.stream(longs)
                .reduce(MathUtils::lcm)
                .orElseThrow();
    }

}
