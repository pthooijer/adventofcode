package nl.pthooijer.util;

import java.io.*;
import java.util.*;
import java.util.stream.*;

public final class ResourceHelper {

    public static BufferedReader getReader(String resource) {
        return new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(ResourceHelper.class.getClassLoader().getResourceAsStream(resource))));
    }

    public static Stream<String> getLinesStream(String resource) {
        return getReader(resource).lines();
    }

    public static List<String> getLinesList(String resource) {
        return getLinesStream(resource).collect(Collectors.toList());
    }

    public static Iterable<String> getLinesIt(String resource) {
        return () -> getLinesStream(resource).iterator();
    }

    public static LongStream getLongsStream(String resource) {
        return getLinesStream(resource).mapToLong(Long::parseLong);
    }

    public static List<Long> getLongsList(String resource) {
        return getLongsStream(resource).boxed().collect(Collectors.toList());
    }

    public static long[] getLongsArray(String resource) {
        return getLongsStream(resource).toArray();
    }

    public static long[][] getLongs2dArray(String resource) {
        return getLinesStream(resource)
                .map(s -> s.chars()
                        .mapToLong(i -> i - '0')
                        .toArray())
                .toArray(long[][]::new);
    }

    public static PrimitiveIterator.OfLong getLongIt(String resource) {
        return getLongsStream(resource).iterator();
    }

    public static char[][] getChars2dArray(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(String::toCharArray)
                .toArray(char[][]::new);
    }
}
