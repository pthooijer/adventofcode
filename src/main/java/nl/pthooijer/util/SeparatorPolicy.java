package nl.pthooijer.util;

public enum SeparatorPolicy {
    JOIN_WITH_PREVIOUS,
    JOIN_WITH_NEXT,
    DROP
}
