package nl.pthooijer.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    private static final Map<String, Pattern> PATTERN_CACHE = new HashMap<>();

    public static Optional<MatchResult> parseRegex(String s, String regex) {
        Pattern pattern = PATTERN_CACHE.computeIfAbsent(regex, Pattern::compile);
        return parseRegex(s, pattern);
    }

    public static Optional<MatchResult> parseRegex(String s, Pattern pattern) {
        Matcher matcher = pattern.matcher(s);
        return matcher.matches() ? Optional.of(matcher.toMatchResult()) : Optional.empty();
    }

}
