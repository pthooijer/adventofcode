package nl.pthooijer.aoc2021.day19;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import lombok.Value;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.point.Point3D;
import nl.pthooijer.util.SeparatorPolicy;
import nl.pthooijer.util.functional.IntTriFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static nl.pthooijer.util.ResourceHelper.getLinesStream;

public class Day19 {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');
    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    public static void main(String[] args) {
        String input = "aoc/2021/day/19/input.txt";
        String sample1 = "aoc/2021/day/19/sample-input1.txt";
        System.out.println("Part 1 and 2 sample1 answer: " + part1And2(sample1));
        System.out.println("Part 1 and 2 answer: " + part1And2(input));
    }

    private static String part1And2(String resource) {
        List<Scan> scanList = parseInput(resource);

        Set<Point3D> totalScan = new HashSet<>(scanList.get(0).pointsSet); // Start with scan 0
        Map<Integer, Point3D> correctlyAlignedScans = new HashMap<>();
        correctlyAlignedScans.put(0, new Point3D(0, 0, 0));

        while (correctlyAlignedScans.size() != scanList.size()) {

            mapLoop:
            for (Integer i : correctlyAlignedScans.keySet()) {
                Scan scan1 = scanList.get(i);

                for (int j = 0; j < scanList.size(); j++) {
                    if (!correctlyAlignedScans.containsKey(j)) {
                        Scan scan2 = scanList.get(j);

                        OverlapResult overlapResult = doOverlap(scan1, scan2);
                        if (overlapResult != null) {
                            // Add to totalScan
                            totalScan.addAll(overlapResult.tfPoints2);

                            // Align the not yet aligned scan in the scanList:
                            Scan rotatedScan2 = new Scan(scan2.id, overlapResult.tfPoints2);
                            scanList.set(j, rotatedScan2);
                            correctlyAlignedScans.put(j, overlapResult.scannerLocation);
                            break mapLoop; // just to be sure, because we edited the map and the scanList
                        }
                    }
                }
            }
        }

        long part1Answer = totalScan.size();

        long part2Answer = correctlyAlignedScans.values().stream()
                .flatMapToLong(p1 -> correctlyAlignedScans.values().stream()
                        .mapToLong(p2 -> Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y) + Math.abs(p1.z - p2.z)))
                .max().orElseThrow();

        return part1Answer + ", " + part2Answer;
    }

    private static OverlapResult doOverlap(Scan scan1, Scan scan2) {
        // Shift all points of scan2 to each (max size() - 12) point of scan1, so at least 1 overlaps
        for (Orientation or : Orientation.values()) {
            List<Point3D> rotatedPoints2 = scan2.pointsList.stream()
                    .map(point -> or.map.apply(point))
                    .collect(Collectors.toList());

            for (Point3D firstPoint2 : rotatedPoints2) {
                for (int i = 0; i < scan1.pointsList.size() - 11; i++) { // -11 because no need to check if the first size - 12 don't match
                    Point3D p1 = scan1.pointsList.get(i);

                    List<Point3D> tfPoints2 = new ArrayList<>(rotatedPoints2.size());

                    int xShift = p1.x - firstPoint2.x;
                    int yShift = p1.y - firstPoint2.y;
                    int zShift = p1.z - firstPoint2.z;

                    long possibleHits = scan2.pointsList.size(); // Start with all hit, then remove all misses
                    for (Point3D p2 : rotatedPoints2) {
                        Point3D shiftedP2 = new Point3D(p2.x + xShift, p2.y + yShift, p2.z + zShift);
                        if (!scan1.pointsSet.contains(shiftedP2)) {
                            possibleHits--;
                            if (possibleHits < 12) {
                                break; // This rotated scan is impossible to fit if less than 12 points are possible to hit
                            }
                        }
                        tfPoints2.add(shiftedP2);
                    }

                    if (possibleHits >= 12) {
                        Point3D scannerLocation = new Point3D(xShift, yShift, zShift);
                        return new OverlapResult(tfPoints2, scannerLocation);
                    }
                }
            }
        }

        return null;
    }

    // Parsing

    public static List<Scan> parseInput(String resource) {
        JoinIterator<String, ?, Scan> joinIterator = new JoinIterator<>(
                getLinesStream(resource).iterator(),
                String::isEmpty,
                SeparatorPolicy.DROP,
                Collectors.collectingAndThen(Collectors.toList(), Day19::parseScan));

        return Streams.stream(joinIterator).collect(Collectors.toList());
    }

    public static Scan parseScan(List<String> block) {
        int id = Integer.parseInt(SPACE_SPLITTER.splitToList(block.get(0)).get(2));
        List<Point3D> points = block.stream()
                .skip(1)
                .map(line -> Streams.stream(COMMA_SPLITTER.split(line))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList()))
                .map(list -> new Point3D(list.get(0), list.get(1), list.get(2)))
                .collect(Collectors.toList());

        return new Scan(id, points);
    }

    // Classes

    @Value
    private static class Scan {
        int id;
        List<Point3D> pointsList; // for iteration
        Set<Point3D> pointsSet; // for .contains()

        public Scan(int id, List<Point3D> points) {
            this.id = id;
            this.pointsList = points;
            this.pointsSet = new HashSet<>(points);
        }
    }

    @Value
    private static class OverlapResult {
        List<Point3D> tfPoints2;
        Point3D scannerLocation;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private enum Orientation {
        // THe 6 directions to face
        IDENTITY(p -> p),
        ROTY0((x, y, z) -> new Point3D(z, y, -x)),
        ROTZ0((x, y, z) -> new Point3D(-y, x, z)),
        CROTY0((x, y, z) -> new Point3D(-z, y, x)),
        CROTZ0((x, y, z) -> new Point3D(y, -x, z)),
        OP0(ROTY0, ROTY0),
        // Each have 4 rotations (doing nothing already in the first block)
        // IDENTITY:
        ROTX((x, y, z) -> new Point3D(x, -z, y)),
        OPPX(ROTX, ROTX),
        CROTX((x, y, z) -> new Point3D(x, z, -y)),
        // OPPOSITE:
        OP1(OP0, ROTX),
        OP2(OP0, OPPX),
        OP3(OP0, CROTX),
        // ROTY:
        ROTY1(ROTY0, ROTZ0),
        ROTY2(ROTY0, ROTZ0, ROTZ0),
        ROTY3(ROTY0, CROTZ0),
        // CROTY:
        CROTY1(CROTY0, ROTZ0),
        CROTY2(CROTY0, ROTZ0, ROTZ0),
        CROTY3(CROTY0, CROTZ0),
        // ROTZ:
        ROTZ1(ROTZ0, ROTY0),
        ROTZ2(ROTZ0, ROTY0, ROTY0),
        ROTZ3(ROTZ0, CROTY0),
        // CROTZ:
        CROTZ1(CROTZ0, ROTY0),
        CROTZ2(CROTZ0, ROTY0, ROTY0),
        CROTZ3(CROTZ0, CROTY0);

        Function<Point3D, Point3D> map; // Because UnaryOperator.andThen returns a Function, not an UnaryOperator

        Orientation(Function<Point3D, Point3D> map) {
            this.map = map;
        }

        Orientation(IntTriFunction<Point3D> map) {
            this.map = point -> map.apply(point.x, point.y, point.z);
        }

        Orientation(Orientation... orientations) {
            Function<Point3D, Point3D> resultMap = orientations[0].map;
            for (int i = 1; i < orientations.length; i++) {
                resultMap = resultMap.andThen(orientations[i].map);
            }
            this.map = resultMap;
        }

    }

}
