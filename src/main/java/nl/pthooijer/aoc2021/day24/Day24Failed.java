package nl.pthooijer.aoc2021.day24;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.LongBinaryOperator;
import java.util.stream.Collectors;

public class Day24Failed {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');

    public static void main(String[] args) {
        String input = "aoc/2021/day/24/input.txt";
        String sample1 = "aoc/2021/day/24/sample-input1.txt";

        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Map<String, LongBinaryOperator> OPERATOR_MAP = Map.of(
            "add", Long::sum,
            "mul", (l, r) -> l * r,
            "div", (l, r) -> l / r,
            "mod", (l, r) -> l % r,
            "eql", (l, r) -> l == r ? 1 : 0);

    public static long part1a(String resource) {
        List<Instruction> instructions = parseInput(resource);

        for (long i = 99999999999999L; i >= 0; i--) {
            long[] digits = Long.toString(i).chars()
                    .mapToLong(c -> c - '0')
                    .toArray();
            if (Arrays.stream(digits).anyMatch(d -> d == 0L)) {
                continue;
            }

            State state = new State(digits, 0, new long[4]);
            if (isValid(state, instructions)) {
                return i;
            }
        }

        return 0L;
    }

    public static long part1(String resource) {
        List<Instruction> instructions = parseInput(resource);

        long serialNr = 98491959997994L;

        long[] digits = Long.toString(serialNr).chars()
                .mapToLong(c -> c - '0')
                .toArray();
        if (Arrays.stream(digits).anyMatch(d -> d == 0L)) {
            return 0L;
        }

        State state = new State(digits, 0, new long[4]);
        if (isValid(state, instructions)) {
            return serialNr;
        }

        return 0L;
    }

//    private static boolean isValid(long modelNr, List<Instruction> instructions) {
//        Deque<Long> digits = Long.toString(modelNr).chars()
//                .mapToObj(c -> (long) (c - '0'))
//                .collect(Collectors.toCollection(LinkedList::new));
//        if (digits.contains(0L)) {
//            return false;
//        }
//
//        long[] vars = new long[4];
//        for (Instruction instruction : instructions) {
//            Arg[] args = instruction.args;
//            if ("inp".equals(instruction.op)) {
//                vars[args[0].value] = digits.removeFirst();
//            } else {
//                vars[args[0].value] = OPERATOR_MAP.get(instruction.op)
//                        .applyAsLong(args[0].value, getValue(vars, args[1]));
//            }
//        }
//
//        return vars[3] == 0;
//    }

    @Value
    private static class State {
        long[] digits;
        int instructionIndex;
        long[] vars;
    }

    private static final Map<State, Boolean> MEMOIZATION_CACHE = new HashMap<>();

    private static boolean isValid(State state, List<Instruction> instructions) {
        if (state.instructionIndex >= instructions.size()) {
            return state.vars[3] == 0;
        } else if (MEMOIZATION_CACHE.containsKey(state)) {
            return MEMOIZATION_CACHE.get(state);
        }

        int instructionIndex = state.instructionIndex;
        long[] vars = Arrays.copyOf(state.vars, state.vars.length);

        Instruction instruction = instructions.get(instructionIndex);
        Arg[] args = instruction.args;
        instructionIndex++;

        long[] digits;
        if ("inp".equals(instruction.op)) {
            vars[args[0].value] = state.digits[0];
            digits = Arrays.copyOfRange(state.digits, 1, state.digits.length);
        } else {
            digits = Arrays.copyOf(state.digits, state.digits.length);
            vars[args[0].value] = OPERATOR_MAP.get(instruction.op)
                    .applyAsLong(vars[args[0].value], getValue(vars, args[1]));
        }

        System.out.println(instruction.toString() + " " + Arrays.toString(vars));

        State newState = new State(digits, instructionIndex, vars);
        boolean result = isValid(newState, instructions);
        MEMOIZATION_CACHE.put(state, result);
        return result;
    }

    private static long getValue(long[] state, Arg arg) {
        return arg.isValue ? arg.value : state[arg.value];
    }

    public static long part2(String resource) {
        return 0L;
    }

    private static List<Instruction> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day24Failed::parseLine)
                .collect(Collectors.toList());
    }

    private static Instruction parseLine(String line) {
        String op = SPACE_SPLITTER.splitToList(line).get(0);
        Arg[] args = Streams.stream(SPACE_SPLITTER.split(line))
                .skip(1)
                .map(Day24Failed::parseArg)
                .toArray(Arg[]::new);
        return new Instruction(line, op, args);
    }

    private static Arg parseArg(String word) {
        try {
            int value = Integer.parseInt(word);
            return new Arg(true, value);
        } catch (NumberFormatException e) {
            return new Arg(false, word.charAt(0) - 'w');
        }
    }

    @Value
    private static class Instruction {
        String string;
        String op;
        Arg[] args;

        public String toString() {
            return string;
        }
    }

    @Value
    private static class Arg {
        boolean isValue;
        int value;
    }


}
