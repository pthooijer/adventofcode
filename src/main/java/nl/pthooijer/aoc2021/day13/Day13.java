package nl.pthooijer.aoc2021.day13;

import com.google.common.base.Splitter;
import lombok.Value;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Debug;
import nl.pthooijer.util.point.Point;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day13 {

    private static final Splitter SPACE_EQUALS_SPLITTER = Splitter.onPattern("( |=)");
    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    public static void main(String[] args) {
        String input = "aoc/2021/day/13/input.txt";
        String sample1 = "aoc/2021/day/13/sample-input1.txt";
        System.out.println("Part 1 sample1 answer (should be 17): " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer (read image from terminal): " + part2(sample1));
        System.out.println("Part 2 answer (read image from terminal): " + part2(input));
    }

    public static long part1(String resource) {
        char[][] grid = parseInputGrid(resource);
        List<FoldInstruction> instructions = parseInstructions(resource);

        char[][] result = fold(grid, instructions.get(0));
        return ArrayUtils.countChars(result, '#');
    }

    // assume for now the fold does not go past row/column 0
    private static char[][] fold(char[][] input, FoldInstruction instruction) {
        if (instruction.isOnX()) {
            char[][] newArr = new char[instruction.coordinate][input[0].length];
            for (int i = 0; i < newArr.length; i++) {
                System.arraycopy(input[i], 0, newArr[i], 0, newArr[0].length);
            }
            for (int i = instruction.coordinate + 1; i < input.length; i++) {
                for (int j = 0; j < newArr[0].length; j++) {
                    int newI = 2 * instruction.coordinate - i;
                    if (input[newI][j] == '.') {
                        newArr[newI][j] = input[i][j];
                    }
                }
            }
            return newArr;

        } else {
            char[][] newArr = new char[input.length][instruction.coordinate];
            for (int i = 0; i < newArr.length; i++) {
                System.arraycopy(input[i], 0, newArr[i], 0, newArr[0].length);
            }
            for (int i = 0; i < newArr.length; i++) {
                for (int j = instruction.coordinate + 1; j < input[0].length; j++) {
                    int newJ = 2 * instruction.coordinate - j;
                    if (input[i][newJ] == '.') {
                        newArr[i][newJ] = input[i][j];
                    }
                }
            }
            return newArr;

        }
    }

    private static char[][] parseInputGrid(String resource) {
        List<Point> points = ResourceHelper.getLinesStream(resource)
                .takeWhile(line -> !line.isEmpty())
                .map(COMMA_SPLITTER::splitToList)
                .map(words -> new Point(Integer.parseInt(words.get(0)), Integer.parseInt(words.get(1))))
                .collect(Collectors.toList());

        int maxX = points.stream()
                .mapToInt(Point::getX)
                .max().orElse(0);
        int maxY = points.stream()
                .mapToInt(Point::getY)
                .max().orElse(0);

        char[][] result = new char[maxX + 1][maxY + 1];
        Arrays.stream(result)
                .forEach(arr -> Arrays.fill(arr, '.'));
        points.forEach(p -> result[p.x][p.y] = '#');
        return result;
    }

    private static List<FoldInstruction> parseInstructions(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .dropWhile(line -> !line.isEmpty())
                .skip(1) // skip the empty line
                .map(Day13::parseInstruction)
                .collect(Collectors.toList());
    }

    private static FoldInstruction parseInstruction(String line) {
        List<String> words = SPACE_EQUALS_SPLITTER.splitToList(line);
        boolean onX = words.get(2).equals("x");
        int coordinate = Integer.parseInt(words.get(3));
        return new FoldInstruction(onX, coordinate);
    }

    @Value
    private static class FoldInstruction {
        boolean onX;
        int coordinate;

        public String toString() {
            char axis = onX ? 'x' : 'y';
            return axis + "=" + coordinate;
        }
    }

    public static long part2(String resource) {
        char[][] grid = parseInputGrid(resource);
        List<FoldInstruction> instructions = parseInstructions(resource);

        char[][] result = grid;
        for (FoldInstruction instruction : instructions) {
            result = fold(result, instruction);
        }
        System.out.println(Debug.toStringTransposed(result));

        return 0;
    }

}
