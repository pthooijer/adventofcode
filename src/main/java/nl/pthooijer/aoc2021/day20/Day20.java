package nl.pthooijer.aoc2021.day20;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Day20 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/20/input.txt";
        String sample1 = "aoc/2021/day/20/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        return doPuzzle(resource, 2);
    }

    public static long part2(String resource) {
        return doPuzzle(resource, 50);
    }

    public static long doPuzzle(String resource, int iterations) {
        int[] algorithm = parseAlgorithm(resource);
        int[][] image = parseImage(resource);
        int infinitePixel = 0;

        for (int i = 0; i < iterations; i++) {
            image = doStep(image, algorithm, infinitePixel);
            infinitePixel = infinitePixel == 0 ? algorithm[0] : algorithm[511];
        }

        return Arrays.stream(image)
                .flatMapToInt(Arrays::stream)
                .sum();
    }

    private static int[][] doStep(int[][] image, int[] algorithm, int infinitePixel) {
        return IntStream.range(0, image.length + 2)
                .mapToObj(x -> IntStream.range(0, image[0].length + 2)
                        .map(y -> doPixelStep(image, algorithm, infinitePixel, x - 1, y - 1))
                        .toArray())
                .toArray(int[][]::new);
    }

    private static int doPixelStep(int[][] image, int[] algorithm, int infinitePixel, int x, int y) {
        int key = IntStream.of(-1, 0, 1)
                .flatMap(dx -> IntStream.of(-1, 0, 1)
                        .map(dy -> ArrayUtils.isInBounds(image, x + dx, y + dy) ?
                                image[x + dx][y + dy] :
                                infinitePixel))
                .reduce(0, (res, pixel) -> (res << 1) + pixel);
        return algorithm[key];
    }

    public static int[] parseAlgorithm(String resource) {
        return ResourceHelper.getLinesList(resource).get(0).chars()
                .map(c -> c == '.' ? 0 : 1)
                .toArray();
    }

    public static int[][] parseImage(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .skip(2)
                .map(line -> line.chars()
                        .map(c -> c == '.' ? 0 : 1)
                        .toArray())
                .toArray(int[][]::new);
    }

}
