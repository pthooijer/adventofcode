package nl.pthooijer.aoc2021.day1;

import nl.pthooijer.util.ResourceHelper;

public class Day1 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/1/input.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        int count = 0;
        long prev = Long.MAX_VALUE;
        for (long i : ResourceHelper.getLongsList(resource)) {
            if (i > prev) {
                count++;
            }
            prev = i;
        }
        return count;
    }

    public static long part2(String resource) {
        long[] input = ResourceHelper.getLongsArray(resource);

        int count = 0;
        long prev = Long.MAX_VALUE;
        for (int i = 0; i < input.length - 2; i++) {
            long amount = input[i] + input[i + 1] + input[i + 2];
            if (amount > prev) {
                count++;
            }
            prev = amount;
        }
        return count;
    }

}
