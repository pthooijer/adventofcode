package nl.pthooijer.aoc2021.day18;

import com.google.common.math.IntMath;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.pthooijer.util.ResourceHelper;

import java.math.RoundingMode;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Day18 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/18/input.txt";
        String sample1 = "aoc/2021/day/18/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static long part1(String resource) {
        Pair result = parseInput(resource).stream()
                .reduce(Day18::reduce).orElseThrow();
        return calculateMagnitude(result);
    }



    private static long part2(String resource) {
        List<Pair> input = parseInput(resource);
        return input.stream()
                .flatMap(pair1 -> input.stream()
                        .filter(pair2 -> !pair1.equals(pair2))
                        .map(pair2 -> reduce(pair1.deepCopy(), pair2.deepCopy())))
                .mapToLong(Day18::calculateMagnitude)
                .max().orElseThrow();
    }

    // Parsing

    private static List<Pair> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day18::parseLine)
                .collect(Collectors.toList());
    }

    // [[[1,4],[1,6]],[0,[5,[6,3]]]]
    private static Pair parseLine(String line) {
        Deque<Character> chars = line.chars()
                .mapToObj(i -> (char) i)
                .collect(Collectors.toCollection(LinkedList::new));
        return (Pair) parseLine(chars);
    }

    private static Node parseLine(Deque<Character> deque) {
        char c = deque.removeFirst();
        if (c == '[') {
            Node left = parseLine(deque);
            deque.removeFirst(); // this is the ','
            Node right = parseLine(deque);
            deque.removeFirst(); // this is the ']'
            return new Pair(left, right);
        } else {
            return new ValueNode(Integer.parseInt(Character.toString(c)));
        }
    }

    // Actions

    private static Pair reduce(Pair leftPair, Pair rightPair) {
        Pair mainNode = new Pair(leftPair, rightPair);
        List<ValueNode> flattenContext = flattenValues(mainNode);

        while (true) {
            boolean hasExploded = explodeFirst(mainNode, flattenContext, 0);
            if (!hasExploded) {
                boolean hasSplit = splitFirst(mainNode, flattenContext);
                if (!hasSplit) {
                    break;
                }
            }
        }
        return mainNode;
    }

    private static List<ValueNode> flattenValues(Node node) {
        if (node instanceof ValueNode vn) {
            return Collections.singletonList(vn);
        } else {
            List<ValueNode> list = new LinkedList<>();
            list.addAll(flattenValues(((Pair) node).left));
            list.addAll(flattenValues(((Pair) node).right));
            return list;
        }
    }

    private static boolean explodeFirst(Node node, List<ValueNode> flattenedContext, int nestLvl) {
        if (node instanceof Pair pair) {
            if (nestLvl == 3) {
                if (pair.left instanceof Pair leftPair) {
                    explode((ValueNode) leftPair.left, flattenedContext, true);
                    int index = explode((ValueNode) leftPair.right, flattenedContext, false);
                    ValueNode replacement = new ValueNode(0);
                    pair.setLeft(replacement);
                    flattenedContext.add(index, replacement);
                    return true;
                } else if (pair.right instanceof Pair rightPair) {
                    explode((ValueNode) rightPair.left, flattenedContext, true);
                    int index = explode((ValueNode) rightPair.right, flattenedContext, false);
                    ValueNode replacement = new ValueNode(0);
                    pair.setRight(replacement);
                    flattenedContext.add(index, replacement);
                    return true;
                } else {
                    return false;
                }
            } else {
                return explodeFirst(pair.left, flattenedContext, nestLvl + 1) ||
                        explodeFirst(pair.right, flattenedContext, nestLvl + 1);
            }
        } else {
            return false;
        }
    }

    private static int explode(ValueNode valueNode, List<ValueNode> flattenContext, boolean toLeft) {
        int index = flattenContext.indexOf(valueNode);
        int indexNeighbour = toLeft ? index - 1 : index + 1;
        if (indexNeighbour >= 0 && indexNeighbour < flattenContext.size()) {
            flattenContext.get(indexNeighbour).add(valueNode.value);
        }
        flattenContext.remove(valueNode);
        return index;
    }

    private static boolean splitFirst(Pair pair, List<ValueNode> flattenContext) {
        ValueNode vnToSplit = flattenContext.stream()
                .filter(vn -> vn.value >= 10)
                .findFirst().orElse(null);
        if (vnToSplit != null) {
            Pair parent = findParent(vnToSplit, pair);
            Pair replacement = createNewPair(vnToSplit, flattenContext); //updates flattenContext
            if (parent.left == vnToSplit) {
                parent.setLeft(replacement);
            } else {
                parent.setRight(replacement);
            }
            return true;
        }
        return false;
    }

    private static Pair findParent(Node node, Pair pairToSearchIn) {
        if (pairToSearchIn.left == node || pairToSearchIn.right == node) {
            return pairToSearchIn;
        }
        if (pairToSearchIn.left instanceof Pair leftPair) {
            Pair leftResult = findParent(node, leftPair);
            if (leftResult != null) {
                return leftResult;
            }
        }
        if (pairToSearchIn.right instanceof Pair rightPair) {
            Pair rightResult = findParent(node, rightPair);
            if (rightResult != null) {
                return rightResult;
            }
        }
        return null;
    }

    private static Pair createNewPair(ValueNode vnToReplace, List<ValueNode> flattenContext) {
        ValueNode leftVn = new ValueNode(IntMath.divide(vnToReplace.value, 2, RoundingMode.DOWN));
        ValueNode rightVn = new ValueNode(vnToReplace.value - leftVn.value);

        int index = flattenContext.indexOf(vnToReplace);
        flattenContext.remove(index);
        flattenContext.add(index, rightVn);
        flattenContext.add(index, leftVn);

        return new Pair(leftVn, rightVn);
    }

    private static long calculateMagnitude(Node node) {
        if (node instanceof Pair pair) {
            return 3L * calculateMagnitude(pair.left) + 2L * calculateMagnitude(pair.right);
        } else {
            return ((ValueNode) node).value;
        }
    }

    // Classes

    private interface Node {
        Node deepCopy();
    }

    @AllArgsConstructor
    @Getter
    @Setter
    private static class Pair implements Node {
        public Node left;
        public Node right;

        @Override
        public Pair deepCopy() {
            return new Pair(left.deepCopy(), right.deepCopy());
        }

        public String toString() {
            return String.format("[%s,%s]", left.toString(), right.toString());
        }
    }

    @AllArgsConstructor
    @Getter
    @Setter
    private static class ValueNode implements Node {
        public int value;

        public void add(int amount) {
            value += amount;
        }

        @Override
        public ValueNode deepCopy() {
            return new ValueNode(value);
        }

        public String toString() {
            return Integer.toString(value);
        }
    }

}
