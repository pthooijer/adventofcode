package nl.pthooijer.aoc2021.day11;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;

public class Day11 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/11/input.txt";
        String sample1 = "aoc/2021/day/11/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long[][] octoArray = ResourceHelper.getLongs2dArray(resource);

        int iterations = 100;
        long flashes = 0;

        for (int i = 0; i < iterations; i++) {
            increment(octoArray);
            flashes += processStep(octoArray);
        }

        return flashes;
    }

    private static void increment(long[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j]++;
            }
        }
    }

    private static long processStep(long[][] arr) {
        long flashes = 0;
        long prevFlashes = -1;
        while (flashes != prevFlashes) {
            prevFlashes = flashes;

            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[0].length; j++) {
                    if (arr[i][j] > 9) {

                        for (int di = -1; di <= 1; di++) {
                            for (int dj = -1; dj <= 1; dj++) {
                                int i2 = i + di;
                                int j2 = j + dj;
                                if (ArrayUtils.isInBounds(arr, i2, j2) && arr[i + di][j + dj] != 0) {
                                    arr[i2][j2]++;
                                }
                            }
                        }

                        arr[i][j] = 0;
                        flashes++;
                    }
                }
            }
        }

        return flashes;
    }

    public static long part2(String resource) {
        long[][] octoArray = ResourceHelper.getLongs2dArray(resource);

        long iterations = 0;
        while (true) {
            iterations++;
            increment(octoArray);
            processStep(octoArray);
            if (isAllZeroes(octoArray)) {
                return iterations;
            }
        }
    }

    private static boolean isAllZeroes(long[][] arr) {
        return Arrays.stream(arr)
                .flatMapToLong(Arrays::stream)
                .allMatch(i -> i == 0);
    }
}
