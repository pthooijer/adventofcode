package nl.pthooijer.aoc2021.day25;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Debug;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;

public class Day25 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/25/input.txt";
        String sample1 = "aoc/2021/day/25/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);

        long steps = 0L;
        boolean moved = true;
        while (moved) {
            steps++;
            moved = false;

            char[][] newGrid = new char[grid.length][grid[0].length];
            Arrays.stream(newGrid).forEach(row -> Arrays.fill(row, '.'));

            for (int x = grid.length - 1; x >= 0; x--) {
                for (int y = grid[0].length - 1; y >= 0; y--) {
                    if (grid[x][y] == '>') {
                        int newY = ArrayUtils.isInBounds(grid, x, y + 1) ? y + 1 : 0;
                        if (grid[x][newY] == '.') {
                            newGrid[x][newY] = '>';
                            moved = true;
                        } else {
                            newGrid[x][y] = '>';
                        }
                    } else if (grid[x][y] == 'v') {
                        newGrid[x][y] = 'v';
                    }
                }
            }

            grid = newGrid;
            newGrid = new char[grid.length][grid[0].length];
            Arrays.stream(newGrid).forEach(row -> Arrays.fill(row, '.'));

            for (int x = grid.length - 1; x >= 0; x--) {
                for (int y = grid[0].length - 1; y >= 0; y--) {
                    if (grid[x][y] == 'v') {
                        int newX = ArrayUtils.isInBounds(grid, x + 1, y) ? x + 1 : 0;
                        if (grid[newX][y] == '.') {
                            newGrid[newX][y] = 'v';
                            moved = true;
                        } else {
                            newGrid[x][y] = 'v';
                        }
                    } else if (grid[x][y] == '>') {
                        newGrid[x][y] = '>';
                    }
                }
            }

            grid = newGrid;
        }

        return steps;
    }

    public static String part2(String resource) {
        return "Happy Holidays!";
    }
}
