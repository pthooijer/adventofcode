package nl.pthooijer.aoc2021.day16;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.function.LongBinaryOperator;
import java.util.stream.Collectors;

public class Day16 {

    private static final int LONG_BITS = Long.BYTES * 8;

    public static void main(String[] args) {
        String input = "aoc/2021/day/16/input.txt";
        String sample1 = "aoc/2021/day/16/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static void toBinaryString(String data) {
        String dataString = data.chars()
                .mapToObj(c -> Character.toString((char) c))
                .collect(Collectors.joining("   "));
        System.out.println(dataString);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length(); i = i + 8) {
            int length = Math.min(8, data.length() - i);
            String subString = data.substring(i, i + length);
            builder.append(Strings.padStart(Long.toString(Long.parseLong(subString, 16), 2),
                    length * 4, '0'));
        }
        System.out.println(builder.toString());
    }

    public static long part1(String resource) {
        String data = ResourceHelper.getLinesList(resource).get(0);

        Decoder decoder = new Decoder(data);
        Packet mainPacket = decoder.nextPacket();

        return packetVersionSum(mainPacket);
    }

    private static int packetVersionSum(Packet packet) {
        return packet.version +
                packet.subPackets.stream()
                        .mapToInt(Day16::packetVersionSum)
                        .sum();
    }


    public static long part2(String resource) {
        String data = ResourceHelper.getLinesList(resource).get(0);

        Decoder decoder = new Decoder(data);
        Packet mainPacket = decoder.nextPacket();

        return calculateValue(mainPacket);
    }

    private static long calculateValue(Packet packet) {
        if (packet instanceof ValuePacket) {
            return ((ValuePacket) packet).value;
        }
        Type type = Type.forId(packet.typeId);
        return packet.subPackets.stream()
                .mapToLong(Day16::calculateValue)
                .reduce(type.reduction)
                .orElseThrow();
    }

    private static class Decoder {
        private final Deque<Character> remaining;
        private long leftover = 0;
        private int leftoverLength = 0;

        private Decoder(String message) {
            this.remaining = message.chars()
                    .mapToObj(i -> (char) i)
                    .collect(Collectors.toCollection(LinkedList::new));
        }

        public long getBits(int amount) {
            long result = leftover;
            int length = leftoverLength;

            while (length < amount) {
                result <<= 4;
                length += 4;
                long nextChar = Long.parseLong(remaining.removeFirst().toString(), 16);
                result |= nextChar;
            }

            leftoverLength = length - amount;
            if (leftoverLength == 0) {
                leftover = 0L;
            } else {
                int shift = (LONG_BITS - leftoverLength); // ex: result = 00101111, leftoverLength = 2
                leftover = result << shift >>> shift; // set all bits to 0 except the last #shift amount. ex: 00000011
                result >>>= leftoverLength; // remove last #leftoverlength bits. ex: 00001011
            }

            return result;
        }

        public int getBitsInt(int amount) {
            return (int) getBits(amount);
        }

        public Packet nextPacket() {
            int version = getBitsInt(3);

            int typeId = getBitsInt(3);
            if (typeId == 4) {
                return parseValuePacket(version);
            } else {
                return parseOperator(version, typeId);
            }

        }

        private Packet parseOperator(int version, int typeId) {
            long test = getBits(1);
            boolean lengthIsInSubPackets = test == 1L;

            if (lengthIsInSubPackets) {
                int length = getBitsInt(11);
                int currentNrOfSubPackets = 0;
                int currentLength = 0;
                List<Packet> subPackets = new ArrayList<>(length);
                while (currentNrOfSubPackets < length) {
                    Packet subPacket = nextPacket();
                    subPackets.add(subPacket);
                    currentNrOfSubPackets++;
                    currentLength += subPacket.length;
                }
                return new Packet(version, typeId, lengthIsInSubPackets, currentLength + 7 + 11, subPackets);

            } else {
                int length = getBitsInt(15);
                int currentLength = 0;
                List<Packet> subPackets = new ArrayList<>();
                while (currentLength < length) {
                    Packet subPacket = nextPacket();
                    subPackets.add(subPacket);
                    currentLength += subPacket.length;
                }
                return new Packet(version, typeId, lengthIsInSubPackets, length + 7 + 15, subPackets);
            }
        }

        private ValuePacket parseValuePacket(int version) {
            long value = 0;
            int valueLength = 0;
            int packetLength = 0;
            while (true) {
                long nextBlock = getBits(5);
                long nextBlockValue = nextBlock << (LONG_BITS - 4) >>> (LONG_BITS - 4);
                value = (value << 4) | nextBlockValue;
                valueLength += 4;
                packetLength += 5;

                if (nextBlock >>> 4 == 0) { // bit 5 is 0, so this was the last block
                    break;
                }
            }
            return new ValuePacket(version, packetLength + 6, valueLength, value);
        }
    }

    @AllArgsConstructor
    @ToString
    private static class Packet {
        int version;
        int typeId;
        boolean lengthIsInSubPackets;
        int length;
        List<Packet> subPackets;
    }

    @Getter
    @ToString(callSuper = true)
    private static class ValuePacket extends Packet {
        private final long value;
        private final int valueLength;

        private ValuePacket(int version, int packetLength, int valueLength, long value) {
            super(version, 4, false, packetLength, Collections.emptyList());
            this.value = value;
            this.valueLength = valueLength;
        }
    }

    private enum Type {
        ZERO(0, 0L, Long::sum),
        ONE(1, 1L, (i, j) -> i * j),
        TWO(2, 0L, Math::min),
        THREE(3, 0L, Math::max),
        FIVE(5, Long.MIN_VALUE, (i, j) -> i > j ? 1L : 0L),
        SIX(6, Long.MAX_VALUE, (i, j) -> i < j ? 1L : 0L),
        SEVEN(7, Long.MIN_VALUE, (i, j) -> i == j ? 1L : 0L);

        private final int typeId;
        private final long identity;
        private final LongBinaryOperator reduction;

        Type(int typeId, long identity, LongBinaryOperator reduction) {
            this.typeId = typeId;
            this.identity = identity;
            this.reduction = reduction;
        }

        public static Type forId(int typeId) {
            return Arrays.stream(values())
                    .filter(type -> type.typeId == typeId)
                    .findAny().orElseThrow();
        }
    }

}
