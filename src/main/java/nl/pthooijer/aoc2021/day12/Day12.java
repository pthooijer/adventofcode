package nl.pthooijer.aoc2021.day12;

import com.google.common.base.Splitter;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day12 {

    private static final Splitter DASH_SPLITTER = Splitter.on('-');

    public static void main(String[] args) {
        String input = "aoc/2021/day/12/input.txt";
        String sample1 = "aoc/2021/day/12/sample-input1.txt";
        String sample2 = "aoc/2021/day/12/sample-input2.txt";
        String sample3 = "aoc/2021/day/12/sample-input3.txt";

        System.out.println("Part 1 sample1 answer (must be 10): " + part1(sample1));
        System.out.println("Part 1 sample2 answer (must be 19): " + part1(sample2));
        System.out.println("Part 1 sample3 answer (must be 226): " + part1(sample3));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Part 2 sample1 answer (must be 36): " + part2(sample1));
        System.out.println("Part 2 sample2 answer (must be 103): " + part2(sample2));
        System.out.println("Part 2 sample3 answer (must be 3509): " + part2(sample3));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        CaveMap caveMap = parseInput(resource);
        List<String> paths = advanceStep(caveMap, "start", "start");
        return paths.size();
    }

    private static CaveMap parseInput(String resource) {
        List<Connection> connections = ResourceHelper.getLinesStream(resource)
                .map(DASH_SPLITTER::splitToList)
                .map(caves -> new Connection(caves.get(0), caves.get(1)))
                .collect(Collectors.toList());
        return new CaveMap(connections, null);
    }

    private static List<String> advanceStep(CaveMap caveMap, String currentCave, String currentPath) {
        List<String> paths = new ArrayList<>();
        for (Connection connection : caveMap.connectedTo(currentCave)) {
            String newCave = connection.getOther(currentCave);
            String newPath = currentPath + "," + newCave;
            if (newCave.equals("end")) {
                paths.add(newPath);
            } else {
                CaveMap newCaveMap = caveMap.deepCopy();
                newCaveMap.removeCaveChecked(currentCave);
                paths.addAll(advanceStep(newCaveMap, newCave, newPath));
            }
        }
        return paths;
    }

    public static long part2(String resource) {
        CaveMap caveMap = parseInput(resource);
        List<String> smallCaveList = caveMap.connections.stream()
                .flatMap(connection -> Stream.of(connection.cave1, connection.cave2))
                .filter(cave -> Character.isLowerCase(cave.charAt(0)))
                .filter(cave -> !"start".equals(cave) && !"end".equals(cave))
                .distinct()
                .collect(Collectors.toList());

        List<String> paths = new ArrayList<>();
        for (String doubleCave : smallCaveList) { // try every cave to be marked doubleCave
            CaveMap caveMapWithDouble = new CaveMap(new LinkedList<>(caveMap.connections), doubleCave);
            paths.addAll(advanceStep(caveMapWithDouble, "start", "start"));
        }
        return paths.stream()
                .distinct() // Above method generates duplicates
                .count();
    }

    private static class CaveMap {
        private final List<Connection> connections;
        private String doubleCave;

        public CaveMap(List<Connection> connections, String doubleCave) {
            this.connections = connections;
            this.doubleCave = doubleCave;
        }

        /**
         * If cave is a large cave, do nothing.
         * If cave is marked as the doubleCave, remove the doubleCave mark.
         * If cave is an other small cave, remove the cave from the CaveMap.
         *
         * @param cave The cave to remove
         */
        public void removeCaveChecked(String cave) {
            if (cave.equals(doubleCave)) {
                doubleCave = null;
            } else {
                connections.removeIf(conn -> Character.isLowerCase(cave.charAt(0)) &&
                        conn.contains(cave));
            }
        }

        public List<Connection> connectedTo(String from) {
            return connections.stream()
                    .filter(conn -> conn.contains(from))
                    .collect(Collectors.toList());
        }

        public CaveMap deepCopy() {
            return new CaveMap(new LinkedList<>(connections), doubleCave);
        }
    }

    @Value
    private static class Connection {
        String cave1;
        String cave2;

        public boolean contains(String cave) {
            return cave1.equals(cave) || cave2.equals(cave);
        }

        public String getOther(String from) {
            if (cave1.equals(from)) {
                return cave2;
            } else if (cave2.equals(from)) {
                return cave1;
            } else {
                throw new IllegalStateException();
            }
        }

        public String toString() {
            return cave1 + "-" + cave2;
        }
    }
}
