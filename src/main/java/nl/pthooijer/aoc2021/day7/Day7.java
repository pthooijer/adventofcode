package nl.pthooijer.aoc2021.day7;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;

public class Day7 {

    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    public static void main(String[] args) {
        String input = "aoc/2021/day/7/input.txt";
        String sample1 = "aoc/2021/day/7/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        // Calculate median
        long[] input = Streams.stream(COMMA_SPLITTER.split(ResourceHelper.getLinesIt(resource).iterator().next()))
                .mapToLong(Long::parseLong)
                .toArray();

        Arrays.sort(input);

        int size = input.length;
        long median;
        if (size % 2 == 0) {
            median = (input[size / 2] + input[size / 2 - 1]) / 2;
        } else {
            median = input[(size - 1) / 2];
        }

        return Arrays.stream(input)
                .map(pos -> Math.abs(pos - median))
                .sum();
    }

    public static long part2(String resource) {
        long[] input = Streams.stream(COMMA_SPLITTER.split(ResourceHelper.getLinesIt(resource).iterator().next()))
                .mapToLong(Long::parseLong)
                .toArray();

        // Start the search halfway
        long pos = Arrays.stream(input).max().orElseThrow() / 2;
        long dist = distance2To(input, pos);

        // Check its neighbours, see if we have to go up or down
        long distUp = distance2To(input, pos + 1);
        long step = distUp < dist ? 1 : -1;

        // Find local minimum (this is the global minimum in this puzzle)
        while (true) {
            pos += step;
            long newDist = distance2To(input, pos);
            if(newDist > dist) {
                return dist;
            } else {
                dist = newDist;
            }
        }
    }

    private static long distance2To(long[] input, long pos) {
        return Arrays.stream(input)
                .map(n -> Math.abs(n - pos))
                .map(Day7::distance2)
                .sum();
    }

    private static long distance2(long n) {
        // Fuel steps are the Triangle numbers
        return n * (n + 1) / 2;
    }

}
