package nl.pthooijer.aoc2021.day17;

import com.google.common.base.Splitter;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.List;
import java.util.stream.Collectors;

public class Day17 {

    private static final Splitter REMOVE_WORDS_SPLITTER =
            Splitter.onPattern("(target area: x=|\\.\\.|, y=)").omitEmptyStrings();

    public static void main(String[] args) {
        String input = "aoc/2021/day/17/input.txt";
        String sample1 = "aoc/2021/day/17/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        // Done on pen and paper
        return 11175L;
    }

    public static long part2(String resource) {
        Area area = parseInput(ResourceHelper.getLinesList(resource).get(0));

        /* vy:
        Max is reached for vy = -yMin + 1
        Direct hit is at vy = yMin

        vx:
        Fastest is 1 step: vx = xMax
        Slowest is n * (n+1) / 2 = xMin -> (sqrt(8 xMin + 1) - 1)/2, let's just take xMin = 1
         */

        long count = 0;
        for (int vx = 1; vx <= area.xMax; vx++) {
            for (int vy = area.yMin; vy <= -area.yMin + 1; vy++) {
                if (hitsArea(vx, vy, area)) {
                    count++;
                }
            }
        }

        return count;
    }

    private static boolean hitsArea(int vxStart, int vyStart, Area area) {
        int x = 0;
        int y = 0;
        int vx = vxStart;
        int vy = vyStart;

        while (x <= area.xMax && y >= area.yMin) {
            x += vx;
            y += vy;
            vx = Math.max(vx - 1, 0);
            vy -= 1;

            if(area.contains(x, y)) {
                return true;
            }
        }

        return false;
    }

    private static Area parseInput(String resource) {
        List<Integer> numbers = REMOVE_WORDS_SPLITTER.splitToList(resource).stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        return new Area(numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3));
    }

    @Value
    private static class Area {
        int xMin;
        int xMax;
        int yMin;
        int yMax;

        public boolean contains(int x, int y) {
            return x >= xMin && x <= xMax && y >= yMin && y <= yMax;
        }

        public String toString() {
            return String.format("x=%d..%d y=%d..%d", xMin, xMax, yMin, yMax);
        }
    }

}
