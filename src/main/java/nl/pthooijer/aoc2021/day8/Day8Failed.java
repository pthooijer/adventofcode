package nl.pthooijer.aoc2021.day8;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Day8Failed {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ').omitEmptyStrings();
    private static final Splitter UNDERSCORE_SPLITTER = Splitter.on('|');

    public static void main(String[] args) {
        String input = "aoc/2021/day/8/input.txt";
        String sample1 = "aoc/2021/day/8/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample0 answer: " + decodeLine("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |" +
                "cdfeb fcadb cdfeb cdbaf"));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        Map<Integer, Long> lengthTallyMap = ResourceHelper.getLinesStream(resource)
                .map(line -> UNDERSCORE_SPLITTER.splitToList(line).get(1))
                .flatMap(line -> Streams.stream(SPACE_SPLITTER.split(line)))
                .collect(Collectors.groupingBy(String::length, Collectors.counting()));
        return lengthTallyMap.get(2) + lengthTallyMap.get(3) + lengthTallyMap.get(4) + lengthTallyMap.get(7);
    }

    public static long part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .mapToLong(Day8Failed::decodeLine)
                .sum();
    }

    private static long decodeLine(String line) {
        List<String> uniquePatterns = Streams.stream(SPACE_SPLITTER.split(UNDERSCORE_SPLITTER.splitToList(line).get(0)))
                .collect(Collectors.toList());

        Map<Integer, List<String>> lengthToWord = uniquePatterns.stream()
                .collect(Collectors.groupingBy(String::length));

        Map<Character, Long> charTally = uniquePatterns.stream()
                .flatMapToInt(String::chars)
                .mapToObj(i -> (char) i)
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));
        Map<Long, List<Character>> charAppearances = charTally.entrySet().stream()
                .collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.mapping(Map.Entry::getKey, Collectors.toList())));

        Map<Integer, String> answerDigitMap = new HashMap<>();
        answerDigitMap.put(1, lengthToWord.get(2).get(0));
        answerDigitMap.put(7, lengthToWord.get(3).get(0));
        answerDigitMap.put(4, lengthToWord.get(4).get(0));
        answerDigitMap.put(8, lengthToWord.get(7).get(0));

        Map<Character, Character> answerPosMap = new HashMap<>();

        // A appears in length 3 (7) but not length 2 (1):
        answerPosMap.put('a', answerDigitMap.get(7).chars()
                .filter(i -> answerDigitMap.get(1).chars().noneMatch(i2 -> i == i2))
                .mapToObj(i -> (char) i)
                .collect(singletonCollector()));

        // Pos F B and E have unique corresponding digits
        answerPosMap.put('f', charAppearances.get(9L).get(0));
        answerPosMap.put('b', charAppearances.get(6L).get(0));
        answerPosMap.put('e', charAppearances.get(4L).get(0));

        // 2 is the only one missing F
        answerDigitMap.put(2, uniquePatterns.stream()
                .filter(s -> !s.contains(answerPosMap.get('f') + ""))
                .collect(singletonCollector()));

        // 3 is the only remaining length 5 missing B
        answerDigitMap.put(3, lengthToWord.get(5).stream()
                .filter(s -> !s.equals(answerDigitMap.get(2)))
                .filter(s -> !s.contains(answerPosMap.get('b') + ""))
                .collect(singletonCollector()));

        // 5 is the only length 5 remaining
        answerDigitMap.put(5, lengthToWord.get(5).stream()
                .filter(s -> !(s.equals(answerDigitMap.get(2)) || s.equals(answerDigitMap.get(3))))
                .collect(singletonCollector()));

        // 9 is the only length 6 missing E
        answerDigitMap.put(9, lengthToWord.get(6).stream()
                .filter(s -> !s.contains(answerPosMap.get('e') + ""))
                .collect(singletonCollector()));

        // D appears in 1 but not in 5
        answerPosMap.put('d', answerDigitMap.get(1).chars()
                .filter(i -> answerDigitMap.get(5).chars().noneMatch(i2 -> i == i2))
                .mapToObj(i -> (char) i)
                .collect(singletonCollector()));

        // 0 is the only length 6 missing D
        answerDigitMap.put(0, lengthToWord.get(6).stream()
                .filter(s -> !s.contains(answerPosMap.get('d') + ""))
                .collect(singletonCollector()));

        // 6 is the only length 6 remaining
        answerDigitMap.put(6, lengthToWord.get(6).stream()
                .filter(s -> !(s.equals(answerDigitMap.get(9)) || s.equals(answerDigitMap.get(0))))
                .collect(singletonCollector()));

//        System.out.println(line);
//        System.out.println(answerDigitMap);
//        System.out.println(answerPosMap);

        // Now decode the signal
        // Sort the strings
        Map<String, Integer> answerDigitMapSorted = answerDigitMap.entrySet().stream()
                .collect(Collectors.toMap(entry -> sortString(entry.getValue()), Map.Entry::getKey));
        return Streams.stream(SPACE_SPLITTER.split(UNDERSCORE_SPLITTER.splitToList(line).get(1)))
                .map(Day8Failed::sortString)
                .map(s -> Integer.toString(answerDigitMapSorted.get(s)))
                .collect(Collectors.collectingAndThen(Collectors.joining(), Long::parseLong));
    }

    private static String sortString(String s) {
        char[] arr = s.toCharArray();
        Arrays.sort(arr);
        return new String(arr);
    }

    private static <T> Collector<T, ?, T> singletonCollector() {
        return Collectors.collectingAndThen(Collectors.toList(), list -> {
            if (list.size() != 1) {
                throw new IllegalStateException();
            }
            return list.get(0);
        });
    }
}
