package nl.pthooijer.aoc2021.day8;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import com.google.common.math.IntMath;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day8 {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ').omitEmptyStrings();
    private static final Splitter UNDERSCORE_SPLITTER = Splitter.on('|');

    public static void main(String[] args) {
        String input = "aoc/2021/day/8/input.txt";
        String sample1 = "aoc/2021/day/8/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample0 answer: " +
                decodeLine("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        Map<Integer, Long> lengthTallyMap = ResourceHelper.getLinesStream(resource)
                .map(line -> UNDERSCORE_SPLITTER.splitToList(line).get(1))
                .flatMap(line -> Streams.stream(SPACE_SPLITTER.split(line)))
                .collect(Collectors.groupingBy(String::length, Collectors.counting()));

        return IntStream.of(2, 3, 4, 7)
                .mapToLong(lengthTallyMap::get)
                .sum();
    }

    public static long part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .mapToLong(Day8::decodeLine)
                .sum();
    }

    private static final Map<String, Integer> SEGMENTS_TO_DIGIT = getSegmentsToDigit();

    private static Map<String, Integer> getSegmentsToDigit() { // To make the keys unique, the Strings must be sorted
        Map<String, Integer> segmentsToDigit = new HashMap<>();
        segmentsToDigit.put("abcefg", 0);
        segmentsToDigit.put("cf", 1);
        segmentsToDigit.put("acdeg", 2);
        segmentsToDigit.put("acdfg", 3);
        segmentsToDigit.put("bcdf", 4);
        segmentsToDigit.put("abdfg", 5);
        segmentsToDigit.put("abdefg", 6);
        segmentsToDigit.put("acf", 7);
        segmentsToDigit.put("abcdefg", 8);
        segmentsToDigit.put("abcdfg", 9);
        return segmentsToDigit;
    }

    private static long decodeLine(String line) {
        List<String> uniquePatterns = Streams.stream(SPACE_SPLITTER.split(UNDERSCORE_SPLITTER.splitToList(line).get(0)))
                .collect(Collectors.toList());
        List<String> wordsToDecode = Streams.stream(SPACE_SPLITTER.split(UNDERSCORE_SPLITTER.splitToList(line).get(1)))
                .collect(Collectors.toList());

        char[] correctDecodeTable = permutations("abcdefg").stream() // brute force every possible decodeTable
                // decodeTable is an array with the encoded char as index, and the original char as value
                .map(String::toCharArray)
                // Check if the decode table generates valid displayed numbers
                .filter(decodeTable -> uniquePatterns.stream()
                        .map(word -> decodeWord(word, decodeTable))
                        .allMatch(SEGMENTS_TO_DIGIT::containsKey))
                // Find the only correct decodeTable
                .findAny().orElseThrow();

        // Decode the wordsToDecode
        return wordsToDecode.stream()
                .map(word -> decodeWord(word, correctDecodeTable))
                .map(SEGMENTS_TO_DIGIT::get)
                .map(Object::toString)
                .collect(Collectors.collectingAndThen(Collectors.joining(), Long::parseLong));
    }

    private static List<String> permutations(String s) {
        List<String> result = new ArrayList<>(IntMath.factorial(s.length()));
        permutations("", s, result);
        return result;
    }

    private static void permutations(String prefix, String s, List<String> list) {
        int n = s.length();
        if (n == 0) {
            list.add(prefix);
        } else {
            for (int i = 0; i < n; i++) {
                permutations(prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, n), list);
            }
        }
    }

    private static String decodeWord(String input, char[] decodeTable) {
        StringBuilder decodedWord = new StringBuilder();
        for (char c : input.toCharArray()) {
            decodedWord.append(decodeTable[c - 'a']);
        }
        return sortString(decodedWord.toString());
    }

    private static String sortString(String s) {
        char[] arr = s.toCharArray();
        Arrays.sort(arr);
        return new String(arr);
    }
}
