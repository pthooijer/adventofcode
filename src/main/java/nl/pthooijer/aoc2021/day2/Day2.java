package nl.pthooijer.aoc2021.day2;

import com.google.common.base.Splitter;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.List;

public class Day2 {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');

    public static void main(String[] args) {
        String input = "aoc/2021/day/2/input.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        Position finalPos = ResourceHelper.getLinesStream(resource)
                .map(Day2::readInstruction)
                .reduce(new Position(0, 0), (p1, p2) ->
                        new Position(p1.horizontal + p2.horizontal, p1.depth + p2.depth));
        return finalPos.horizontal * finalPos.depth;
    }

    private static Position readInstruction(String instruction) {
        List<String> words = SPACE_SPLITTER.splitToList(instruction);
        String direction = words.get(0);
        long amount = Long.parseLong(words.get(1));
        return switch (direction) {
            case "forward" -> new Position(amount, 0);
            case "down" -> new Position(0, amount);
            case "up" -> new Position(0, -amount);
            default -> throw new IllegalArgumentException();
        };
    }

    @Value
    private static class Position {
        long horizontal;
        long depth;
    }

    public static long part2(String resource) {
        long horizontal = 0;
        long depth = 0;
        long aim = 0;

        for(String instruction : ResourceHelper.getLinesIt(resource)) {
            List<String> words = SPACE_SPLITTER.splitToList(instruction);
            String direction = words.get(0);
            long amount = Long.parseLong(words.get(1));

            switch (direction) {
                case "down" -> aim += amount;
                case "up" -> aim -= amount;
                case "forward" -> {
                    horizontal += amount;
                    depth += aim * amount;
                }
                default -> throw new IllegalArgumentException();
            }
        }
        return horizontal * depth;
    }

}
