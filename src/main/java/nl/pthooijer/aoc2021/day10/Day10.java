package nl.pthooijer.aoc2021.day10;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

public class Day10 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/10/input.txt";
        String sample1 = "aoc/2021/day/10/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final BiMap<Character, Character> BEGIN_END_PAIRS = HashBiMap.create(Map.of(
            '(', ')',
            '[', ']',
            '{', '}',
            '<', '>'));
    private static final Map<Character, Long> CORRUPT_SCORE_SHEET = Map.of(
            ')', 3L,
            ']', 57L,
            '}', 1197L,
            '>', 25137L);

    public static long part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day10::searchCorruptCharacter)
                .filter(c -> !Objects.isNull(c))
                .mapToLong(CORRUPT_SCORE_SHEET::get)
                .sum();
    }

    // Returns null if there is no corrupt char
    private static Character searchCorruptCharacter(String line) {
        Deque<Character> beginStack = new LinkedList<>();
        for (char c : line.toCharArray()) {
            if (BEGIN_END_PAIRS.containsKey(c)) {
                beginStack.addLast(c);
            } else {
                char beginToClose = beginStack.removeLast();
                if (!BEGIN_END_PAIRS.get(beginToClose).equals(c)) {
                    return c;
                }
            }
        }
        return null;
    }

    private static final Map<Character, Long> AC_SCORE_SHEET = Map.of(
            ')', 1L,
            ']', 2L,
            '}', 3L,
            '>', 4L);

    public static long part2(String resource) {
        long[] scores = ResourceHelper.getLinesStream(resource)
                .filter(line -> searchCorruptCharacter(line) == null)
                .map(Day10::completeLine)
                .mapToLong(Day10::calculateAcScore)
                .sorted()
                .toArray();

        return scores[scores.length / 2];
    }

    public static char[] completeLine(String line) {
        Deque<Character> beginStack = new LinkedList<>();
        for (char c : line.toCharArray()) {
            if (BEGIN_END_PAIRS.containsKey(c)) {
                beginStack.addLast(c);
            } else {
                beginStack.removeLast();
            }
        }

        int size = beginStack.size(); // Not in the for loop, .size() keeps getting updated
        char[] result = new char[size];
        for (int i = 0; i < size; i++) {
            result[i] = BEGIN_END_PAIRS.get(beginStack.removeLast());
        }
        return result;
    }

    private static long calculateAcScore(char[] result) {
        return ArrayUtils.stream(result)
                .mapToLong(AC_SCORE_SHEET::get)
                .reduce(0L, (res, score) -> res * 5L + score);
    }
}
