package nl.pthooijer.aoc2021.day21;

import com.google.common.base.Splitter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.stream.IntStream;

public class Day21 {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');

    public static void main(String[] args) {
        String input = "aoc/2021/day/21/input.txt";
        String sample1 = "aoc/2021/day/21/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long pos1 = parseStart(1, resource);
        long pos2 = parseStart(2, resource);
        long score1 = 0L;
        long score2 = 0L;
        long nextRoll = 1L;
        long nrOfRolls = 0L;

        while (true) {
            // player 1
            long roll = nextRoll + modPlusOne(nextRoll + 1, 100L) + modPlusOne(nextRoll + 2, 100L);
            nrOfRolls += 3;
            nextRoll = modPlusOne(nextRoll + 3, 100L);

            pos1 = modPlusOne(pos1 + roll, 10L);
            score1 += pos1;
            if (score1 >= 1000L) {
                return score2 * nrOfRolls;
            }

            // player 2
            roll = nextRoll + modPlusOne(nextRoll + 1, 100L) + modPlusOne(nextRoll + 2, 100L);
            nrOfRolls += 3;
            nextRoll = modPlusOne(nextRoll + 3, 100L);

            pos2 = modPlusOne(pos2 + roll, 10L);
            score2 += pos2;
            if (score2 >= 1000L) {
                return score1 * nrOfRolls;
            }
        }
    }

    private static long modPlusOne(long dividend, long divisor) {
        return (((dividend) - 1L) % divisor) + 1L; // ex: 100 -> 99 -> 99 -> 100. ex2: 101 -> 100 -> 0 -> 1.
    }

    private static long parseStart(int player, String resource) {
        String playerLine = ResourceHelper.getLinesList(resource).get(player - 1);
        return Long.parseLong(SPACE_SPLITTER.splitToList(playerLine).get(4));
    }

    // MULTIPLICITY_TABLE[i] = nr of ways you can roll i. ex: nr of ways you can roll 4 is 3: 112, 121, 211
    private static final long[] MULTIPLICITY_TABLE = new long[]{0L, 0L, 0L, 1L, 3L, 6L, 7L, 6L, 3L, 1L};

    public static long part2(String resource) {
        State state = new State(parseStart(1, resource), parseStart(2, resource), 0L, 0L);

        Wins wins = doNextRoll(1, state);

        return Math.max(wins.p1Wins, wins.p2Wins);
    }

    public static Wins doNextRoll(int player, State state) {
        return IntStream.rangeClosed(3, 9)
                .mapToObj(nextRoll -> withNextRoll(player, state.copy(), nextRoll)
                        .times(MULTIPLICITY_TABLE[nextRoll]))
                .reduce(Wins::add).orElseThrow();
    }

    public static Wins withNextRoll(int player, State state, int roll) {
        if (player == 1) {
            state.pos1 = modPlusOne(state.pos1 + roll, 10L);
            state.score1 += state.pos1;
            return state.score1 >= 21 ? new Wins(1L, 0L) : doNextRoll(2, state);
        } else {
            state.pos2 = modPlusOne(state.pos2 + roll, 10L);
            state.score2 += state.pos2;
            return state.score2 >= 21 ? new Wins(0L, 1L) : doNextRoll(1, state);
        }
    }

    @Value
    private static class Wins {
        long p1Wins;
        long p2Wins;

        public Wins add(Wins other) {
            return new Wins(this.p1Wins + other.p1Wins, this.p2Wins + other.p2Wins);
        }

        public Wins times(long amount) {
            return new Wins(this.p1Wins * amount, this.p2Wins * amount);
        }
    }


    @Data
    @AllArgsConstructor
    private static class State {
        long pos1;
        long pos2;
        long score1;
        long score2;

        public State copy() {
            return new State(pos1, pos2, score1, score2);
        }
    }

}
