package nl.pthooijer.aoc2021.day15;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.point.Point;
import nl.pthooijer.util.ResourceHelper;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day15 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/15/input.txt";
        String sample1 = "aoc/2021/day/15/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long[][] input = ResourceHelper.getLongs2dArray(resource);
        return dijkstraAlgorithm(input);
    }

    // Algorithm copied from the pseudo-code on Wikipedia: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Using_a_priority_queue
    private static long dijkstraAlgorithm(long[][] input) {
        Point start = new Point(0, 0);
        Point end = new Point(input.length - 1, input[0].length - 1);

        Map<Point, Long> dist = new HashMap<>();
        dist.put(start, 0L);

        Comparator<Point> hasLowestDist = Comparator.comparingLong(p -> dist.getOrDefault(p, Long.MAX_VALUE));
        PriorityQueue<Point> prioQueue = new PriorityQueue<>(hasLowestDist);
        prioQueue.add(start);

        while (true) {
            Point current = prioQueue.remove();
            if (current.equals(end)) {
                return dist.get(end);
            }

            // Check each neighbour
            List<Point> neighbours = neighboursStream(current)
                    .filter(p -> ArrayUtils.isInBounds(input, p.x, p.y))
                    .collect(Collectors.toList());
            for (Point neighbour : neighbours) {
                long altDist = dist.get(current) + input[neighbour.x][neighbour.y];
                if (altDist < dist.getOrDefault(neighbour, Long.MAX_VALUE)) {
                    dist.put(neighbour, altDist);
                    if (!prioQueue.contains(neighbour)) {
                        prioQueue.add(neighbour);
                    }
                }
            }
        }
    }

    private static Stream<Point> neighboursStream(Point p) {
        return Stream.of(new Point(p.x - 1, p.y),
                new Point(p.x, p.y - 1),
                new Point(p.x + 1, p.y),
                new Point(p.x, p.y + 1));
    }

    // Algorithm copied from the pseudo-code on Wikipedia: https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode
    // Not faster, did not yet find out why
    private static long aStarAlgorithm(long[][] input) {
        Point start = new Point(0, 0);
        Point end = new Point(input.length - 1, input[0].length - 1);

        Map<Point, Long> gScore = new HashMap<>();
        gScore.put(start, 0L);

        Map<Point, Long> fScore = new HashMap<>();
        fScore.put(start, hFunction(start, end));

        Comparator<Point> hasLowestFScore = Comparator.comparingLong(p -> fScore.getOrDefault(p, Long.MAX_VALUE));
        Queue<Point> openSet = new PriorityQueue<>(hasLowestFScore);
        openSet.add(start);

        while (!openSet.isEmpty()) {
            Point current = openSet.remove();
            if (current.equals(end)) {
                return gScore.get(end);
            }

            // Check each neighbour
            List<Point> neighbours = neighboursStream(current)
                    .filter(p -> ArrayUtils.isInBounds(input, p.x, p.y))
                    .collect(Collectors.toList());
            for (Point neighbour : neighbours) {
                long tentativeGScore = gScore.get(current) + input[neighbour.x][neighbour.y];
                if (tentativeGScore < gScore.getOrDefault(neighbour, Long.MAX_VALUE)) {
                    gScore.put(neighbour, tentativeGScore);
                    fScore.put(neighbour, tentativeGScore + hFunction(neighbour, end));
                    if (!openSet.contains(neighbour)) {
                        openSet.add(neighbour);
                    }
                }
            }
        }

        throw new IllegalStateException();
    }

    private static long hFunction(Point p, Point end) {
        return 2L * (end.x - p.x + end.y - p.y);
    }

    public static long part2(String resource) {
        long[][] input = ResourceHelper.getLongs2dArray(resource);
        long[][] expandedInput = new long[input.length * 5][input[0].length * 5];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input.length; j++) {
                for (int di = 0; di < 5; di++) {
                    for (int dj = 0; dj < 5; dj++) {
                        expandedInput[input.length * di + i][input[0].length * dj + j] =
                                ((input[i][j] - 1 + di + dj) % 9) + 1;
                    }
                }
            }
        }

        return dijkstraAlgorithm(expandedInput);
    }

}
