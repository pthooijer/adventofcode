package nl.pthooijer.aoc2021.day22;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Day22 {

    private static final Splitter SPLITTER = Splitter.onPattern("(on|off| x=|,y=|,z=|\\.\\.)").omitEmptyStrings();

    public static void main(String[] args) {
        String input = "aoc/2021/day/22/input.txt";
        String sample1 = "aoc/2021/day/22/sample-input1.txt";
        String sample2 = "aoc/2021/day/22/sample-input2.txt";

        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 sample2 answer: " + part2(sample2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final int CUBE_SIZE = 50;

    public static long part1(String resource) {
        List<Instruction> instructionList = parseInput(resource).stream()
                .filter(instr -> Arrays.stream(instr.cube.bounds)
                        .allMatch(interval -> interval.min >= -CUBE_SIZE && interval.max <= CUBE_SIZE))
                .collect(Collectors.toList());

        int arraySize = 2 * CUBE_SIZE + 1;
        int[][][] array = new int[arraySize][arraySize][arraySize]; // all 0
        for (Instruction instr : instructionList) {
            int value = instr.on ? 1 : 0;

            Interval[] bounds = instr.cube.bounds;
            for (int x = bounds[0].min; x <= bounds[0].max; x++) {
                for (int y = bounds[1].min; y <= bounds[1].max; y++) {
                    for (int z = bounds[2].min; z <= bounds[2].max; z++) {
                        array[x + CUBE_SIZE][y + CUBE_SIZE][z + CUBE_SIZE] = value;
                    }
                }
            }
        }

        return Arrays.stream(array)
                .flatMapToInt(arr2 -> Arrays.stream(arr2)
                        .flatMapToInt(Arrays::stream))
                .sum();
    }

    public static long part2(String resource) {
        List<Instruction> instructionList = parseInput(resource);
        List<Cube> onCubes = new LinkedList<>();
        for (Instruction instruction : instructionList) {
            Cube cube = instruction.getCube();

            // (1) remove cubes fully within the new cube
            onCubes.removeIf(cube::contains);

            // (2) split cubes intersecting with the new cube and remove the intersection
            List<Cube> replacementCubes = onCubes.stream()
                    .filter(cube::intersects)
                    .flatMap(onCube -> splitCube(onCube, cube).stream()
                            // (2b) Remove split cubes contained in the new cube
                            .filter(splitCube -> !cube.contains(splitCube)))
                    .collect(Collectors.toList());
            // (3) replace the pre-split cubes with the replacement cubes
            onCubes.removeIf(cube::intersects);
            onCubes.addAll(replacementCubes);

            // add the new cube if on. Now does not intersect with any other cube due to step (1) and step (2b)
            if (instruction.on) {
                onCubes.add(cube);
            }
        }

        return onCubes.stream()
                .mapToLong(Cube::size)
                .sum();
    }

    private static List<Cube> splitCube(Cube cube, Cube bounds) {
        List<Interval>[] splitIntervals = new List[3]; // array for dimensions, list for the intervals for that dimension
        for (int d = 0; d < 3; d++) {
            splitIntervals[d] = splitInterval(cube.bounds[d], bounds.bounds[d]);
        }

        List<Cube> result = new ArrayList<>();
        for (Interval xInterval : splitIntervals[0]) {
            for (Interval yInterval : splitIntervals[1]) {
                for (Interval zInterval : splitIntervals[2]) {
                    Cube splitCube = new Cube(new Interval[]{xInterval, yInterval, zInterval});
                    result.add(splitCube);
                }
            }
        }
        return result;
    }

    private static List<Interval> splitInterval(Interval toSplitIntv, Interval otherIntv) {
        if (!toSplitIntv.intersects(otherIntv)) {
            return Collections.singletonList(toSplitIntv);
        }
        List<Interval> result = new ArrayList<>(3);

        // lower interval
        if (toSplitIntv.min < otherIntv.min) {
            result.add(new Interval(toSplitIntv.min, otherIntv.min - 1));
        }
        // middle interval
        result.add(new Interval(Math.max(toSplitIntv.min, otherIntv.min), Math.min(toSplitIntv.max, otherIntv.max)));

        // higher interval
        if (toSplitIntv.max > otherIntv.max) {
            result.add(new Interval(otherIntv.max + 1, toSplitIntv.max));
        }

        return result;
    }

    private static List<Instruction> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day22::parseLine)
                .collect(Collectors.toList());
    }

    private static Instruction parseLine(String line) {
        boolean on = line.charAt(1) == 'n';
        int[] nrs = Streams.stream(SPLITTER.split(line))
                .mapToInt(Integer::parseInt)
                .toArray();

        Interval[] bounds = new Interval[]{
                new Interval(nrs[0], nrs[1]),
                new Interval(nrs[2], nrs[3]),
                new Interval(nrs[4], nrs[5])};
        return new Instruction(on, new Cube(bounds));
    }

    @Value
    private static class Interval { // closed
        int min;
        int max;

        public boolean contains(Interval smaller) {
            return this.min <= smaller.min && this.max >= smaller.max;
        }

        public boolean intersects(Interval other) {
            return this.max >= other.min && other.max >= this.min;
        }

        public long size() {
            return max - min + 1L;
        }

        public String toString() {
            return min + ".." + max;
        }
    }

    @Value
    private static class Cube {
        Interval[] bounds;

        public boolean contains(Cube smaller) {
            for (int d = 0; d < 3; d++) {
                if (!bounds[d].contains(smaller.bounds[d])) {
                    return false;
                }
            }
            return true;
        }

        public boolean intersects(Cube other) {
            for (int d = 0; d < 3; d++) {
                if (!bounds[d].intersects(other.bounds[d])) {
                    return false;
                }
            }
            return true;
        }

        public long size() {
            long size = 1L;
            for (int d = 0; d < 3; d++) {
                size *= bounds[d].size();
            }
            return size;
        }

        public String toString() {
            return String.format("x=%s,y=%s,z=%s", bounds[0], bounds[1], bounds[2]);
        }
    }

    @Value
    private static class Instruction {
        boolean on;
        Cube cube;

        public String toString() {
            return (on ? "on " : "off ") + cube.toString();
        }
    }

}
