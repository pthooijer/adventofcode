package nl.pthooijer.aoc2021.day4;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import lombok.ToString;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.SeparatorPolicy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static nl.pthooijer.util.ResourceHelper.getLinesIt;

public class Day4 {

    private static final Splitter COMMA_SPLITTER = Splitter.on(',');
    private static final Splitter SPACE_SPLITTER = Splitter.on(' ').omitEmptyStrings();
    private static final int BOARD_SIZE = 5;

    public static void main(String[] args) {
        String input = "aoc/2021/day/4/input.txt";
        String sample1 = "aoc/2021/day/4/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator =
                new JoinIterator<>(getLinesIt(resource).iterator(),
                        String::isEmpty,
                        SeparatorPolicy.DROP,
                        Collectors.toList());
        List<Integer> numbers = Streams.stream(COMMA_SPLITTER.split(joinIterator.next().get(0)))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        List<Board> boards = Streams.stream(joinIterator)
                .map(Day4::parseBoard)
                .collect(Collectors.toList());

        for (int number : numbers) {
            for (Board board : boards) {
                markNumber(board, number);
                if (hasBingo(board)) {
                    return calculateScore(board, number);
                }
            }
        }

        throw new IllegalArgumentException();
    }

    private static Board parseBoard(List<String> lines) {
        int[][] numbers = lines.stream()
                .map(line -> SPACE_SPLITTER.splitToList(line).stream()
                        .mapToInt(Integer::parseInt)
                        .toArray())
                .toArray(int[][]::new);
        return new Board(numbers);
    }

    private static void markNumber(Board board, int number) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board.numbers[i][j] == number) {
                    board.marks[i][j] = 1;
                    return;
                }
            }
        }
    }

    private static boolean hasBingo(Board board) {
        // horizontal
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board.marks[i][j] == 0) {
                    break;
                }
                if (j == BOARD_SIZE - 1) {
                    return true;
                }
            }
        }

        // vertical
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board.marks[j][i] == 0) {
                    break;
                }
                if (j == BOARD_SIZE - 1) {
                    return true;
                }
            }
        }
        return false;
    }

    private static int calculateScore(Board board, int lastNumber) {
        int sum = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board.marks[i][j] == 0) {
                    sum += board.numbers[i][j];
                }
            }
        }
        return sum * lastNumber;
    }

    @ToString
    private static class Board {
        final int[][] numbers;
        final int[][] marks = new int[BOARD_SIZE][BOARD_SIZE];

        public Board(int[][] numbers) {
            this.numbers = numbers;
        }
    }

    public static long part2(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator =
                new JoinIterator<>(getLinesIt(resource).iterator(),
                        String::isEmpty,
                        SeparatorPolicy.DROP,
                        Collectors.toList());
        List<Integer> numbers = Streams.stream(COMMA_SPLITTER.split(joinIterator.next().get(0)))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        List<Board> boards = Streams.stream(joinIterator)
                .map(Day4::parseBoard)
                .collect(Collectors.toList());

        for (int number : numbers) {
            List<Board> boardsToRemove = new ArrayList<>();
            for (Board board : boards) {
                markNumber(board, number);
                if (hasBingo(board)) {
                    if (boards.size() == 1) {
                        return calculateScore(board, number);
                    }
                    boardsToRemove.add(board);
                }
            }
            boards.removeAll(boardsToRemove);
        }

        throw new IllegalArgumentException();
    }

}
