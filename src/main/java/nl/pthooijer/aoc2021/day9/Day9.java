package nl.pthooijer.aoc2021.day9;

import nl.pthooijer.util.point.Point;
import nl.pthooijer.util.ResourceHelper;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static nl.pthooijer.util.ArrayUtils.isInBounds;

public class Day9 {

    public static void main(String[] args) {
        String input = "aoc/2021/day/9/input.txt";
        String sample1 = "aoc/2021/day/9/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long[][] input = ResourceHelper.getLongs2dArray(resource);

        return findMinima(input).stream()
                .mapToLong(p -> input[p.x][p.y] + 1)
                .sum();
    }

    private static Set<Point> findMinima(long[][] input) {
        return allPointsStream(input)
                .filter(p -> neighboursStream(p)
                        .allMatch(q -> !isInBounds(input, q.x, q.y) || input[q.x][q.y] > input[p.x][p.y]))
                .collect(Collectors.toSet());
    }

    private static Stream<Point> allPointsStream(long[][] input) {
        return IntStream.range(0, input.length)
                .mapToObj(i -> IntStream.range(0, input[0].length) // .flatMapToObj does not exist! :o
                        .mapToObj(j -> new Point(i, j)))
                .flatMap(Function.identity());
    }

    private static Stream<Point> neighboursStream(Point p) {
        return Stream.of(new Point(p.x - 1, p.y),
                new Point(p.x, p.y - 1),
                new Point(p.x + 1, p.y),
                new Point(p.x, p.y + 1));
    }

    public static long part2(String resource) {
        long[][] input = ResourceHelper.getLongs2dArray(resource);
        Set<Point> minima = findMinima(input);

        Map<Point, Long> basinCount = allPointsStream(input)
                .filter(p -> input[p.x][p.y] != 9)
                .map(p -> findBasin(input, p, minima))
                .collect(Collectors.groupingBy(p -> p, Collectors.counting()));

        return basinCount.values().stream()
                .sorted(Comparator.reverseOrder())
                .limit(3)
                .mapToLong(i -> i)
                .reduce(1L, (i, j) -> i * j);
    }

    private static Point findBasin(long[][] input, Point p, Set<Point> minima) {
        if (minima.contains(p)) {
            return p;
        }
        return findBasin(input, findLowestNeighbour(input, p), minima);
    }

    private static Point findLowestNeighbour(long[][] input, Point p) {
        return neighboursStream(p)
                .filter(q -> isInBounds(input, q.x, q.y))
                .min(Comparator.comparingLong(q -> input[q.x][q.y]))
                .orElse(p);
    }
}
