package nl.pthooijer.aoc2021.day5;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day5 {

    private static final Splitter COMMA_SPLITTER = Splitter.onPattern("(,| -> )");

    public static void main(String[] args) {
        String input = "aoc/2021/day/5/input.txt";
        String sample1 = "aoc/2021/day/5/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        List<Line> hvLines = ResourceHelper.getLinesStream(resource)
                .map(Day5::parseLine)
                .filter(line -> line.x1 == line.x2 || line.y1 == line.y2)
                .collect(Collectors.toList());
        int[][] grid = initializeGrid(hvLines);

        for (Line line : hvLines) {
            if (line.x1 == line.x2) {
                for (int i = Math.min(line.y1, line.y2); i <= Math.max(line.y1, line.y2); i++) {
                    grid[i][line.x1]++;
                }
            } else {
                for (int i = Math.min(line.x1, line.x2); i <= Math.max(line.x1, line.x2); i++) {
                    grid[line.y1][i]++;
                }
            }
        }

        return Arrays.stream(grid)
                .flatMapToInt(Arrays::stream)
                .filter(i -> i > 1)
                .count();
    }

    private static int[][] initializeGrid(List<Line> hvLines) {
        int maxCoordinate = hvLines.stream()
                .flatMapToInt(line -> IntStream.of(line.x1, line.y1, line.x2, line.y2))
                .max()
                .orElseThrow()
                + 1; // Coordinates are inclusive
        return new int[maxCoordinate][maxCoordinate];
    }

    private static Line parseLine(String s) {
        List<Integer> words = Streams.stream(COMMA_SPLITTER.split(s))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        return new Line(words.get(0), words.get(1), words.get(2), words.get(3));
    }

    @Value
    private static class Line {
        int x1;
        int y1;
        int x2;
        int y2;
    }

    public static long part2(String resource) {
        List<Line> hvLines = ResourceHelper.getLinesStream(resource)
                .map(Day5::parseLine)
                .collect(Collectors.toList());
        int[][] grid = initializeGrid(hvLines);

        for (Line line : hvLines) {
            if (line.x1 == line.x2) {
                for (int i = Math.min(line.y1, line.y2); i <= Math.max(line.y1, line.y2); i++) {
                    grid[line.x1][i]++;
                }
            } else if (line.y1 == line.y2) {
                for (int i = Math.min(line.x1, line.x2); i <= Math.max(line.x1, line.x2); i++) {
                    grid[i][line.y1]++;
                }
            } else { // diagonal 45 degrees
                int length = Math.abs(line.x1 - line.x2);
                int xSign = line.x1 > line.x2 ? -1 : 1;
                int ySign = line.y1 > line.y2 ? -1 : 1;
                for (int i = 0; i <= length; i++) {
                    grid[line.x1 + i * xSign][line.y1 + i * ySign]++;
                }
            }
        }

        return Arrays.stream(grid)
                .flatMapToInt(Arrays::stream)
                .filter(i -> i > 1)
                .count();
    }

    private static void printArray(int[][] arr) {
        for (int i = 0; i < arr[0].length; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < arr.length; j++) {
                sb.append(arr[j][i]).append(' ');
            }
            System.out.println(sb.toString());
        }
    }

}
