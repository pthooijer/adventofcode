package nl.pthooijer.aoc2021.day6;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;

public class Day6 {

    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    public static void main(String[] args) {
        String input = "aoc/2021/day/6/input.txt";
        String sample1 = "aoc/2021/day/6/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long[] startArr = new long[9];
        Streams.stream(COMMA_SPLITTER.split(ResourceHelper.getLinesIt(resource).iterator().next()))
                .mapToInt(Integer::parseInt)
                .forEach(i -> startArr[i]++);

        long[] counterArr = startArr;
        for (int i = 0; i < 80; i++) {
            counterArr = simulateDay(counterArr);
        }

        return Arrays.stream(counterArr)
                .sum();
    }

    private static long[] simulateDay(long[] prevDay) {
        long[] nextDay = new long[9];
        for (int i = 0; i <= 8; i++) {
            if (i == 0) {
                nextDay[8] = prevDay[0];
                nextDay[6] = prevDay[0];
            } else {
                nextDay[i - 1] += prevDay[i];
            }
        }
        return nextDay;
    }

    public static long part2(String resource) {
        long[] startArr = new long[9];
        Streams.stream(COMMA_SPLITTER.split(ResourceHelper.getLinesIt(resource).iterator().next()))
                .mapToInt(Integer::parseInt)
                .forEach(i -> startArr[i]++);

        long[] counterArr = startArr;
        for (int i = 0; i < 256; i++) {
            counterArr = simulateDay(counterArr);
        }

        return Arrays.stream(counterArr)
                .sum();
    }

}
