package nl.pthooijer.aoc2021.day14;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.HashMap;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;

public class Day14 {

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');

    public static void main(String[] args) {
        String input = "aoc/2021/day/14/input.txt";
        String sample1 = "aoc/2021/day/14/sample-input1.txt";
        System.out.println("Part 1 sample1 answer: " + part1(sample1));
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        String polymer = ResourceHelper.getLinesStream(resource).findFirst().orElseThrow();
        Map<String, Character> rules = parseRules(resource);

        int iterations = 10;
        for (int i = 0; i < iterations; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < polymer.length() - 1; j++) {
                String link = polymer.substring(j, j + 2);
                builder.append(link.charAt(0));
                if (rules.containsKey(link)) {
                    builder.append(rules.get(link));
                }
            }
            builder.append(polymer.charAt(polymer.length() - 1)); // add last char

            polymer = builder.toString();
        }

        Map<Integer, Long> charTally = polymer.chars()
                .boxed()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));

        LongSummaryStatistics minMax = charTally.values().stream()
                .collect(Collectors.summarizingLong(i -> i));
        return minMax.getMax() - minMax.getMin();
    }

    private static Map<String, Character> parseRules(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .skip(2)
                .map(SPACE_SPLITTER::splitToList)
                .collect(Collectors.toMap(list -> list.get(0), list -> list.get(2).charAt(0)));
    }

    public static long part2(String resource) {
        String polymer = ResourceHelper.getLinesStream(resource).findFirst().orElseThrow();
        Map<String, Character> rules = parseRules(resource);

        Map<String, Long> linkTally = new HashMap<>();
        for (int j = 0; j < polymer.length() - 1; j++) {
            String link = polymer.substring(j, j + 2);
            linkTally.merge(link, 1L, Long::sum);
        }

        int iterations = 40;
        for (int i = 0; i < iterations; i++) {
            linkTally = applyIteration(linkTally, rules);
        }

        // All characters appear twice in the linktally, except the two ends. Fixed by linking the two ends
        String twoEndsLink = "" + polymer.charAt(0) + polymer.charAt(polymer.length() - 1);
        linkTally.merge(twoEndsLink, 1L, Long::sum);

        // Count chars
        Map<Character, Long> charTallyDoubled = new HashMap<>();
        linkTally.forEach((link, amount) -> {
            charTallyDoubled.merge(link.charAt(0), amount, Long::sum);
            charTallyDoubled.merge(link.charAt(1), amount, Long::sum);
        });

        LongSummaryStatistics minMax = charTallyDoubled.values().stream()
                .mapToLong(i -> i / 2) // because all chars are in two links, and thus counted double
                .summaryStatistics();
        return minMax.getMax() - minMax.getMin();
    }

    private static Map<String, Long> applyIteration(Map<String, Long> linkTally, Map<String, Character> rules) {
        Map<String, Long> newLinkTally = new HashMap<>();
        linkTally.forEach((link, amount) -> {
            if (rules.containsKey(link)) {
                char inBetweenChar = rules.get(link);
                String newLeftLink = "" + link.charAt(0) + inBetweenChar;
                newLinkTally.merge(newLeftLink, amount, Long::sum);

                String newRightLink = "" + inBetweenChar + link.charAt(1);
                newLinkTally.merge(newRightLink, amount, Long::sum);
            } else {
                newLinkTally.merge(link, amount, Long::sum);
            }
        });
        return newLinkTally;
    }
}
