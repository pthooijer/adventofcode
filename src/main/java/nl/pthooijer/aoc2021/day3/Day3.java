package nl.pthooijer.aoc2021.day3;

import nl.pthooijer.util.ResourceHelper;

import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class Day3 {

    private static final int WORD_LENGTH = 12;

    public static void main(String[] args) {
        String input = "aoc/2021/day/3/input.txt";
        String sample1 = "aoc/2021/day/3/sample-input1.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 sample1 answer: " + part2(sample1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        long[] zeroesCount = new long[WORD_LENGTH];
        long[] onesCount = new long[WORD_LENGTH];
        for (String s : ResourceHelper.getLinesIt(resource)) {
            for (int i = 0; i < WORD_LENGTH; i++) {
                if (s.charAt(i) == '0') {
                    zeroesCount[i]++;
                } else {
                    onesCount[i]++;
                }
            }
        }

        StringBuilder gammaBin = new StringBuilder();
        StringBuilder epsilonBin = new StringBuilder();
        for (int i = 0; i < WORD_LENGTH; i++) {
            if (zeroesCount[i] > onesCount[i]) {
                gammaBin.append('0');
                epsilonBin.append('1');
            } else {
                gammaBin.append('1');
                epsilonBin.append('0');
            }
        }

        long gamma = Long.parseLong(gammaBin.toString(), 2);
        long epsilon = Long.parseLong(epsilonBin.toString(), 2);
        return gamma * epsilon;
    }

    public static long part2(String resource) {
        long oxygen = calculateLsr(resource, (count0, count1) -> count0 <= count1);
        long co2 = calculateLsr(resource, (count0, count1) -> count0 > count1);
        return oxygen * co2;
    }

    private static long calculateLsr(String resource, BiPredicate<Long, Long> counts01ToKeep1) {
        List<String> linesArr = ResourceHelper.getLinesList(resource);
        for (int i = 0; i < WORD_LENGTH; i++) {
            long zeroesCount = 0;
            long onesCount = 0;
            for (String s : linesArr) {
                if (s.charAt(i) == '0') {
                    zeroesCount++;
                } else {
                    onesCount++;
                }
            }

            int finalI = i;
            if (counts01ToKeep1.test(zeroesCount, onesCount)) {
                linesArr = linesArr.stream()
                        .filter(s -> s.charAt(finalI) == '1')
                        .collect(Collectors.toList());
            } else {
                linesArr = linesArr.stream()
                        .filter(s -> s.charAt(finalI) == '0')
                        .collect(Collectors.toList());
            }
            if (linesArr.size() == 1) {
                break;
            }
        }
        return Long.parseLong(linesArr.get(0), 2);
    }
}
