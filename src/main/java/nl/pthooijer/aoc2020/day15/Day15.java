package nl.pthooijer.aoc2020.day15;

import java.util.*;

public class Day15 {

    public static void main(String[] args) {
        System.out.println("Sample1 Part 1 answer (must be 436): " + part1(0, 3, 6));
        System.out.println("Sample2 Part 1 answer (must be 1): " + part1(1, 3, 2));
        System.out.println("Sample3 Part 1 answer (must be 10): " + part1(2, 1, 3));
        System.out.println("Sample4 Part 1 answer (must be 27): " + part1(1, 2, 3));
        System.out.println("Sample5 Part 1 answer (must be 78): " + part1(2, 3, 1));
        System.out.println("Sample6 Part 1 answer (must be 438): " + part1(3, 2, 1));
        System.out.println("Sample7 Part 1 answer (must be 1836): " + part1(3, 1, 2));
        System.out.println("Part 1 answer: " + part1(8, 0, 17, 4, 1, 12));

        System.out.println("Sample1 Part 2 answer (must be 175594): " + part2(0, 3, 6));
        System.out.println("Sample2 Part 2 answer (must be 2578): " + part2(1, 3, 2));
        System.out.println("Sample3 Part 2 answer (must be 3544142): " + part2(2, 1, 3));
        System.out.println("Sample4 Part 2 answer (must be 261214): " + part2(1, 2, 3));
        System.out.println("Sample5 Part 2 answer (must be 6895259): " + part2(2, 3, 1));
        System.out.println("Sample6 Part 2 answer (must be 18): " + part2(3, 2, 1));
        System.out.println("Sample7 Part 2 answer (must be 362): " + part2(3, 1, 2));
        System.out.println("Part 2 answer: " + part2(8, 0, 17, 4, 1, 12));
    }

    public static int part1(int... input) {
        return part(2020, input);
    }

    public static int part2(int... input) {
        return part(30000000, input);
    }

    public static int part(int iterations, int... input) {
        int lastNumber = input[0];
        int lastIndex = 0;

        Map<Integer, Integer> lastIndexMap = new HashMap<>();

        for (int i = 0; i < iterations; i++) {
            Integer tmpNewValue;
            if (i < input.length) {
                tmpNewValue = input[i];
            } else {
                tmpNewValue = lastIndexMap.get(lastNumber);
                tmpNewValue = tmpNewValue == null ? 0 : i - tmpNewValue - 1;
            }
            lastIndexMap.put(lastNumber, lastIndex);
            lastNumber = tmpNewValue;
            lastIndex = i;
        }
        return lastNumber;
    }
}
