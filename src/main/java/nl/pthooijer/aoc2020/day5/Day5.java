package nl.pthooijer.aoc2020.day5;

import com.google.common.math.*;
import lombok.*;

import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day5 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/5/input.txt";

        System.out.println("Test input 1 (should be row 70, column 7, seat ID 567): " + parseLine("BFFFBBFRRR"));
        System.out.println("Test input 2 (should be row 14, column 7, seat ID 119): " + parseLine("FFFBBBFRRR"));
        System.out.println("Test input 3 (should be row 102, column 4, seat ID 820): " + parseLine("BBFFBBFRLL"));

        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static int part1(String resource) {
        return getLinesStream(resource)
                .map(Day5::parseLine)
                .mapToInt(BoardingPass::getSeatId)
                .max()
                .orElseThrow(() -> new IllegalArgumentException("Empty input provided"));
    }

    // === Part 2

    public static int part2(String resource) {
        NavigableSet<Integer> set = getLinesStream(resource)
                .map(Day5::parseLine)
                .map(BoardingPass::getSeatId)
                .collect(Collectors.toCollection(TreeSet::new));

        Integer prev = set.first();
        while (true) {
            Integer next = set.higher(prev);
            if (next - prev != 1) {
                return prev + 1;
            }
            prev = next;
        }
    }

    // === Parsing

    public static BoardingPass parseLine(String line) {
        int row = getId(line.substring(0, 7), 'F', 'B');
        int column = getId(line.substring(7), 'L', 'R');
        int seatId = getSeatId(row, column);
        return new BoardingPass(line, row, column, seatId);
    }

    // I know, it's binary search instead of interpreting the input as a binary number
    public static int getId(String code, char lowChar, char highChar) {
        int length = code.length();
        int min = 0;
        int max = IntMath.pow(2, length) - 1;
        int step = IntMath.pow(2, length - 1);
        for (int i = 0; i < length; i++) {
            char c = code.charAt(i);
            if (c == lowChar) {
                max = min + step - 1;
            } else if (c == highChar) {
                min = min + step;
            } else {
                throw new IllegalArgumentException("Unknown character " + c);
            }
            step /= 2;
        }
        if (min == max) {
            return min;
        } else {
            throw new IllegalStateException("Something went wrong");
        }
    }

    public static int getSeatId(int row, int column) {
        return row * 8 + column;
    }

    @Value
    public static class BoardingPass {
        String line;
        int row;
        int column;
        int seatId;
    }

}
