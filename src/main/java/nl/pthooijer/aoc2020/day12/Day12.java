package nl.pthooijer.aoc2020.day12;

import com.google.common.math.*;
import lombok.*;

import java.math.*;
import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;


public class Day12 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/12/sample-input1.txt";
        String input = "aoc/2020/day/12/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 25): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 286): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        List<String> input = parseInput(resource);
        State state = new State();
        for (String line : input) {
            char instr = line.charAt(0);
            int arg = Integer.parseInt(line.substring(1));
            switch (instr) {
                case 'N':
                case 'S':
                case 'W':
                case 'E':
                    move(state, instr, arg);
                    break;
                case 'F':
                    move(state, DIRS[state.face], arg);
                    break;
                case 'R':
                    turn(state, arg);
                    break;
                case 'L':
                    turn(state, -arg);
                    break;
            }
        }
        System.out.println(state);
        return Math.abs(state.ns) + Math.abs(state.ew);
    }

    public static char[] DIRS = new char[]{'E', 'S', 'W', 'N'};

    public static void turn(State state, int arg) {
        int cwSteps = IntMath.divide(arg, 90, RoundingMode.UNNECESSARY);  // To check if all degrees are multiples of 90
        state.face = Math.floorMod(state.face + cwSteps, 4);
    }

    public static void move(State state, char dir, int arg) {
        switch (dir) {
            case 'N':
                state.ns += arg;
                break;
            case 'S':
                state.ns -= arg;
                break;
            case 'W':
                state.ew -= arg;
                break;
            case 'E':
                state.ew += arg;
                break;
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class State {
        int ns = 0;
        int ew = 0;
        int face = 0;
    }

    // === Part 2

    public static long part2(String resource) {
        List<String> input = parseInput(resource);
        State waypoint = new State(1, 10, 0);
        State ship = new State(0, 0, 0);
        for (String line : input) {
            char instr = line.charAt(0);
            int arg = Integer.parseInt(line.substring(1));
            switch (instr) {
                case 'N':
                case 'S':
                case 'W':
                case 'E':
                    move(waypoint, instr, arg);
                    break;
                case 'F':
                    moveToWaypoint(ship, waypoint, arg);
                    break;
                case 'R':
                    turnWaypoint(waypoint, arg);
                    break;
                case 'L':
                    turnWaypoint(waypoint, -arg);
                    break;
            }
        }
        return Math.abs(ship.ns) + Math.abs(ship.ew);
    }

    public static void moveToWaypoint(State ship, State waypoint, int arg) {
        ship.ns += waypoint.ns * arg;
        ship.ew += waypoint.ew * arg;
    }

    public static void turnWaypoint(State state, int arg) {
        int cwSteps = IntMath.divide(arg, 90, RoundingMode.UNNECESSARY);
        int realSteps = Math.floorMod(cwSteps, 4);
        for (int i = 0; i < realSteps; i++) {
            int oldNs = state.ns;
            int oldEw = state.ew;
            state.ns = -oldEw;
            state.ew = oldNs;
        }
    }

    // === Parsing

    public static List<String> parseInput(String resource) {
        return getLinesStream(resource)
                .collect(Collectors.toList());
    }

}
