package nl.pthooijer.aoc2020.day9;

import com.google.common.collect.*;

import java.util.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day9 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/9/sample-input1.txt";
        String input = "aoc/2020/day/9/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 127): " + part1(sampleInput1, 5));
        System.out.println("Part 1 answer: " + part1(input, 25));

        System.out.println("Sample1 Part 2 answer (must be 62): " + part2(sampleInput1, 127));
        System.out.println("Part 2 answer: " + part2(input, 85848519));
    }

    // === Part 1

    public static long part1(String resource, int preambleLength) {
        long[] input = getLongsArray(resource);
        Table<Long, Long, Long> table = initializePreamble(input, preambleLength);
        for (int i = preambleLength; i < input.length; i++) {
            long n = input[i];
            if (!table.containsValue(n)) {
                return n;
            }

            // Update the table
            long nToRemove = input[i - preambleLength];
            table.rowMap().remove(nToRemove);
            table.columnMap().remove(nToRemove);

            for (int j = i - preambleLength + 1; j < i; j++) {
                long nInPreamble = input[j];
                table.put(nInPreamble, n, nInPreamble + n);
                table.put(n, nInPreamble, nInPreamble + n);
            }
        }
        throw new IllegalArgumentException("Input contains no answer");
    }

    private static Table<Long, Long, Long> initializePreamble(long[] input, int preambleLength) {
        HashBasedTable<Long, Long, Long> table = HashBasedTable.create(preambleLength, preambleLength);
        for (int i1 = 0; i1 < preambleLength; i1++) {
            long n1 = input[i1];
            for (int i2 = 0; i2 < preambleLength; i2++) {
                if (i1 != i2) {
                    long n2 = input[i2];
                    table.put(n1, n2, n1 + n2);
                }
            }
        }
        return table;
    }

    // === Part 2

    public static long part2(String resource, long nrToSearch) {
        long[] input = getLongsArray(resource);
        Deque<Long> contiguousSet = new LinkedList<>();
        contiguousSet.add(input[0]);
        int iEnd = 0;
        long sum = input[0];
        while (true) {
            if(sum == nrToSearch) {
                if(contiguousSet.size() <= 1) {
                    throw new UnsupportedOperationException("To be implemented");
                }
                long min = Collections.min(contiguousSet);
                long max = Collections.max(contiguousSet);
                return min + max;
            } else if (sum <= nrToSearch) {
                iEnd++;
                contiguousSet.addLast(input[iEnd]);
                sum += input[iEnd];
            } else {
                sum -= contiguousSet.removeFirst();
            }
        }
    }

}
