package nl.pthooijer.aoc2020.day1;

import java.util.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day1 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/1/input.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        for (PrimitiveIterator.OfLong it1 = getLongIt(resource); it1.hasNext(); ) {
            long l1 = it1.nextLong();
            for (PrimitiveIterator.OfLong it2 = getLongIt(resource); it2.hasNext(); ) {
                long l2 = it2.nextLong();
                long sum = l1 + l2;
                if (sum == 2020) {
                    return l1 * l2;
                }
            }
        }
        throw new IllegalArgumentException("Input does not contain an answer");
    }

    public static long part2(String resource) {
        for (PrimitiveIterator.OfLong it1 = getLongIt(resource); it1.hasNext(); ) {
            long l1 = it1.nextLong();
            for (PrimitiveIterator.OfLong it2 = getLongIt(resource); it2.hasNext(); ) {
                long l2 = it2.nextLong();
                for (PrimitiveIterator.OfLong it3 = getLongIt(resource); it3.hasNext(); ) {
                    long l3 = it3.nextLong();
                    long sum = l1 + l2 + l3;
                    if (sum == 2020) {
                        return l1 * l2 * l3;
                    }
                }
            }
        }
        throw new IllegalArgumentException("Input does not contain an answer");
    }

}
