package nl.pthooijer.aoc2020.day17;

import nl.pthooijer.util.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day17 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/17/sample-input1.txt";
        String input = "aoc/2020/day/17/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 112): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 848): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        int cycles = 6;
        char[][] startSlice = getChars2dArray(resource);

        // Fill the starting universe
        int xSize = startSlice.length;
        int ySize = startSlice[0].length;
        char[][][] universe = new char[xSize][ySize][1];
        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                universe[x][y][0] = startSlice[x][y];
            }
        }

        // Advance N cycles
        for (int i = 0; i < cycles; i++) {
            universe = advance(universe);
        }

        // Count the number of occupied cubes
        long count = 0;
        for (int x = 0; x < universe.length; x++) {
            for (int y = 0; y < universe[0].length; y++) {
                for (int z = 0; z < universe[0][0].length; z++) {
                    if (universe[x][y][z] == '#') {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    public static char[][][] advance(char[][][] seed) {
        // Result array is created that is 1 larger in every direction
        int xSize = seed.length + 2;
        int ySize = seed.length + 2;
        int zSize = seed.length + 2;
        char[][][] result = new char[xSize][ySize][zSize];

        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                for (int z = 0; z < zSize; z++) {
                    int activeNeighbours = countActiveNeighbours(seed, x - 1, y - 1, z - 1);
                    if (ArrayUtils.isInBounds(seed, x - 1, y - 1, z - 1) &&
                            seed[x - 1][y - 1][z - 1] == '#') {
                        result[x][y][z] = activeNeighbours == 2 || activeNeighbours == 3 ? '#' : '.';
                    } else {
                        result[x][y][z] = activeNeighbours == 3 ? '#' : '.';
                    }
                }
            }
        }
        return result;
    }

    public static int countActiveNeighbours(char[][][] array, int x, int y, int z) {
        int count = 0;
        for (int xn = -1; xn <= 1; xn++) {
            for (int yn = -1; yn <= 1; yn++) {
                for (int zn = -1; zn <= 1; zn++) {
                    if ((xn != 0 || yn != 0 || zn != 0) && // Exclude the cube itself
                            ArrayUtils.isInBounds(array, x + xn, y + yn, z + zn) &&
                            array[x + xn][y + yn][z + zn] == '#') {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    // === Part 2
    // Is a copy of part 1, no generalizations today.

    public static long part2(String resource) {
        int cycles = 6;
        char[][] startSlice = getChars2dArray(resource);

        // Fill the starting universe
        int xSize = startSlice.length;
        int ySize = startSlice[0].length;
        char[][][][] universe = new char[xSize][ySize][1][1];
        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                universe[x][y][0][0] = startSlice[x][y];
            }
        }

        // Advance N cycles
        for (int i = 0; i < cycles; i++) {
            universe = advance(universe);
        }

        // Count the number of occupied cubes
        long count = 0;
        for (int x = 0; x < universe.length; x++) {
            for (int y = 0; y < universe[0].length; y++) {
                for (int z = 0; z < universe[0][0].length; z++) {
                    for (int w = 0; w < universe[0][0][0].length; w++) {
                        if (universe[x][y][z][w] == '#') {
                            count++;
                        }
                    }
                }
            }
        }

        return count;
    }

    public static char[][][][] advance(char[][][][] seed) {
        // Result array is created that is 1 larger in every direction
        int xSize = seed.length + 2;
        int ySize = seed.length + 2;
        int zSize = seed.length + 2;
        int wSize = seed.length + 2;
        char[][][][] result = new char[xSize][ySize][zSize][wSize];

        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                for (int z = 0; z < zSize; z++) {
                    for (int w = 0; w < wSize; w++) {
                        int activeNeighbours = countActiveNeighbours(seed, x - 1, y - 1, z - 1, w - 1);
                        if (ArrayUtils.isInBounds(seed, x - 1, y - 1, z - 1, w - 1) &&
                                seed[x - 1][y - 1][z - 1][w - 1] == '#') {
                            result[x][y][z][w] = activeNeighbours == 2 || activeNeighbours == 3 ? '#' : '.';
                        } else {
                            result[x][y][z][w] = activeNeighbours == 3 ? '#' : '.';
                        }
                    }
                }
            }
        }
        return result;
    }

    public static int countActiveNeighbours(char[][][][] array, int x, int y, int z, int w) {
        int count = 0;
        for (int xn = -1; xn <= 1; xn++) {
            for (int yn = -1; yn <= 1; yn++) {
                for (int zn = -1; zn <= 1; zn++) {
                    for (int wn = -1; wn <= 1; wn++) {
                        if ((xn != 0 || yn != 0 || zn != 0 || wn != 0) && // Exclude the cube itself
                                ArrayUtils.isInBounds(array, x + xn, y + yn, z + zn, w + wn) &&
                                array[x + xn][y + yn][z + zn][w + wn] == '#') {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

}
