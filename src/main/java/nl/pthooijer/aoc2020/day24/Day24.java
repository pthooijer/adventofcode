package nl.pthooijer.aoc2020.day24;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static nl.pthooijer.util.ResourceHelper.getLinesStream;

public class Day24 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/24/sample-input1.txt";
        String input = "aoc/2020/day/24/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 10): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 2208): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        return getStartTiles(resource).size(); // In a separate method because you need the output for part 2
    }

    public static Set<Hex> getStartTiles(String resource) {
        List<Hex> input = parseInput(resource);
        Map<Hex, Long> timesFlippedMap = input.stream()
                .collect(Collectors.groupingBy(hc -> hc, Collectors.counting()));

        return timesFlippedMap.entrySet().stream()
                .filter(entry -> entry.getValue() % 2 == 1) // Get all tiles flipped an odd amount of times
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    // === Part 2

    public static long part2(String resource) {
        Set<Hex> tiles = getStartTiles(resource);
        for (int i = 0; i < 100; i++) {
            tiles = advanceOneStep(tiles);
        }

        return tiles.size();
    }

    public static Set<Hex> advanceOneStep(Set<Hex> currentStep) {
        Set<Hex> nextStep = new HashSet<>();

        long maxDistance = currentStep.stream()
                .mapToLong(Hex.ORIGIN::distance)
                .max()
                .orElse(0);

        // Iterate over all hexes with distance of maxDistance + 1:
        long range = maxDistance + 1;
        for (long x = -range; x <= range; x++) {
            for (long y = -range; y <= range + 1; y++) {
                Hex hex = new Hex(x, y, -x - y);
                int activeNeighbours = countActiveNeighbours(hex, currentStep);
                if (activeNeighbours == 2 || (activeNeighbours == 1 && currentStep.contains(hex))) {
                    nextStep.add(hex);
                }
            }
        }

        return nextStep;
    }

    public static int countActiveNeighbours(Hex hex, Set<Hex> environment) {
        int count = 0;
        for (Hex direction : Hex.DIRECTIONS) {
            if (environment.contains(hex.plus(direction))) {
                count++;
            }
        }
        return count;
    }

    // === Parsing

    public static List<Hex> parseInput(String resource) {
        return getLinesStream(resource)
                .map(Day24::parseLine)
                .collect(Collectors.toList());
    }

    public static Hex parseLine(String line) {
        Hex output = Hex.ORIGIN;
        Character prevChar = null;
        for (char c : line.toCharArray()) {
            switch (c) {
                case 's', 'n' -> prevChar = c;
                case 'e' -> {
                    if (prevChar == null) {
                        output = output.plus(Hex.E);
                    } else if (prevChar == 's') {
                        output = output.plus(Hex.SE);
                    } else {
                        output = output.plus(Hex.NE);
                    }
                    prevChar = null;
                }
                case 'w' -> {
                    if (prevChar == null) {
                        output = output.plus(Hex.W);
                    } else if (prevChar == 's') {
                        output = output.plus(Hex.SW);
                    } else {
                        output = output.plus(Hex.NW);
                    }
                    prevChar = null;
                }
            }
        }
        return output;
    }

    @EqualsAndHashCode
    public static final class Hex { // Using Cubic Coordinates, where x + y + z = 0
        public static final Hex ORIGIN = new Hex(0, 0, 0);
        public static final Hex E = new Hex(1, -1, 0);
        public static final Hex SE = new Hex(0, -1, 1);
        public static final Hex SW = new Hex(-1, 0, 1);
        public static final Hex W = new Hex(-1, 1, 0);
        public static final Hex NW = new Hex(0, 1, -1);
        public static final Hex NE = new Hex(1, 0, -1);
        public static final Hex[] DIRECTIONS = new Hex[]{E, SE, SW, W, NW, NE};

        private final long x;
        private final long y;
        private final long z;

        public Hex(long x, long y, long z) {
            checkZeroSum(x, y, z);
            this.x = x;
            this.y = y;
            this.z = z;
        }

        private static void checkZeroSum(long x, long y, long z) {
            if (x + y + z != 0) {
                throw new IllegalArgumentException(String.format(
                        "Sum of coordinates must be zero. x=%d y=%d z=%d", x, y, z));
            }
        }

        public Hex plus(Hex other) {
            return new Hex(this.x + other.x, this.y + other.y, this.z + other.z);
        }

        public long distance(Hex other) {
            return (Math.abs(this.x - other.x) + Math.abs(this.y - other.y) + Math.abs(this.z - other.z)) / 2;
        }
    }
}
