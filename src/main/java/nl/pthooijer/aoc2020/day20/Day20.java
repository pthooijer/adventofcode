package nl.pthooijer.aoc2020.day20;

import com.google.common.collect.Streams;
import com.google.common.math.*;
import nl.pthooijer.util.*;

import java.math.*;
import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.aoc2020.day20.Day20.D4Action.*;
import static nl.pthooijer.util.ResourceHelper.*;

public class Day20 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/20/sample-input1.txt";
        String input = "aoc/2020/day/20/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 20899048083289): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be 273): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        Tile[][] result = part1Result(resource);

        int length = result.length;
        return result[0][0].id * result[0][length - 1].id
                * result[length - 1][0].id * result[length - 1][length - 1].id;
    }

    public static Tile[][] part1Result(String resource) {
        Map<Long, Tile> input = parseInput(resource);
        int squareLength = IntMath.sqrt(input.size(), RoundingMode.UNNECESSARY); // Because the output image is a square
        Tile[][] resultImage = new Tile[squareLength][squareLength];

        Set<Long> availableTiles = new HashSet<>(input.keySet());
        return tryFit(resultImage, 0, 0, availableTiles, input);
    }

    // For each tile not yet placed, try to place the tile in the (freeI, freeJ) location in incompleteArray, in all 8 orientations.
    // For each tile and orientation that fits, call this method again for the next (freeI, freeJ) location,
    // with the found tile removed from availableTiles.
    // If none fits, go 1 step back in the recursion by returning null.
    public static Tile[][] tryFit(Tile[][] incompleteArray, int freeI, int freeJ,
                                  Set<Long> availableTiles, Map<Long, Tile> tileMap) {
        int length = incompleteArray.length;
        for (long tileId : availableTiles) {
            Tile tile = tileMap.get(tileId);
            for (D4Action action : D4Action.values()) {
                Tile transformedTile = tile.applyAction(action);
                if (doesFit(incompleteArray, transformedTile, freeI, freeJ)) {
                    // Place the tile that fits
                    Tile[][] newArray = copyArray(incompleteArray);
                    newArray[freeI][freeJ] = transformedTile;

                    // Remove it from the available tiles
                    Set<Long> newAvailableTiles = new HashSet<>(availableTiles);
                    newAvailableTiles.remove(tileId);

                    // Increase the freeI and freeJ indices
                    int newI = freeI + 1;
                    int newJ = freeJ;
                    if (newI >= length) {
                        newI = 0;
                        newJ++;
                    }
                    if (newJ < length) {
                        // Try to fit the next tile
                        Tile[][] result = tryFit(newArray, newI, newJ, newAvailableTiles, tileMap);
                        if (result != null) { // If a solution was found, return it one recursion up.
                            return result;
                        } // If no solution was found for the next tiles, continue iterating.
                    } else { // No more free tiles left. Solution found!
                        return newArray;
                    }
                }
            }
        }
        return null; // Nothing fits, return null.
    }

    // You only have to check north and west, since the other directions aren't filled in yet
    private static boolean doesFit(Tile[][] incompleteArray, Tile tile, int freeI, int freeJ) {
        // north
        int i = freeI - 1;
        int j = freeJ;
        if (ArrayUtils.isInBounds(incompleteArray, i, j)) {
            char[] tileTop = tile.row(0);

            Tile northTile = incompleteArray[i][j];
            char[] northBottom = northTile.row(northTile.length - 1);
            if (!Arrays.equals(tileTop, northBottom)) {
                return false;
            }
        }

        // west
        i = freeI;
        j = freeJ - 1;
        if (ArrayUtils.isInBounds(incompleteArray, i, j)) {
            char[] tileLeft = tile.column(0);

            Tile westTile = incompleteArray[i][j];
            char[] westRight = westTile.column(westTile.length - 1);
            if (!Arrays.equals(tileLeft, westRight)) {
                return false;
            }
        }

        return true;
    }

    public static Tile[][] copyArray(Tile[][] array) {
        int length = array.length;
        Tile[][] result = new Tile[length][length];
        for (int i = 0; i < length; i++) {
            System.arraycopy(array[i], 0, result[i], 0, length);
        }
        return result;
    }

    public static Map<Long, Tile> parseInput(String resource) {
        Collector<String, ?, Tile> tileCollector =
                Collectors.collectingAndThen(Collectors.toList(), list -> {
                    long id = Long.parseLong(list.get(0).substring(5, 9));
                    char[][] array = list.stream()
                            .skip(1)
                            .map(String::toCharArray)
                            .toArray(char[][]::new);
                    return new Tile(id, array);
                });
        JoinIterator<String, ?, Tile> joinIterator = new JoinIterator<>(
                getLinesStream(resource).iterator(),
                String::isEmpty,
                SeparatorPolicy.DROP,
                tileCollector);
        return Streams.stream(joinIterator)
                .collect(Collectors.toMap(tile -> tile.id, tile -> tile));
    }

    // D4 = The Dihedral Group of order 8 (the symmetry group of a square):
    // {@link https://en.wikipedia.org/wiki/Examples_of_groups#dihedral_group_of_order_8}
    // Rotations are counter-clockwise
    public enum D4Action {
        IDENTITY,
        ROTATE_90,
        ROTATE_180,
        ROTATE_270,
        FLIP_HORIZONTAL,
        FLIP_VERTICAL,
        FLIP_DIAG_NW_SE,
        FLIP_DIAG_NE_SW;
    }

    public static class Tile {
        public final long id;
        public final char[][] array;
        public final int length;
        public Map<D4Action, Tile> actionTileMap = null; // Only generate this map when needed, otherwise you have an infinite recursion.

        public Tile(long id, char[][] array) {
            this.id = id;
            this.array = array;
            this.length = array.length;
        }

        // Returns a new Tile with the same id but the array transformed according to the given D4Action
        private Tile applyAction(D4Action action) {
            if (actionTileMap == null) {
                generateActionTileMap();
            }
            return actionTileMap.get(action);
        }

        private void generateActionTileMap() {
            actionTileMap = new EnumMap<>(D4Action.class);
            actionTileMap.put(IDENTITY, this);
            char[][] arr90 = rotate90(this.array);
            actionTileMap.put(ROTATE_90, new Tile(id, arr90));
            char[][] arr180 = rotate90(arr90);
            actionTileMap.put(ROTATE_180, new Tile(id, arr180));
            char[][] arr270 = rotate90(arr180);
            actionTileMap.put(ROTATE_270, new Tile(id, arr270));

            actionTileMap.put(FLIP_HORIZONTAL, new Tile(id, flipHorizontal(this.array)));
            actionTileMap.put(FLIP_VERTICAL, new Tile(id, flipHorizontal(arr180)));
            actionTileMap.put(FLIP_DIAG_NW_SE, new Tile(id, flipHorizontal(arr90)));
            actionTileMap.put(FLIP_DIAG_NE_SW, new Tile(id, flipHorizontal(arr270)));
        }

        // Counter-clockwise
        public static char[][] rotate90(char[][] array) {
            int length = array.length;
            char[][] result = new char[length][length];
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    result[i][j] = array[j][length - i - 1];
                }
            }
            return result;
        }

        public static char[][] flipHorizontal(char[][] array) {
            int length = array.length;
            char[][] result = new char[length][length];
            for (int i = 0; i < length; i++) {
                System.arraycopy(array[length - i - 1], 0, result[i], 0, length);
            }
            return result;
        }

        public char[] row(int index) {
            return this.array[index];
        }

        public char[] column(int index) {
            char[] result = new char[length];
            for (int i = 0; i < length; i++) {
                result[i] = this.array[i][index];
            }
            return result;
        }
    }

    // === Part 2

    public static long part2(String resource) {
        Tile[][] part1Solution = part1Result(resource);

        // Join the images together, ignoring the boundaries
        int adjTileLength = part1Solution[0][0].array.length - 2;
        int totalLength = adjTileLength * part1Solution.length;
        char[][] totalArr = new char[totalLength][totalLength];
        for (int i = 0; i < totalLength; i++) {
            for (int j = 0; j < totalLength; j++) {
                totalArr[i][j] = part1Solution
                        [IntMath.divide(i, adjTileLength, RoundingMode.FLOOR)]
                        [IntMath.divide(j, adjTileLength, RoundingMode.FLOOR)]
                        .array
                        [(i % adjTileLength) + 1]
                        [(j % adjTileLength) + 1]; // what a mess :D
            }
        }
        Tile image = new Tile(1, totalArr); // In a Tile object so we can use the D4Action methods
        int[][] seaMonsterPos = new int[][]{{0, 0}, {1, 1},
                {1, 4}, {0, 5}, {0, 6}, {1, 7},
                {1, 10}, {0, 11}, {0, 12}, {1, 13},
                {1, 16}, {0, 17}, {0, 18}, {0, 19}, {-1, 18}}; // newline for every sea monster segment

        long nrOfSeaMonsters = 0;
        for (D4Action action : D4Action.values()) {
            char[][] imageArr = image.applyAction(action).array;
            // check for every starting position
            for (int i = 0; i < totalLength; i++) {
                for (int j = 0; j < totalLength; j++) {
                    if (hasSeaMonsterAt(imageArr, seaMonsterPos, i, j)) {
                        nrOfSeaMonsters++;
                    }
                }
            }
        }

        long nrOfRoughWater = ArrayUtils.countChars(totalArr, '#');
        return nrOfRoughWater - (nrOfSeaMonsters * seaMonsterPos.length);
    }

    private static boolean hasSeaMonsterAt(char[][] image, int[][] seaMonster, int i, int j) {
        for (int[] point : seaMonster) {
            int pi = i + point[0];
            int pj = j + point[1];
            if (!ArrayUtils.isInBounds(image, pi, pj) || image[pi][pj] != '#') {
                return false;
            }
        }
        return true;
    }
}
