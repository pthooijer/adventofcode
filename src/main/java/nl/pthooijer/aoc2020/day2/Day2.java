package nl.pthooijer.aoc2020.day2;

import lombok.*;

import java.util.regex.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day2 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/2/input.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        return getLinesStream(resource)
                .map(Day2::parseLine)
                .filter(Day2::isValid1)
                .count();
    }

    private static boolean isValid1(Password password) {
        long count = 0;
        for (char c : password.password.toCharArray()) {
            if (c == password.c) {
                count++;
            }
        }
        return count >= password.l1 && count <= password.l2;
    }

    // === Part 2

    public static long part2(String resource) {
        return getLinesStream(resource)
                .map(Day2::parseLine)
                .filter(Day2::isValid2)
                .count();
    }

    private static boolean isValid2(Password password) {
        boolean valid1 = password.password.charAt(password.l1 - 1) == password.c;
        boolean valid2 = password.password.charAt(password.l2 - 1) == password.c;
        return valid1 ^ valid2;
    }

    // === Parsing

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^([0-9]+)-([0-9]+) (.): (.*)");

    private static Password parseLine(String line) {
        Matcher matcher = PASSWORD_PATTERN.matcher(line);
        matcher.matches();
        return new Password(
                Integer.parseInt(matcher.group(1)),
                Integer.parseInt(matcher.group(2)),
                matcher.group(3).charAt(0),
                matcher.group(4));
    }

    @Value
    public static class Password {
        int l1;
        int l2;
        char c;
        String password;
    }

}
