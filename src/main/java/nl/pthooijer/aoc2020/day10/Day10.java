package nl.pthooijer.aoc2020.day10;

import nl.pthooijer.util.*;

import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day10 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/10/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/10/sample-input2.txt";
        String input = "aoc/2020/day/10/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 35): " + part1(sampleInput1));
        System.out.println("Sample1 Part 1 answer (must be 35): " + part1(sampleInput2));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 8): " + part2(sampleInput1));
        System.out.println("Sample1 Part 2 answer (must be 19208): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        List<Long> input = getLongsStream(resource)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        Map<Long, Long> countDifMap = new HashMap<>();
        for (int i = 0; i < input.size() - 1; i++) {
            long dif = input.get(i + 1) - input.get(i);
            countDifMap.merge(dif, 1L, (mapL, l1) -> mapL + 1);
        }
        countDifMap.merge(input.get(0), 1L, (mapL, l1) -> mapL + 1);
        countDifMap.merge(3L, 1L, (mapL, l1) -> mapL + 1);


        return countDifMap.get(1L) * countDifMap.get(3L);
    }

    public static long part2(String resource) {
        NavigableSet<Long> input = getLongsStream(resource)
                .boxed()
                .collect(Collectors.toCollection(TreeSet::new));
        input.add(0L);
        input.add(input.last() + 3L);

        NavigableMap<Long, Set<Long>> canConnectToMap = input.stream()
                .collect(Collectors.toMap(a -> a, a -> input.subSet(a + 1, true, a + 3, true),
                        StreamUtils.throwingMerger(), TreeMap::new));

        SortedMap<Long, Long> nrOfConnectionsMap = new TreeMap<>();
        for (Map.Entry<Long, Set<Long>> entry : canConnectToMap.descendingMap().entrySet()) {
            long adapter = entry.getKey();
            Set<Long> canConnectTo = entry.getValue();
            long value = 0L;
            if (canConnectTo.isEmpty()) {
                value = 1;
            } else {
                for (long cAdapter : canConnectTo) {
                    value += nrOfConnectionsMap.get(cAdapter);
                }
            }
            nrOfConnectionsMap.put(adapter, value);
        }

        return nrOfConnectionsMap.get(0L);
    }


}
