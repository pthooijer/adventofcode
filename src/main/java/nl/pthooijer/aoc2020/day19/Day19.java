package nl.pthooijer.aoc2020.day19;

import com.google.common.base.*;
import lombok.*;
import nl.pthooijer.util.*;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day19 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/19/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/19/sample-input2.txt";
        String input = "aoc/2020/day/19/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 2): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 1 answer (must be 3): " + part1(sampleInput2));
        System.out.println("Sample2 Part 2 answer (must be 12): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        Input input = parseInput(resource);

        // Collect all rules without dependencies
        Set<Integer> finishedRules = new HashSet<>(input.ruleMap.size());
        input.ruleMap.entrySet().stream()
                .filter(entry -> entry.getValue().indexOf('"') >= 0)
                .map(Map.Entry::getKey)
                .forEach(finishedRules::add);

        // Prepare the input rules, see method for details
        input.ruleMap.replaceAll((i, rule) -> prepareIncompleteRule(rule));

        // First iteration starts with the rules without dependencies
        List<Integer> rulesToInject = new ArrayList<>(finishedRules);
        while (finishedRules.size() < input.ruleMap.size()) {
            List<Integer> nextRulesToInject = new ArrayList<>();
            for (Integer index : rulesToInject) {

                String stringToReplace = "<" + index + ">"; // We search for this string to replace
                for (Map.Entry<Integer, String> rule : input.ruleMap.entrySet()) {
                    if (!finishedRules.contains(rule.getKey())) {
                        // Replace the number with the finished rule.
                        // Ex: for index = 4 with value "aba", "<4><5>|<5><4>" becomes "aba<5>|<5>aba"
                        rule.setValue(rule.getValue().replace(stringToReplace, input.ruleMap.get(index)));

                        if (isFinished(rule.getValue())) {
                            finishedRules.add(rule.getKey());
                            nextRulesToInject.add(rule.getKey());
                        }
                    }
                }
            }
            if (nextRulesToInject.isEmpty()) {
                throw new IllegalStateException("Program will not terminate");
            } else {
                rulesToInject = nextRulesToInject;
            }
        }

        // Get entry 0 and make it a pattern, including anchors
        Pattern pattern = Pattern.compile("^" + input.ruleMap.get(0) + "$");

        // Count all messages matching this pattern
        return input.messageList.stream()
                .filter(msg -> pattern.matcher(msg).matches())
                .count();
    }

    // Transforms the rule so that every reference number is surrounded by <> brackets,
    // If the rule contains a |, the rule is surrounded by () brackets.
    // Example: "1 3 | 3 1" becomes "(<1><3>|<3><1>)"
    // For ground rules, the "'s are replaced
    public static String prepareIncompleteRule(String rule) {
        if (rule.indexOf('"') >= 0) {
            return rule.substring(1, 2);
        } else if (rule.indexOf('|') >= 0) {
            return "(<" +
                    rule.replace(" ", "><").replace("<|>", "|") +
                    ">)";
        } else {
            return "<" + rule.replace(" ", "><") + ">";
        }
    }

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("<[0-9]+>");

    public static boolean isFinished(String rule) {
        return !PLACEHOLDER_PATTERN.matcher(rule).find();
    }

    // === Part 2
    // Copy paste from part 1, no generalizations today.

    public static long part2(String resource) {
        Input input = parseInput(resource);

        // Collect all rules without dependencies
        Set<Integer> finishedRules = new HashSet<>(input.ruleMap.size());
        input.ruleMap.entrySet().stream()
                .filter(entry -> entry.getValue().indexOf('"') >= 0)
                .map(Map.Entry::getKey)
                .forEach(finishedRules::add);

        // Prepare the input rules, see method for details
        input.ruleMap.replaceAll(Day19::prepareIncompleteRule2);

        // First iteration starts with the rules without dependencies
        List<Integer> rulesToInject = new ArrayList<>(finishedRules);
        while (finishedRules.size() < input.ruleMap.size()) {
            List<Integer> nextRulesToInject = new ArrayList<>();
            for (Integer index : rulesToInject) {

                String stringToReplace = "<" + index + ">"; // We search for this string to replace
                for (Map.Entry<Integer, String> rule : input.ruleMap.entrySet()) {
                    if (!finishedRules.contains(rule.getKey())) {
                        // Replace the number with the finished rule.
                        // Ex: for index = 4 with value "aba", "<4><5>|<5><4>" becomes "aba<5>|<5>aba"
                        rule.setValue(rule.getValue().replace(stringToReplace, input.ruleMap.get(index)));

                        if (isFinished(rule.getValue())) {
                            finishedRules.add(rule.getKey());
                            nextRulesToInject.add(rule.getKey());

                            // After care for rule 11:
                            // Rule 11 is now <42>{X}<31>{X}
                            // We replace this by (<42>{1}<31>{1}|<42>{2}<31>{2}|<42>{3}<31>{3}|<42>{4}<31>{4}...)
                            if (rule.getKey() == 11) {
                                String startValue = rule.getValue();
                                StringBuilder rule11Builder = new StringBuilder();
                                rule11Builder.append('(');
                                int maxDepth = 100;
                                for (int i = 1; i < maxDepth; i++) {
                                    rule11Builder.append(startValue.replace("X", Integer.toString(i)));
                                    if (i < maxDepth - 1) { // Last one does not have a pipe at the end
                                        rule11Builder.append('|');
                                    }
                                }
                                rule11Builder.append(')');
                                rule.setValue(rule11Builder.toString());
                            }
                        }
                    }
                }
            }
            if (nextRulesToInject.isEmpty()) {
                throw new IllegalStateException("Program will not terminate");
            } else {
                rulesToInject = nextRulesToInject;
            }
        }

        // Get entry 0 and make it a pattern, including anchors
        Pattern pattern = Pattern.compile(input.ruleMap.get(0) + "$");

        // Count all messages matching this pattern
        return input.messageList.stream()
                .filter(msg -> pattern.matcher(msg).matches())
                .count();
    }

    // Same as its part1 counterpart, but with special care for the extra rules.
    public static String prepareIncompleteRule2(Integer index, String rule) {
        if (index == 8) {
            return "<42>+";
        } else if (index == 11) {
            return "<42>{X}<31>{X}"; // X will be filled with a range from 1 to maxDepth recursively to make a very long regex.
            // Newer version of regex can use "<42>(?R)?<31>", but this is not supported in Java :(
        } else {
            return prepareIncompleteRule(rule);
        }
    }

    // === Parsing

    private static final Splitter RULE_SPLITTER = Splitter.on(": ");

    public static Input parseInput(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator = new JoinIterator<>(getLinesIt(resource).iterator(),
                String::isEmpty, SeparatorPolicy.DROP, Collectors.toList());

        Map<Integer, String> ruleList = joinIterator.next().stream()
                .map(RULE_SPLITTER::splitToList)
                .collect(Collectors.toMap(list -> Integer.parseInt(list.get(0)), list -> list.get(1)));

        return new Input(ruleList, joinIterator.next());
    }

    @Value
    public static class Input {
        Map<Integer, String> ruleMap;
        List<String> messageList;
    }

}
