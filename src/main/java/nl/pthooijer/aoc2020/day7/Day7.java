package nl.pthooijer.aoc2020.day7;

import com.google.common.base.*;
import lombok.*;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day7 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/7/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/7/sample-input2.txt";
        String input = "aoc/2020/day/7/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 4): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 32): " + part2(sampleInput1));
        System.out.println("Sample2 Part 2 answer (must be 126): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 2

    public static long part2(String resource) {
        Map<Bag, Map<Bag, Long>> inputMap = parseInput(resource);
        Bag startBag = new Bag("shiny gold");
        return requiredBagsInside(inputMap, startBag);
    }

    public static long requiredBagsInside(Map<Bag, Map<Bag, Long>> inputMap, Bag bag) {
        long result = 0;
        Map<Bag, Long> valueLine = inputMap.get(bag);
        for (Map.Entry<Bag, Long> entry : valueLine.entrySet()) {
            result += (requiredBagsInside(inputMap, entry.getKey()) + 1L) * entry.getValue(); // + 1L = including itself
        }
        return result;
    }

    // === Part 1

    public static long part1(String resource) {
        Map<Bag, Map<Bag, Long>> inputMap = parseInput(resource);
        Set<Bag> result = new HashSet<>();
        Bag startBag = new Bag("shiny gold");
        fillResultSet(inputMap, result, startBag);
        result.remove(startBag);
        return result.size();
    }

    public static void fillResultSet(Map<Bag, Map<Bag, Long>> inputMap, Set<Bag> resultSet, Bag bag) {
        resultSet.add(bag);
        Set<Bag> canDirectlyContainSet = canDirectlyContain(inputMap, bag);
        for (Bag lowerBag : canDirectlyContainSet) {
            if (!resultSet.contains(lowerBag)) {
                fillResultSet(inputMap, resultSet, lowerBag);
            }
        }
    }

    public static Set<Bag> canDirectlyContain(Map<Bag, Map<Bag, Long>> map, Bag bag) {
        return map.entrySet().stream()
                .filter(e -> e.getValue().containsKey(bag))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    // === Parsing

    private static final Pattern LINE_PATTERN = Pattern.compile("^([^\\s]* [^\\s]*) bags contain (.*)\\.");
    private static final Pattern VALUE_ENTRY_PATTERN = Pattern.compile("^([0-9]+) ([^\\s]* [^\\s]*) bags?");
    private static final Splitter COMMA_SPLITTER = Splitter.on(", ");

    private static Map<Bag, Map<Bag, Long>> parseInput(String resource) {
        return getLinesStream(resource)
                .map(Day7::parseLine)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static Map.Entry<Bag, Map<Bag, Long>> parseLine(String line) {
        Matcher lineMatcher = LINE_PATTERN.matcher(line);
        Preconditions.checkArgument(lineMatcher.matches(), "Corrupt input line");
        Bag key = parseBag(lineMatcher.group(1));
        String value = lineMatcher.group(2);

        if ("no other bags".equals(value)) {
            return Map.entry(key, Collections.emptyMap());
        } else {
            Map<Bag, Long> valueMap = new HashMap<>();
            for (String valueEntry : COMMA_SPLITTER.split(value)) {
                Matcher valueEntryMatcher = VALUE_ENTRY_PATTERN.matcher(valueEntry);
                Preconditions.checkArgument(valueEntryMatcher.matches(), "Corrupt input value line");
                long amount = Long.parseLong(valueEntryMatcher.group(1));
                Bag bag = parseBag(valueEntryMatcher.group(2));
                valueMap.put(bag, amount);
            }
            return Map.entry(key, valueMap);
        }
    }

    public static Bag parseBag(String string) {
        return new Bag(string);
    }

    @Value
    public static class Bag {
        String type;
    }


}
