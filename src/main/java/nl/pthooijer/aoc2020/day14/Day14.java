package nl.pthooijer.aoc2020.day14;

import com.google.common.base.*;
import com.google.common.math.*;
import lombok.*;

import java.util.*;
import java.util.regex.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day14 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/14/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/14/sample-input2.txt";
        String input = "aoc/2020/day/14/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 165): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 208): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Pattern MEM_PATTERN = Pattern.compile("^mem\\[([0-9]+)] = ([0-9]+)$");

    // === Part 1

    public static long part1(String resource) {
        MaskPart1 mask = new MaskPart1();
        Map<Long, Long> arrayMap = new HashMap<>();
        for (String line : getLinesIt(resource)) {
            if (line.startsWith("mask = ")) {
                mask.updateMask(line.substring(7));
            } else {
                Matcher matcher = MEM_PATTERN.matcher(line);
                if (matcher.matches()) {
                    long address = Long.parseLong(matcher.group(1));
                    long value = Long.parseLong(matcher.group(2));
                    arrayMap.put(address, mask.apply(value));
                } else {
                    throw new IllegalArgumentException("Invalid input line: " + line);
                }
            }
        }

        return arrayMap.values().stream()
                .reduce(0L, LongMath::checkedAdd);
    }

    @Data
    public static class MaskPart1 {
        public long mask0 = -1; // = 111..(64 times)
        public long mask1 = 0;

        public long apply(long value) {
            return (value & mask0) | mask1;
        }

        public void updateMask(String maskCode) {
            String mask64bit = Strings.padStart(maskCode, 64, 'X');
            mask0 = Long.parseUnsignedLong(mask64bit.replace('X', '1'), 2);
            mask1 = Long.parseUnsignedLong(mask64bit.replace('X', '0'), 2);
        }
    }

    // === Part 2

    public static long part2(String resource) {
        char[] mask = new char[36];
        List<Integer> xIndices = new ArrayList<>(0);
        Map<Long, Long> arrayMap = new HashMap<>(); // Max number of X's is 9, so max 2^9 = 512 entries per line = acceptable
        for (String line : getLinesIt(resource)) {
            if (line.startsWith("mask = ")) {
                mask = line.substring(7).toCharArray();
                xIndices = getIndicesOfX(mask);
            } else {
                Matcher matcher = MEM_PATTERN.matcher(line);
                if (matcher.matches()) {
                    char[] convertedIndex = applyMask1(mask, Long.parseLong(matcher.group(1)));
                    List<Long> arrayIndices = new ArrayList<>(IntMath.pow(2, xIndices.size()));
                    evaluateAllXFloats(arrayIndices, xIndices, convertedIndex, 0);

                    long value = Long.parseLong(matcher.group(2));
                    for(Long arrayIndex : arrayIndices) {
                        arrayMap.put(arrayIndex, value);
                    }
                } else {
                    throw new IllegalArgumentException("Invalid input line: " + line);
                }
            }
        }

        return arrayMap.values().stream()
                .reduce(0L, LongMath::checkedAdd);
    }

    // Applies the mask by setting 1 (in mask) to 1 (in value) en X (in mask) to 1 (in value)
    // The 1 set by X will be recursively replaced by 0 in evaluateAllXFloats
    public static char[] applyMask1(char[] mask2, long value) {
        char[] valueArr = Strings.padStart(Long.toString(value, 2), 36, '0')
                .toCharArray();
        for (int i = 0; i < mask2.length; i++) {
            if (mask2[i] != '0') {
                valueArr[i] = '1';
            }
        }
        return valueArr;
    }

    public static List<Integer> getIndicesOfX(char[] cArr) {
        List<Integer> list = new ArrayList<>(9);
        for (int i = 0; i < cArr.length; i++) {
            if (cArr[i] == 'X') {
                list.add(i);
            }
        }
        return list;
    }

    public static void evaluateAllXFloats(List<Long> listToUpdate, List<Integer> xIndices, char[] valueArray, int iteration) {
        if (iteration >= xIndices.size()) {
            listToUpdate.add(Long.parseLong(new String(valueArray), 2));
        } else {
            evaluateAllXFloats(listToUpdate, xIndices, valueArray, iteration + 1);
            char[] editedValueArray = Arrays.copyOf(valueArray, valueArray.length);
            editedValueArray[xIndices.get(iteration)] = '0'; // Swap the ith bit that was an X from 1 to 0
            evaluateAllXFloats(listToUpdate, xIndices, editedValueArray, iteration + 1);
        }
    }

}
