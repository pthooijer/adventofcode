package nl.pthooijer.aoc2020.day4;

import com.google.common.base.*;
import com.google.common.collect.Streams;
import nl.pthooijer.util.*;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day4 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/4/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/4/sample-input2.txt";
        String sampleInput3 = "aoc/2020/day/4/sample-input3.txt";
        String input = "aoc/2020/day/4/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 2): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be 0): " + part2(sampleInput2));
        System.out.println("Sample3 Part 2 answer (must be 4): " + part2(sampleInput3));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    private static final List<String> REQUIRED_FIELDS = Arrays.asList("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid");

    public static long part1(String resource) {
        return parseInput(resource)
                .filter(Day4::isValidPassport)
                .count();
    }

    private static boolean isValidPassport(Map<String, String> p) {
        return REQUIRED_FIELDS.stream().allMatch(p::containsKey);
    }

    // === Part 2

    public static long part2(String resource) {
        return parseInput(resource)
                .filter(Day4::isValidPassport2)
                .count();
    }

    private static final Pattern YR_PATTERN = Pattern.compile("^[0-9]{4}$");
    private static final Pattern HGT_PATTERN = Pattern.compile("^([0-9]+)(cm|in)$");
    private static final Pattern HCL_PATTERN = Pattern.compile("^#[0-9a-f]{6}$");
    private static final Pattern ECL_PATTERN = Pattern.compile("^(amb|blu|brn|gry|grn|hzl|oth)");
    private static final Pattern PID_PATTERN = Pattern.compile("^[0-9]{9}$");

    private static boolean isValidPassport2(Map<String, String> p) {
        return validateYr(p, "byr", 1920, 2002) &&
                validateYr(p, "iyr", 2010, 2020) &&
                validateYr(p, "eyr", 2020, 2030) &&
                validateHgt(p, "hgt") &&
                validatePattern(p, "hcl", HCL_PATTERN) &&
                validatePattern(p, "ecl", ECL_PATTERN) &&
                validatePattern(p, "pid", PID_PATTERN);
    }

    private static boolean validateYr(Map<String, String> p, String field, int min, int max) {
        if (!validateNotNull(p, field)) {
            return false;
        }
        String value = p.get(field);
        if (YR_PATTERN.matcher(value).matches()) {
            int year = Integer.parseInt(value);
            return debugInvalid(year >= min && year <= max,
                    String.format("Wrong %s: %s, min:%s, max:%s", field, value, min, max));
        } else {
            return debugInvalid(false, String.format("Wrong %s: %s; Format:%s", field, value, YR_PATTERN));
        }
    }


    private static boolean validateHgt(Map<String, String> p, String field) {
        if (!validateNotNull(p, field)) {
            return false;
        }
        String value = p.get(field);
        Matcher matcher = HGT_PATTERN.matcher(value);
        if (matcher.matches()) {
            int num = Integer.parseInt(matcher.group(1));
            switch (matcher.group(2)) {
                case "cm":
                    return debugInvalid(num >= 150 && num <= 193,
                            String.format("Wrong %s: %s; unit=%s, min=%s, max=%s", field, value, "cm", 150, 193));
                case "in":
                    return debugInvalid(num >= 59 && num <= 76,
                            String.format("Wrong %s: %s; unit=%s, min=%s, max=%s", field, value, "in", 59, 76));
                default:
                    return debugInvalid(false, "Should not happen 1");
            }
        } else {
            return debugInvalid(false, String.format("Wrong %s: %s; Format:%s", field, value, HGT_PATTERN));
        }
    }

    private static boolean validatePattern(Map<String, String> p, String field, Pattern pattern) {
        if (!validateNotNull(p, field)) {
            return false;
        }
        String value = p.get(field);
        return debugInvalid(pattern.matcher(value).matches(),
                String.format("Wrong %s: %s; Format:%s", field, value, pattern));
    }

    private static boolean validateNotNull(Map<String, String> p, String field) {
        return debugInvalid(p.containsKey(field), String.format("Wrong %s: Must be present", field));
    }

    private static boolean debugInvalid(boolean valid, String errorMsg) {
        if (!valid) {
            // System.out.println(errorMsg);
        }
        return valid;
    }

    // === Parsing

    private static final Splitter.MapSplitter MAP_SPLITTER = Splitter.on(' ').omitEmptyStrings().withKeyValueSeparator(':');

    public static Stream<Map<String, String>> parseInput(String resource) {
        JoinIterator<String, ?, String> joinIterator = new JoinIterator<>(
                getLinesIt(resource).iterator(),
                String::isEmpty,
                SeparatorPolicy.DROP,
                Collectors.joining(" "));
        return Streams.stream(joinIterator)
                .map(Day4::parsePassport);
    }

    public static Map<String, String> parsePassport(String line) {
        return MAP_SPLITTER.split(line);
    }

}
