package nl.pthooijer.aoc2020.day16;

import com.google.common.base.*;
import com.google.common.collect.Streams;
import com.google.common.collect.*;
import lombok.*;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day16 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/16/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/16/sample-input2.txt";
        String input = "aoc/2020/day/16/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 71): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 143): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        Input input = parseInput(resource);
        RangeSet<Long> totalRangeSet = totalRangeSet(input.rules.values());

        return input.nearbyTickets.stream()
                .flatMap(Collection::stream)
                .filter(value -> !totalRangeSet.contains(value))
                .mapToLong(l -> l)
                .sum();
    }

    public static <T extends Comparable<T>> RangeSet<T> totalRangeSet(Iterable<RangeSet<T>> rangeSets) {
        return Streams.stream(rangeSets)
                .flatMap(rs -> rs.asRanges().stream())
                .collect(Collectors.collectingAndThen(Collectors.toList(), ImmutableRangeSet::unionOf));
    }

    // === Part 2

    public static long part2(String resource) {
        Input input = parseInput(resource);
        RangeSet<Long> totalRangeSet = totalRangeSet(input.rules.values());

        // Filter out all invalid tickets, add your own ticket as valid ticket
        List<List<Long>> validTickets = Stream.concat(input.nearbyTickets.stream(), Stream.of(input.myTicket))
                .filter(list -> list.stream()
                        .allMatch(totalRangeSet::contains))
                .collect(Collectors.toList());

        // Store which values you see for each index
        Map<Integer, Set<Long>> indexToValuesMap = new HashMap<>();
        int nrOfFields = validTickets.get(0).size();
        if (nrOfFields != input.rules.size()) {
            throw new IllegalArgumentException("Nr of rules not equal to nr of fields in a ticket");
        }
        for (int i = 0; i < nrOfFields; i++) {
            indexToValuesMap.put(i, new HashSet<>(validTickets.size()));
            for (List<Long> ticket : validTickets) {
                indexToValuesMap.get(i).add(ticket.get(i));
            }
        }

        // Collect for each set of seen values the rules they obey.
        // Put them in a map from index to all obeyed rules.
        Map<Integer, Set<String>> indexToObeyedRulesMap = new HashMap<>(nrOfFields);
        for (int i = 0; i < nrOfFields; i++) {
            indexToObeyedRulesMap.put(i, new HashSet<>());
            for (Map.Entry<String, RangeSet<Long>> entry : input.rules.entrySet()) {
                if (rangeSetContainsAll(entry.getValue(), indexToValuesMap.get(i))) {
                    indexToObeyedRulesMap.get(i).add(entry.getKey());
                }
            }
        }

        // Find the indices which only has one rule it obeys to, lock them in and put them in a map.
        // Then remove the found index and the found rule from the obeyed rules map. Repeat the process.
        Map<String, Integer> keyToIndexMap = new HashMap<>(nrOfFields);
        resolveIndicesWithOneValidKey(keyToIndexMap, indexToObeyedRulesMap);

        // Filter only the departure rules, extract those rule-values from your ticket, and multiply.
        return keyToIndexMap.entrySet().stream()
                .filter(e -> e.getKey().startsWith("departure"))
                .mapToInt(Map.Entry::getValue)
                .mapToLong(input.myTicket::get)
                .reduce(1, (n1, n2) -> n1 * n2);
    }

    public static <T extends Comparable<T>> boolean rangeSetContainsAll(RangeSet<T> rangeSet, Iterable<T> values) {
        for (T value : values) {
            if (!rangeSet.contains(value)) {
                return false;
            }
        }
        return true;
    }

    public static void resolveIndicesWithOneValidKey(Map<String, Integer> outputMap,
                                                     Map<Integer, Set<String>> indexToObeyedRulesMap) {
        // Find all indices with only 1 valid rule. Put them in a map.
        int startSize = indexToObeyedRulesMap.size();
        Map<String, Integer> newValues = new HashMap<>();
        for (Map.Entry<Integer, Set<String>> entry : indexToObeyedRulesMap.entrySet()) {
            if (entry.getValue().size() == 1) {
                newValues.put(entry.getValue().iterator().next(), entry.getKey());
            }
        }

        // Remove the found indices from the original map
        newValues.values().forEach(indexToObeyedRulesMap::remove);
        // Remove the found rules from the valid rules sets
        newValues.keySet().forEach(key ->
                indexToObeyedRulesMap.values().forEach(set -> set.remove(key)));

        // Iterate
        outputMap.putAll(newValues);
        if (!indexToObeyedRulesMap.isEmpty()) {
            resolveIndicesWithOneValidKey(outputMap, indexToObeyedRulesMap);
        } else if (indexToObeyedRulesMap.size() == startSize) {
            throw new IllegalStateException("Program will never terminate");
        }
    }

    // === Parsing

    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    public static Input parseInput(String resource) {
        Iterator<String> inputIterator = getLinesIt(resource).iterator();

        Map<String, RangeSet<Long>> rules = new HashMap<>();
        while (inputIterator.hasNext()) {
            String line = inputIterator.next();
            if (line.isEmpty()) {
                break;
            }

            Map.Entry<String, RangeSet<Long>> rule = parseRule(line);
            rules.put(rule.getKey(), rule.getValue());
        }

        inputIterator.next(); // "your ticket:"
        List<Long> myTicket = parseTicket(inputIterator.next());

        inputIterator.next(); // "";
        inputIterator.next(); // "nearby tickets:"
        List<List<Long>> nearbyTickets = new ArrayList<>();
        while (inputIterator.hasNext()) {
            String line = inputIterator.next();
            if (line.isEmpty()) {
                break;
            }

            nearbyTickets.add(parseTicket(line));
        }

        return new Input(rules, myTicket, nearbyTickets);
    }

    private static final Pattern RULE_PATTERN = Pattern.compile("^(.+): (\\d+)-(\\d+) or (\\d+)-(\\d+)");

    public static Map.Entry<String, RangeSet<Long>> parseRule(String line) {
        Matcher matcher = RULE_PATTERN.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid line: " + line);
        }

        String key = matcher.group(1);
        Range<Long> range1 = Range.closed(Long.parseLong(matcher.group(2)), Long.parseLong(matcher.group(3)));
        Range<Long> range2 = Range.closed(Long.parseLong(matcher.group(4)), Long.parseLong(matcher.group(5)));
        RangeSet<Long> rangeSet = ImmutableRangeSet.unionOf(Arrays.asList(range1, range2));

        return Map.entry(key, rangeSet);
    }

    public static List<Long> parseTicket(String line) {
        return COMMA_SPLITTER.splitToList(line).stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    @Value
    public static class Input {
        Map<String, RangeSet<Long>> rules;
        List<Long> myTicket;
        List<List<Long>> nearbyTickets;
    }

}
