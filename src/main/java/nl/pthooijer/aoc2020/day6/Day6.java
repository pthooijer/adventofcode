package nl.pthooijer.aoc2020.day6;

import com.google.common.collect.Streams;
import nl.pthooijer.util.*;

import java.util.*;
import java.util.stream.*;

public class Day6 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/6/input.txt";
        String sampleInput1 = "aoc/2020/day/6/sample-input1.txt";

        System.out.println("Sample1 Part 1 answer (must be 11): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 6): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        return parseInput(resource)
                .mapToLong(list -> list.stream()
                        .flatMapToInt(String::chars)
                        .distinct()
                        .count())
                .sum();
    }

    public static long part2(String resource) {
        return parseInput(resource)
                .mapToLong(list -> list.get(0).chars()
                        .filter(i -> list.stream()
                                .skip(1)
                                .allMatch(s -> s.indexOf(i) != -1))
                        .count())
                .sum();
    }

    public static Stream<List<String>> parseInput(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator = new JoinIterator<>(
                ResourceHelper.getLinesIt(resource).iterator(),
                String::isEmpty,
                SeparatorPolicy.DROP,
                Collectors.toList());
        return Streams.stream(joinIterator);
    }
}
