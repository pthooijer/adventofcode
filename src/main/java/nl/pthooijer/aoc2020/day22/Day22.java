package nl.pthooijer.aoc2020.day22;

import lombok.*;
import nl.pthooijer.util.*;

import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

@SuppressWarnings("ConstantConditions")
public class Day22 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/22/sample-input1.txt";
        String sampleInput2 = "aoc/2020/day/22/sample-input2.txt";
        String input = "aoc/2020/day/22/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 306): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 291): " + part2(sampleInput1));
        System.out.println("Sample2 Part 2 answer (must be 105): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        State state = parseInput(resource);
        Deque<Long> deck1 = state.deck1;
        Deque<Long> deck2 = state.deck2;
        while (!deck1.isEmpty() && !deck2.isEmpty()) {
            Long card1 = deck1.pollFirst();
            Long card2 = deck2.pollFirst();
            if (card1 > card2) {
                deck1.addLast(card1);
                deck1.addLast(card2);
            } else {
                deck2.addLast(card2);
                deck2.addLast(card1);
            }
        }

        Deque<Long> winningDeck = deck1.isEmpty() ? deck2 : deck1;
        return calculateScore(winningDeck);
    }

    public static long calculateScore(Deque<Long> deck) {
        long nrOfCards = deck.size();
        long score = 0L;
        for (long i = 1; i <= nrOfCards; i++) {
            score += deck.pollLast() * i;
        }
        return score;
    }

    public static long part2(String resource) {
        State state = parseInput(resource);
        GameResult endResult = playRecursiveCombat(state);
        return calculateScore(endResult.winningDeck);
    }

    public static GameResult playRecursiveCombat(State state) {
        Set<State> prevStates = new HashSet<>();
        State currentState = state.copy();
        while (true) {
            if (currentState.deck1.isEmpty()) {
                return new GameResult(false, currentState.deck2);
            } else if (currentState.deck2.isEmpty() || !prevStates.add(currentState)) { // Add returns false if the set already contains the element
                return new GameResult(true, currentState.deck1);
            }

            Long card1 = currentState.deck1.pollFirst();
            Long card2 = currentState.deck2.pollFirst();
            boolean winner; // true = P1, false = P2
            if (currentState.deck1.size() >= card1 && currentState.deck2.size() >= card2) {
                // Make a new State with deck sizes equal to the drawn value
                State subState = new State(subDeque(currentState.deck1, card1),
                        subDeque(currentState.deck2, card2));
                winner = playRecursiveCombat(subState).winner;
            } else {
                winner = card1 > card2;
            }

            if (winner) { // If P1 wins
                currentState.deck1.addLast(card1);
                currentState.deck1.addLast(card2);
            } else { // If P2 wins
                currentState.deck2.addLast(card2);
                currentState.deck2.addLast(card1);
            }
        }
    }

    public static <T> Deque<T> subDeque(Deque<T> deque, long amount) {
        Deque<T> dequeCopy = new LinkedList<>(deque);
        Deque<T> result = new LinkedList<>();
        for(long i=0; i<amount; i++) {
            result.addLast(dequeCopy.pollFirst());
        }
        return result;
    }

    // === Parsing

    public static State parseInput(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator = new JoinIterator<>(getLinesIt(resource).iterator(),
                String::isEmpty,
                SeparatorPolicy.DROP,
                Collectors.toList());

        Deque<Long> deck1 = joinIterator.next().stream()
                .skip(1) // header
                .map(Long::parseLong)
                .collect(Collectors.toCollection(LinkedList::new));
        Deque<Long> deck2 = joinIterator.next().stream()
                .skip(1) // header
                .map(Long::parseLong)
                .collect(Collectors.toCollection(LinkedList::new));
        return new State(deck1, deck2);
    }

    @Value
    public static class GameResult {
        boolean winner;
        Deque<Long> winningDeck;
    }

    @Data
    @AllArgsConstructor
    public static class State {
        Deque<Long> deck1;
        Deque<Long> deck2;

        public State copy() {
            return new State(new LinkedList<>(deck1), new LinkedList<>(deck2));
        }
    }

}
