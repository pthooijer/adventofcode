package nl.pthooijer.aoc2020.day11;

import nl.pthooijer.util.*;

import static nl.pthooijer.util.ResourceHelper.*;


public class Day11 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/11/sample-input1.txt";
        String input = "aoc/2020/day/11/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 37): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 26): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Parts

    public static int part1(String resource) {
        Rule rule = new Rule() {
            @Override
            public int toEmptyThreshold() {
                return 4;
            }

            @Override
            public int countOccupiedNeighbours(char[][] input, int h, int v) {
                int count = 0;
                for (int i = h - 1; i <= h + 1; i++) {
                    for (int j = v - 1; j <= v + 1; j++) {
                        if ((i != h || j != v) && ArrayUtils.isInBounds(input, i, j) && input[i][j] == '#') {
                            count++;
                        }
                    }
                }
                return count;
            }
        };
        return run(resource, rule);
    }

    public static long part2(String resource) {
        Rule rule = new Rule() {
            @Override
            public int toEmptyThreshold() {
                return 5;
            }

            @Override
            public int countOccupiedNeighbours(char[][] input, int h, int v) {
                int count = 0;
                for (int stepI = -1; stepI <= 1; stepI++) {
                    for (int stepJ = -1; stepJ <= 1; stepJ++) {
                        if (stepI == 0 && stepJ == 0) {
                            continue;
                        }
                        int i = h;
                        int j = v;
                        while (true) {
                            i += stepI;
                            j += stepJ;
                            if (!ArrayUtils.isInBounds(input, i, j)) {
                                break;
                            }
                            char c = input[i][j];
                            if (c == '#') {
                                count++;
                                break;
                            } else if (c == 'L') {
                                break;
                            }
                        }
                    }
                }
                return count;
            }
        };
        return run(resource, rule);
    }

    // === Main code

    public static int run(String resource, Rule rule) {
        char[][] state = getChars2dArray(resource);
        int occupiedSeats = ArrayUtils.countChars(state, '#');
        while (true) {
            int prevOccupiedSeats = occupiedSeats;
            state = advanceOneStep(state, rule);
            occupiedSeats = ArrayUtils.countChars(state, '#');
            if (prevOccupiedSeats == occupiedSeats) {
                return occupiedSeats;
            }
        }
    }

    public static char[][] advanceOneStep(char[][] input, Rule rule) {
        char[][] output = new char[input.length][input[0].length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                switch (input[i][j]) {
                    case 'L':
                        output[i][j] = rule.countOccupiedNeighbours(input, i, j) == 0 ? '#' : 'L';
                        break;
                    case '#':
                        output[i][j] = rule.countOccupiedNeighbours(input, i, j) >= rule.toEmptyThreshold() ? 'L' : '#';
                        break;
                    default:
                        output[i][j] = input[i][j];
                }
            }
        }
        return output;
    }

    public interface Rule {
        int toEmptyThreshold();

        int countOccupiedNeighbours(char[][] input, int h, int v);
    }
}
