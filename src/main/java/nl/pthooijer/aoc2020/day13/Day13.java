package nl.pthooijer.aoc2020.day13;

import com.google.common.base.*;
import com.google.common.collect.Streams;
import com.google.common.math.*;
import lombok.*;

import java.util.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day13 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/13/sample-input1.txt";
        String input = "aoc/2020/day/13/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 295): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample0 Part 2 answer (must be 39): " + crtSieving(Arrays.asList(
                new CRTEntry(3, 0), new CRTEntry(4, 3), new CRTEntry(5, 4))));
        System.out.println("Sample1 Part 2 answer (must be 1068781): " + part2(sampleInput1));
        System.out.println("Sample2 Part 2 answer (must be 3417): " + part2Line("17,x,13,19"));
        System.out.println("Sample3 Part 2 answer (must be 754018): " + part2Line("67,7,59,61"));
        System.out.println("Sample4 Part 2 answer (must be 779210): " + part2Line("67,x,7,59,61"));
        System.out.println("Sample5 Part 2 answer (must be 1261476): " + part2Line("67,7,x,59,61"));
        System.out.println("Sample6 Part 2 answer (must be 1202161486): " + part2Line("1789,37,47,1889"));

        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static Splitter COMMA_SPLITTER = Splitter.on(',');

    public static long part1(String resource) {
        List<String> input = getLinesList(resource);
        long startTime = Long.parseLong(input.get(0));
        return Streams.stream(COMMA_SPLITTER.split(input.get(1)).iterator())
                .filter(s -> !"x".equals(s))
                .mapToLong(Long::parseLong)
                .mapToObj(l -> new Answer1(l, l - startTime % l))
                .min(Comparator.comparingLong(answer -> answer.waitTime))
                .map(answer -> answer.busId * answer.waitTime)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Input"));
    }

    @Value
    public static class Answer1 {
        long busId;
        long waitTime;
    }

    // === Part 2

    public static long part2(String resource) {
        return part2Line(getLinesList(resource).get(1));
    }

    public static long part2Line(String line) {
        List<CRTEntry> input = parseInput2(line);
        return crtSieving(input);
    }

    public static long crtSieving(Collection<CRTEntry> input) {
        if (!areCoprime(input.stream().mapToLong(CRTEntry::getDivisor).toArray())) {
            throw new IllegalStateException("Solution only works for pairwise coprime entries");
        }
        List<CRTEntry> largestDivFirst = input.stream()
                .sorted(Comparator.comparingLong(CRTEntry::getDivisor).reversed())
                .collect(Collectors.toList());
        long result = largestDivFirst.get(0).remainder;
        long productStep = largestDivFirst.get(0).divisor;
        for (int i = 1; i < largestDivFirst.size(); i++) {
            CRTEntry entry = largestDivFirst.get(i);
            result = findNext(result, productStep, entry);
            productStep *= entry.divisor;
        }
        return result;
    }

    public static long findNext(long start, long stepSize, CRTEntry crtEntry) {
        long n = start;
        final long div = crtEntry.divisor;
        final long rem = crtEntry.remainder;
        while (true) {
            if (n % div == rem) {
                return n;
            }
            n += stepSize;
        }
    }

    public static List<CRTEntry> parseInput2(String line) {
        List<CRTEntry> result = new ArrayList<>();
        List<String> splitLine = COMMA_SPLITTER.splitToList(line);
        for (int i = 0; i < splitLine.size(); i++) {
            String word = splitLine.get(i);
            if (!"x".equals(word)) {
                long busId = Long.parseLong(word);
                long remainder = Math.floorMod(busId - i, busId);
                result.add(new CRTEntry(busId, remainder));
            }
        }
        return result;
    }

    public static boolean areCoprime(long[] input) {
        for (long n1 : input) {
            for (long n2 : input) {
                if (n1 != n2 && LongMath.gcd(n1, n2) != 1) {
                    return false;
                }
            }
        }
        return true;
    }

    // CRT = Chinese Remainder Theorem
    @Value
    public static class CRTEntry {
        long divisor;
        long remainder;
    }

}
