package nl.pthooijer.aoc2020.day23;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.LinkedList;
import java.util.stream.Collectors;

public class Day23 {

    public static void main(String[] args) {

        System.out.println("Sample1 Part 1 answer (must be 92658374): " + part1("389125467", 10));
        System.out.println("Sample2 Part 1 answer (must be 67384529): " + part1("389125467", 100));
        System.out.println("Part 1 answer: " + part1("589174263", 100));

        System.out.println("Sample1 Part 1 as 2 answer (must be 92658374): " + part1UsingPart2("389125467", 10));
        System.out.println("Sample2 Part 1 as 2 answer (must be 67384529): " + part1UsingPart2("389125467", 100));
        System.out.println("Sample3 Part 2 answer (must be 149245887792): " + part2("389125467", 10_000_000));
        System.out.println("Part 2 answer: " + part2("589174263", 10_000_000));
    }

    public static String part1(String line, int iterations) {
        State state = parseInput(line);
        for (int i = 0; i < iterations; i++) {
            crabCupsRound(state);
        }

        // Set cup 1 in the first position
        while (state.cups.peekFirst() != 1L) {
            state.cups.addLast(state.cups.pollFirst());
        }

        // Generate outputString
        return state.cups.stream()
                .skip(1) // Skip first element, which is Cup 1
                .map(l -> Long.toString(l))
                .collect(Collectors.joining());
    }

    public static void crabCupsRound(State state) {
        // Remove next three cups
        LinkedList<Long> removed = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            removed.add(state.cups.remove(1));
        }

        // Find index of destination cup
        int destinationCupIndex = findDestinationCup(state);

        // Place removed cup after destination cup
        for (int i = 0; i < 3; i++) {
            state.cups.add(destinationCupIndex + 1, removed.pollLast());
        }

        // Current cup = next cup, so move first element to the end.
        state.cups.addLast(state.cups.pollFirst());
    }

    public static int findDestinationCup(State state) {
        long valueToSearch = state.cups.get(0);
        int foundIndex = -1;
        while (foundIndex == -1) {
            valueToSearch = Math.floorMod(valueToSearch - 2, state.nrOfCups) + 1;
            foundIndex = state.cups.indexOf(valueToSearch);
        }
        return foundIndex;
    }

    public static State parseInput(String line) {
        LinkedList<Long> cups = line.chars()
                .mapToObj(i -> Long.parseLong(Character.toString(i)))
                .collect(Collectors.toCollection(LinkedList::new));
        return new State(cups);
    }

    @Value
    public static class State {
        LinkedList<Long> cups;
        int nrOfCups = 9;
    }

    // === Part 2

    public static String part1UsingPart2(String line, int iterations) {
        State2 state = parseInput2(line, false);

        for (int i = 0; i < iterations; i++) {
            crabCupsRound2(state);
        }

        // Generate outputString
        int cup = 1;
        StringBuilder outputString = new StringBuilder();
        for (int i = 0; i < state.cupToNextCupMap.length - 1; i++) {
            cup = state.cupToNextCupMap[cup];
            outputString.append(cup);
        }
        return outputString.toString();
    }

    public static long part2(String line, int iterations) {
        State2 state = parseInput2(line, true);

        for (int i = 0; i < iterations; i++) {
            crabCupsRound2(state);
        }

        long cupAfter1 = state.cupToNextCupMap[1];
        long cupAfter2 = state.cupToNextCupMap[(int) cupAfter1];
        return cupAfter1 * cupAfter2;
    }

    private static void crabCupsRound2(State2 state) {
        int[] cupToNextCupMap = state.cupToNextCupMap;

        // Remove the next three cups
        int nextCup1 = cupToNextCupMap[state.currentCup];
        int nextCup2 = cupToNextCupMap[nextCup1];
        int nextCup3 = cupToNextCupMap[nextCup2];
        cupToNextCupMap[state.currentCup] = cupToNextCupMap[nextCup3]; // Link current to the 4th
        int[] removedCups = new int[]{nextCup1, nextCup2, nextCup3};

        // Select the destination cup, the one with one label lower
        int destinationCup = state.currentCup;
        do {
            destinationCup = destinationCup - 1;
            if (destinationCup <= 0) { // wrap around
                destinationCup = state.highestCup;
            }
        } while (destinationCup == nextCup1 || destinationCup == nextCup2 || destinationCup == nextCup3);

        // Place the three cups directly after the destination cup
        int oldCupAfterDestination = cupToNextCupMap[destinationCup];
        cupToNextCupMap[destinationCup] = nextCup1;
        cupToNextCupMap[nextCup1] = nextCup2;
        cupToNextCupMap[nextCup2] = nextCup3;
        cupToNextCupMap[nextCup3] = oldCupAfterDestination;

        // Determine the new current cup, it's the one after the current cup
        state.currentCup = cupToNextCupMap[state.currentCup];
    }

    public static State2 parseInput2(String line, boolean appendToOneMillion) {
        int length = appendToOneMillion ? 1_000_001 : line.length() + 1;
        // Index 0 is unused. Is less space efficient but makes the code much more readable

        int[] cupToNextCupMap = new int[length];
        int firstCup = -1;
        int prevCup = -1;
        for (char c : line.toCharArray()) {
            int cup = Integer.parseInt(Character.toString(c));
            if (firstCup == -1) {
                firstCup = cup;
            } else {
                cupToNextCupMap[prevCup] = cup;
            }
            prevCup = cup;
        }

        if (appendToOneMillion) {
            for (int i = 10; i <= 1_000_000; i++) {
                cupToNextCupMap[prevCup] = i;
                prevCup = i;
            }
        }

        // Link the last cup to the first cup
        cupToNextCupMap[prevCup] = firstCup;

        return new State2(cupToNextCupMap, length - 1, firstCup);
    }

    @AllArgsConstructor
    public static class State2 {
        int[] cupToNextCupMap; // Index is target cup, value is next cup. It's like a map from index to value.
        int highestCup;
        int currentCup;
    }

}
