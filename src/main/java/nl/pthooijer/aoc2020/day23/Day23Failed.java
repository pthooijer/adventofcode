package nl.pthooijer.aoc2020.day23;

import lombok.*;

import java.util.*;

// My first attempt for part 2 caused an Out of Memory error because the key Strings became too long
// Earlier attempts used a Double instead of a String, but the Double precision was not great enough for this.
// After that this approach was abandoned and replaced with the solution in the main class.
public class Day23Failed {

    public static void main(String[] args) {

        System.out.println("Sample1 Part 1 as 2 answer (must be 92658374): " + part1UsingPart2("389125467", 10));
        System.out.println("Sample2 Part 1 as 2 answer (must be 67384529): " + part1UsingPart2("389125467", 100));
        System.out.println("Sample3 Part 2 answer (must be 149245887792): " + part2("389125467", 10_000_000));
        System.out.println("Part 2 answer: " + part2("589174263", 10_000_000));
    }

    // === Part 2 Failed

    public static String part1UsingPart2(String line, int iterations) {
        State2 state = parseInput2(line, false);

        for (int i = 0; i < iterations; i++) {
            crabCupsRound2(state);
        }

        // Generate outputString
        Long cup = 1L;
        StringBuilder outputString = new StringBuilder();
        for (int i = 0; i < state.indexToCupMap.size() - 1; i++) {
            cup = state.getNextCupByIndex(cup);
            outputString.append(cup);
        }
        return outputString.toString();
    }

    public static long part2(String line, int iterations) {
        State2 state = parseInput2(line, true);
        for (int i = 0; i < iterations; i++) {
            crabCupsRound2(state);
        }

        // Return the two cups after the current cup
        Long nextCup = state.getNextCupByIndex(state.currentCup);
        Long next2Cup = state.getNextCupByIndex(nextCup);

        NavigableMap<String, Integer> test = new TreeMap<>();

        LinkedList<String> test2 = new LinkedList<>();
        return nextCup * next2Cup;
    }

    private static final String[] NEW_INDEX_POSTFIX = new String[]{"a", "b", "c"};

    private static void crabCupsRound2(State2 state) {
//        System.out.println(state.currentCup + " " + state.indexToCupMap);
//        System.out.println(state.currentCup + " " + state.indexToCupMap.values());
        // Save the three cups after the current cup
        LinkedList<Long> removedEntries = new LinkedList<>();
        String pointerKey = state.cupToIndexMap.get(state.currentCup);
        for (int i = 0; i < 3; i++) {
            pointerKey = state.getNextIndex(pointerKey);
            removedEntries.add(state.indexToCupMap.get(pointerKey));
        }

        // Find the destination cup, which is the cup with the value below the current cup
        Long destinationCup;
        Long cupAbove = state.currentCup;
        while (true) {
            Long candidateCup = state.getPreviousCupByLabel(cupAbove);
            if (!removedEntries.contains(candidateCup)) { // If not in removed list
                destinationCup = candidateCup; // destination cup found
                break;
            } else {
                cupAbove = candidateCup; // Move 1 cup down
            }
        }

        // Add the removed entries after the destination cup
        // Make three indices between the destination cup and the next one (this is why we use Strings)
        String destinationCupIndex = state.cupToIndexMap.get(destinationCup);
        for (int i = 0; i < 3; i++) {
            String newIndex = destinationCupIndex + NEW_INDEX_POSTFIX[i];
            Long removedCup = removedEntries.pollFirst();
            String oldIndex = state.cupToIndexMap.put(removedCup, newIndex);
            state.indexToCupMap.remove(oldIndex);
            state.indexToCupMap.put(newIndex, removedCup);
        }

        // Get the next current cup, it is at the next index;
        state.currentCup = state.getNextCupByIndex(state.currentCup);
    }

    public static State2 parseInput2(String line, boolean appendToOneMillion) {
        NavigableMap<Long, String> cupToIndexMap = new TreeMap<>();
        NavigableMap<String, Long> indexToCupMap = new TreeMap<>();

        long i = 1;
        for (char c : line.toCharArray()) {
            long cup = Long.parseLong(Character.toString(c));
            String index = String.format("%010d", i);
            cupToIndexMap.put(cup, index);
            indexToCupMap.put(index, cup);
            i++;
        }

        if (appendToOneMillion) {
            while (i <= 1_000_000) {
                String index = String.format("%010d", i);
                cupToIndexMap.put(i, index);
                indexToCupMap.put(index, i);
                i++;
            }
        }

        return new State2(cupToIndexMap, indexToCupMap, indexToCupMap.firstEntry().getValue());
    }

    @Data
    @AllArgsConstructor
    public static class State2 {
        NavigableMap<Long, String> cupToIndexMap;
        NavigableMap<String, Long> indexToCupMap;
        Long currentCup;

        public String getNextIndex(String index) {
            String foundIndex = indexToCupMap.higherKey(index);
            // If index is already the highest key, return the lowest key (wrap around)
            return foundIndex == null ? indexToCupMap.firstKey() : foundIndex;
        }

        public Long getPreviousCupByLabel(Long cup) {
            Long foundCup = cupToIndexMap.lowerKey(cup);
            // If cup is already the lowest key, return the highest key (wrap around)
            return foundCup == null ? cupToIndexMap.lastKey() : foundCup;
        }

        public Long getNextCupByIndex(Long cup) {
            String currentIndex = cupToIndexMap.get(cup);
            String nextIndex = getNextIndex(currentIndex);
            return indexToCupMap.get(nextIndex);
        }
    }

}
