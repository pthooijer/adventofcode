package nl.pthooijer.aoc2020.day3;

import lombok.*;

import java.util.*;

import static nl.pthooijer.util.ResourceHelper.*;


public class Day3 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/3/input.txt";
        System.out.println("Part 1 answer: " + part1(input));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static long part1(String resource) {
        char[][] input = parseInput(resource);
        Slope slope = new Slope(3, 1);
        return countTrees(input, slope);
    }


    public static long part2(String resource) {
        char[][] input = parseInput(resource);
        Slope[] slopes = new Slope[]{
                new Slope(1, 1),
                new Slope(3, 1),
                new Slope(5, 1),
                new Slope(7, 1),
                new Slope(1, 2)};

        return Arrays.stream(slopes)
                .mapToLong(s -> countTrees(input, s))
                .reduce(1L, (l1, l2) -> l1 * l2);
    }

    public static long countTrees(char[][] input, Slope slope) {
        int v = 0;
        int h = 0;
        long count = 0L;
        while (v < input.length) {
            if (get(input, v, h) == '#') {
                count++;
            }
            v = v + slope.down;
            h = h + slope.right;
        }
        return count;
    }

    private static char get(char[][] arr, int v, int h) {
        return arr[v % arr.length][h % arr[0].length];
    }

    // input.length = 323
    // input[0].length = 31
    // input[0-322][0-30]
    private static char[][] parseInput(String resource) {
        return getChars2dArray(resource);
    }

    @Value
    public static class Slope {
        int right;
        int down;
    }

}
