package nl.pthooijer.aoc2020.day21;

import com.google.common.base.*;
import lombok.*;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day21 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/21/sample-input1.txt";
        String input = "aoc/2020/day/21/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 5): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be mxmxvkd,sqjhc,fvjkl): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Part 1

    public static long part1(String resource) {
        List<Food> foodList = parseInput(resource);

        // Returns for key Allergen each Ingredient that appears in every food we know contains the Allergen
        // Aka, the intersection of all Food.ingredients that has Food.allergen containing the key Allergen.
        Map<String, Set<String>> aToIIntersections = getAllergenToIngredientIntersections(foodList);

        // For each ingredient (duplicates allowed), count the ingredients that do not appear in the intersections.
        return foodList.stream()
                .flatMap(food -> food.ingredients.stream())
                .filter(i -> aToIIntersections.values().stream()
                        .noneMatch(set -> set.contains(i)))
                .count();
    }

    public static Map<String, Set<String>> getAllergenToIngredientIntersections(List<Food> foodList) {
        // Map contains all value Ingredients that appear in each seen key Allergen.
        Map<String, Set<String>> aToIIntersections = new HashMap<>();
        for (Food food : foodList) {
            for (String allergen : food.allergens) {
                // Only retain the ingredients already in the map and in this Food's ingredient set.
                // If this is the first time seeing the allergen, make a new entry with this Food's ingredient set.
                aToIIntersections.merge(allergen, new HashSet<>(food.ingredients),
                        (i1, i2) -> {
                            i1.retainAll(i2);
                            return i1;
                        });
            }
        }
        return aToIIntersections;
    }

    // === Part 2

    public static String part2(String resource) {
        List<Food> foodList = parseInput(resource);
        Map<String, Set<String>> aToIMap = getAllergenToIngredientIntersections(foodList);
        Map<String, String> aToIResults = new HashMap<>();
        // Put all Allergens with only one available Ingredient in the result map.
        // Remove the found Ingredient from all Ingredient sets in the aToIMap.
        // Iterate.
        reduce(aToIMap, aToIResults);

        return aToIResults.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.joining(","));
    }

    public static void reduce(Map<String, Set<String>> sourceMap, Map<String, String> aToIResults) {
        Map<String, String> newFound = new HashMap<>();
        // Put all Allergens with only one available Ingredient in the result map.
        sourceMap.entrySet().stream()
                .filter(entry -> entry.getValue().size() == 1)
                .forEach(entry -> newFound.put(entry.getKey(), entry.getValue().iterator().next()));
        aToIResults.putAll(newFound);

        // Remove the found Ingredient from all Ingredient sets in the aToIMap.
        sourceMap.replaceAll((a, iSet) -> {
            iSet.removeAll(newFound.values());
            return iSet;
        });

        // Iterate if the result map is not yet completely filled
        if (sourceMap.size() != aToIResults.size()) {
            reduce(sourceMap, aToIResults);
        }
    }

    // === Parsing

    public static final Splitter SPACE_SPLITTER = Splitter.on(' ');
    public static final Splitter COMMA_SPLITTER = Splitter.on(", ");
    public static final Splitter BRACKET_SPLITTER = Splitter.on(Pattern.compile("( \\(contains |\\))"));

    public static List<Food> parseInput(String resource) {
        List<Food> foodList = new ArrayList<>();
        for (String line : getLinesIt(resource)) {
            List<String> fields = BRACKET_SPLITTER.splitToList(line);
            Set<String> ingredients = new HashSet<>(SPACE_SPLITTER.splitToList(fields.get(0)));
            Set<String> allergens = new HashSet<>(COMMA_SPLITTER.splitToList(fields.get(1)));
            foodList.add(new Food(ingredients, allergens));
        }
        return foodList;
    }

    @Value
    public static class Food {
        Set<String> ingredients;
        Set<String> allergens;
    }

}
