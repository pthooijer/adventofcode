package nl.pthooijer.aoc2020.day25;

public class Day25 {

    public static void main(String[] args) {
        System.out.println("Sample1 Part 1 answer (must be 14897079): " + part1(5764801, 17807724));
        System.out.println("Part 1 answer: " + part1(2084668, 3704642));

        System.out.println("Part 2 answer: " + part2());
    }

    public static long part1(long key1, long key2) {
        long subjectNr = 7;
        long mod = 20201227;

        long publicKey1 = 1;
        while (publicKey1 != key1) {
            publicKey1 = (publicKey1 * subjectNr) % mod;
        }

        long publicKey2 = 1;
        long answer = 1;
        long subjectNrTot = publicKey1;
        while (publicKey2 != key2) {
            publicKey2 = (publicKey2 * subjectNr) % mod;
            answer = (answer * subjectNrTot) % mod;
        }

        return answer;
    }

    public static String part2() {
        return "Happy holidays!";
    }

}
