package nl.pthooijer.aoc2020.day18;

import java.util.function.*;

import static nl.pthooijer.util.ResourceHelper.*;

public class Day18 {

    public static void main(String[] args) {
        String input = "aoc/2020/day/18/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 26): " + part1Line("2 * 3 + (4 * 5)"));
        System.out.println("Sample2 Part 1 answer (must be 437): " + part1Line("5 + (8 * 3 + 9 + 3 * 4 * 3)"));
        System.out.println("Sample3 Part 1 answer (must be 12240): " + part1Line("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"));
        System.out.println("Sample4 Part 1 answer (must be 13632): " + part1Line("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample5 Part 2 answer (must be 51): " + part2Line("1 + (2 * 3) + (4 * (5 + 6))"));
        System.out.println("Sample6 Part 2 answer (must be 46): " + part2Line("2 * 3 + (4 * 5)"));
        System.out.println("Sample7 Part 2 answer (must be 1445): " + part2Line("5 + (8 * 3 + 9 + 3 * 4 * 3)"));
        System.out.println("Sample8 Part 2 answer (must be 669060): " + part2Line("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"));
        System.out.println("Sample9 Part 2 answer (must be 23340): " + part2Line("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"));
        System.out.println("Part 2 answer: " + part2(input));
    }

    // === Parts

    public static long part1(String resource) {
        return getLinesStream(resource)
                .mapToLong(Day18::part1Line)
                .sum();
    }

    public static long part1Line(String line) {
        line = line.replace(" ", "");
        return flattenAndEvaluate(line, Day18::evaluatedFlattened1);
    }

    public static long part2(String resource) {
        return getLinesStream(resource)
                .mapToLong(Day18::part2Line)
                .sum();
    }

    public static long part2Line(String line) {
        line = line.replace(" ", "");
        return flattenAndEvaluate(line, Day18::evaluatedFlattened2);
    }

    // === Main Code

    public static long flattenAndEvaluate(String equation, ToLongFunction<String> evaluator) {
        // First flatten the equation
        StringBuilder flattenedBuilder = new StringBuilder();
        if (equation.indexOf('(') == -1) {
            flattenedBuilder.append(equation);
        } else {
            StringBuilder parenthesisBuilder = new StringBuilder();
            int parenthesisDepth = 0;
            for (char c : equation.toCharArray()) {
                if (parenthesisDepth == 1 && c == ')') {
                    long parenthesisValue = flattenAndEvaluate(parenthesisBuilder.toString(), evaluator);
                    flattenedBuilder.append(parenthesisValue);
                    parenthesisBuilder = new StringBuilder();
                    parenthesisDepth--;
                } else if (parenthesisDepth >= 1) {
                    parenthesisBuilder.append(c);
                    if (c == '(') {
                        parenthesisDepth++;
                    } else if (c == ')') {
                        parenthesisDepth--;
                    }
                } else if (c == ')') {
                    parenthesisDepth--;
                } else if (c == '(') {
                    parenthesisDepth++;
                } else {
                    flattenedBuilder.append(c);
                }
            }
        }

        // Then evaluate the flattened equation
        return evaluator.applyAsLong(flattenedBuilder.toString());
    }

    private static long evaluatedFlattened1(String flattenedEquation) {
        flattenedEquation += "+"; // Append a dummy operation to get the last number evaluated
        StringBuilder numberBuilder = new StringBuilder();
        char prevOperation = '+';
        long value = 0L;
        for (char c : flattenedEquation.toCharArray()) {
            if (c == '+' || c == '*') {
                long prevNumber = Long.parseLong(numberBuilder.toString());
                value = applyOperator(value, prevNumber, prevOperation);
                prevOperation = c;
                numberBuilder = new StringBuilder();
            } else {
                numberBuilder.append(c);
            }
        }
        return value;
    }

    private static long evaluatedFlattened2(String flattenedEquation) {
        // First evaluate the additions
        flattenedEquation += "+"; // Append a last dummy operation to get the last number evaluated
        StringBuilder numberBuilder = new StringBuilder();
        long prev2Number = 0L;
        char prevOperation = '+';
        StringBuilder intermediateEquationBuilder = new StringBuilder(); // Store everything in another String
        for (char c : flattenedEquation.toCharArray()) {
            if (c == '+' || c == '*') {
                long prevNumber = Long.parseLong(numberBuilder.toString());
                numberBuilder = new StringBuilder();
                // Evaluate addition, don't evaluate multiplication
                if (prevOperation == '+') {
                    // Merge prev into prev2. Prev2 does not shift
                    prev2Number = applyOperator(prev2Number, prevNumber, prevOperation);
                } else {
                    // Prev shifts to prev2. Prev2 shifts to the StringBuilder
                    intermediateEquationBuilder.append(prev2Number)
                            .append(prevOperation);
                    prev2Number = prevNumber;
                }
                prevOperation = c;
            } else {
                numberBuilder.append(c);
            }
        }
        intermediateEquationBuilder.append(prev2Number); // Shift the last prev2 into the StringBuilder

        // Then evaluate the multiplications. Use part 1 code
        return evaluatedFlattened1(intermediateEquationBuilder.toString());
    }

    private static long applyOperator(long v1, long v2, char operation) {
        switch (operation) {
            case '+':
                return v1 + v2;
            case '*':
                return v1 * v2;
            default:
                throw new UnsupportedOperationException("Unsupported operation: " + operation);
        }
    }

}
