package nl.pthooijer.aoc2020.day8;

import com.google.common.base.*;
import lombok.*;
import nl.pthooijer.util.*;

import java.util.*;
import java.util.stream.*;

public class Day8 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2020/day/8/sample-input1.txt";
        String input = "aoc/2020/day/8/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 5): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 8): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Return part1(String resource) {
        return evaluate(parseInput(resource));
    }

    public static Return evaluate(List<Instruction> input) {
        int i = 0;
        int acc = 0;
        Set<Integer> seenIndices = new HashSet<>();
        while (true) {
            if (i == input.size()) {
                return new Return(ReturnCode.SUCCESS, acc);
            } else if (i < 0 || i > input.size()) {
                return new Return(ReturnCode.OUT_OF_BOUNDS, acc);
            } else if (!seenIndices.add(i)) {
                return new Return(ReturnCode.LOOP, acc);
            }

            Instruction instruction = input.get(i);
            i++;
            switch (instruction.op) {
                case ACC:
                    acc += instruction.arg;
                    break;
                case JMP:
                    i += instruction.arg - 1; // -1 because you did i++ earlier
                    break;
                case NOP:
                default:
                    break;
            }
        }
    }

    public static long part2(String resource) {
        List<Instruction> list = new ArrayList<>(parseInput(resource));
        for(int i=0; i < list.size(); i++) {
            Instruction instruction = list.get(i);
            if(instruction.op == Operation.NOP) {
                instruction.setOp(Operation.JMP);
            } else if (instruction.op == Operation.JMP) {
                instruction.setOp(Operation.NOP);
            } else {
                continue;
            }

            Return result = evaluate(list);
            if(result.returnCode == ReturnCode.SUCCESS) {
                return result.acc;
            }

            if(instruction.op == Operation.NOP) {
                instruction.setOp(Operation.JMP);
            } else if (instruction.op == Operation.JMP) {
                instruction.setOp(Operation.NOP);
            }
        }
        throw new IllegalArgumentException("No solution found for given input");
    }

    // === Parsing

    public static List<Instruction> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day8::parseLine)
                .collect(Collectors.toList());
    }

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');

    public static Instruction parseLine(String line) {
        List<String> elements = SPACE_SPLITTER.splitToList(line);
        return new Instruction(
                Operation.forString(elements.get(0)),
                Integer.parseInt(elements.get(1)));
    }

    // === Classes

    @Value
    public static class Return {
        ReturnCode returnCode;
        int acc;
    }

    public enum ReturnCode {
        SUCCESS,
        LOOP,
        OUT_OF_BOUNDS
    }

    @Data
    @AllArgsConstructor
    public static class Instruction {
        Operation op;
        int arg;
    }

    public enum Operation {
        ACC,
        JMP,
        NOP;

        private static final Map<String, Operation> NAME_MAP = Arrays.stream(Operation.values())
                .collect(Collectors.toMap(op -> op.name().toLowerCase(), op -> op));

        public static Operation forString(String string) {
            return NAME_MAP.get(string);
        }
    }

}
