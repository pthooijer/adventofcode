package nl.pthooijer.aoc2022.day16;

import com.google.common.base.Splitter;
import nl.pthooijer.util.Debug;
import nl.pthooijer.util.ResourceHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day16 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/16/sample-input1.txt";
        String input = "aoc/2022/day/16/input.txt";

        Debug.measureAndPrint(() -> "Sample1 Part 1 answer (must be 1651): " + part1(sampleInput1));
        Debug.measureAndPrint(() -> "Part 1 answer: " + part1(input));

        Debug.measureAndPrint(() -> "Sample1 Part 2 answer (must be 1707): " + part2(sampleInput1));
        Debug.measureAndPrint(() -> "Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Map<String, Valve> input = ResourceHelper.getLinesStream(resource)
                .map(Day16::parseLine)
                .collect(Collectors.toMap(Valve::name, valve -> valve));

        String start = "AA";
        CacheKey state = new CacheKey(start, 0, new HashSet<>(), true);

        Map<CacheKey, Long> cache = new HashMap<>();

        return findBest1(state, input, cache);
    }


    private record CacheKey(String location, long minute, Set<String> open, boolean player1) {
    }

    public static long findBest1(CacheKey state, Map<String, Valve> input, Map<CacheKey, Long> cache) {
        if (state.minute == 30) {
            return 0;
        }
        if (cache.containsKey(state)) {
            return cache.get(state);
        }

        // calculate cacheKey value
        long bestReleased = Long.MIN_VALUE;
        Valve valve = input.get(state.location);
        if (!state.open.contains(state.location)) {
            long rate = valve.rate;
            if (rate > 0) {
                Set<String> newOpen = new HashSet<>(state.open);
                newOpen.add(state.location);
                long bestWhenStay = findBest1(new CacheKey(state.location, state.minute + 1, newOpen, state.player1), input, cache);
                long newReleased = bestWhenStay + rate * (30 - state.minute - 1);
                bestReleased = newReleased;
            }
        }

        for (String neighbour : valve.to) {
            long bestWhenMove = findBest1(new CacheKey(neighbour, state.minute + 1, state.open, state.player1), input, cache);
            bestReleased = Math.max(bestReleased, bestWhenMove);
        }

        cache.put(state, bestReleased);
        return bestReleased;
    }

    public static long findBest2(CacheKey state, Map<String, Valve> input, Map<CacheKey, Long> cache) {
        if (state.minute == 26) {
            if (state.player1) {
                state = new CacheKey("AA", 0, state.open, false);
            } else {
                return 0;
            }
        }
        if (cache.containsKey(state)) {
            return cache.get(state);
        }

        // calculate cacheKey value
        long bestReleased = Long.MIN_VALUE;
        Valve valve = input.get(state.location);
        if (!state.open.contains(state.location)) {
            long rate = valve.rate;
            if (rate > 0) {
                Set<String> newOpen = new HashSet<>(state.open);
                newOpen.add(state.location);
                long bestWhenStay = findBest2(new CacheKey(state.location, state.minute + 1, newOpen, state.player1), input, cache);
                long newReleased = bestWhenStay + rate * (26 - state.minute - 1);
                bestReleased = newReleased;
            }
        }

        for (String neighbour : valve.to) {
            long bestWhenMove = findBest2(new CacheKey(neighbour, state.minute + 1, state.open, state.player1), input, cache);
            bestReleased = Math.max(bestReleased, bestWhenMove);
        }

        cache.put(state, bestReleased);
        return bestReleased;
    }

    public static Object part2(String resource) {
        Map<String, Valve> input = ResourceHelper.getLinesStream(resource)
                .map(Day16::parseLine)
                .collect(Collectors.toMap(Valve::name, valve -> valve));

        String start = "AA";
        CacheKey state = new CacheKey(start, 0, new HashSet<>(), true);

        Map<CacheKey, Long> cache = new HashMap<>();

        return findBest2(state, input, cache);
    }

    private static final Pattern LINE_PATTERN = Pattern.compile(
            "Valve (?<name>\\D\\D) has flow rate=(?<rate>\\d+); tunnels? leads? to valves? (?<to>.*)");
    private static final Splitter LIST_SPLITTER = Splitter.on(", ");

    public static Valve parseLine(String line) {
        Matcher matcher = LINE_PATTERN.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(line);
        }

        String name = matcher.group("name");
        long rate = Long.parseLong(matcher.group("rate"));
        List<String> to = LIST_SPLITTER.splitToList(matcher.group("to"));
        return new Valve(name, rate, to);
    }

    public record Valve(String name, long rate, List<String> to) {
    }
}
