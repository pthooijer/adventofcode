package nl.pthooijer.aoc2022.day17;

import com.google.common.base.Splitter;
import com.google.common.hash.Funnel;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.math.LongMath;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Day17 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/17/sample-input1.txt";
        String input = "aoc/2022/day/17/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 3068): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 1514285714288): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Character> jets = ResourceHelper.getLinesIt(resource).iterator().next()
                .codePoints()
                .mapToObj(i -> (char) i)
                .toList();

        char[][] arr = new char[1][7];
        Arrays.fill(arr[0], '#');
        Grid grid = new Grid(arr, 0, 0);

        return simulateSteps(jets, grid, 2022); // -1 because floor
    }

    public static Object part2(String resource) {
        List<Character> jets = ResourceHelper.getLinesIt(resource).iterator().next()
                .codePoints()
                .mapToObj(i -> (char) i)
                .toList();

        char[][] arr = new char[1][7];
        Arrays.fill(arr[0], '#');
        Grid grid = new Grid(arr, 0, 0);

        return simulateSteps(jets, grid, 1000000000000L);
    }

    private static long simulateSteps(List<Character> jets, Grid grid, long steps) {
        Map<HashCode, CacheValue> cache = new HashMap<>();

        for (long i = 0; i < steps; i++) {
            grid = resizeGrid(grid);

            char[][] rock = ROCKS.get((int) (i % ROCKS.size()));

            CacheKey cacheKey = new CacheKey(grid.arr, grid.nextJetIndex, rock);
            HashCode cacheHash = createHash(cacheKey); // default .hashCode() is only an int and causes hash collisions. That's a fun bug to debug!
            long height = calculateHeight(grid);
            if (!cache.containsKey(cacheHash)) {
                cache.put(cacheHash, new CacheValue(i, height));
                grid = simulateRock(jets, grid, rock);
            } else { // only used in part 2
                long cycleStart = cache.get(cacheHash).index;
                long cycleLength = i - cycleStart;

                long startHeight = cache.get(cacheHash).height;
                long cycleHeight = height - startHeight;

                long nrOfCycles = LongMath.divide(steps - cycleStart, cycleLength, RoundingMode.DOWN);

                long todoAfterLastCycle = steps - cycleStart - (nrOfCycles * cycleLength);


                for (int j = 0; j < todoAfterLastCycle; j++) {
                    grid = resizeGrid(grid);
                    char[][] rock2 = ROCKS.get((int) ((i + j) % ROCKS.size()));
                    grid = simulateRock(jets, grid, rock2);
                }
                long tailHeight = calculateHeight(grid);
                long tailHeightDiff = tailHeight - height;


                long result = startHeight + (cycleHeight * nrOfCycles) + tailHeightDiff;
                return result - 1; // -1 because of floor
            }

        }

        return calculateHeight(grid) - 1; // -1 because of floor
    }

    private static Grid simulateRock(List<Character> jets, Grid grid, char[][] rock) {
        Point position = SPAWN_POS;
        int jetIndex = grid.nextJetIndex;
        while (true) {
            // jet stream first
            char jet = jets.get(jetIndex);
            jetIndex = (jetIndex + 1) % jets.size();
            int move = jet == '>' ? 1 : -1;
            Point newPos = new Point(position.x, position.y + move);
            boolean collision = checkCollision(grid.arr, rock, newPos);
            position = collision ? position : newPos;

            // then gravity
            newPos = new Point(position.x + 1, position.y);
            collision = checkCollision(grid.arr, rock, newPos);
            if (collision) {
                drawRock(grid.arr, rock, position);
                grid = new Grid(grid.arr, grid.offset, jetIndex);
//                    System.out.println(Debug.toString(grid.arr));
                break;
            } else {
                position = newPos;
            }
        }
        return grid;
    }

    private static void drawRock(char[][] grid, char[][] rock, Point position) {
        for (int h = 0; h < 4; h++) {
            for (int w = 0; w < rock[0].length; w++) {
                if (rock[h][w] == '#') {
                    int hGrid = h + position.x - 3;
                    int wGrid = w + position.y;
                    grid[hGrid][wGrid] = '#';
                }
            }
        }
    }

    private static boolean checkCollision(char[][] grid, char[][] rock, Point position) {
        for (int h = 0; h < 4; h++) {
            for (int w = 0; w < rock[0].length; w++) {
                if (rock[h][w] == '#') {
                    int hGrid = h + position.x - 3;
                    int wGrid = w + position.y;
                    if (!ArrayUtils.isInBounds(grid, hGrid, wGrid) || grid[hGrid][wGrid] != '.') {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static final char[] EMPTY_ROW = ".......".toCharArray();
    private static final Point SPAWN_POS = new Point(3, 2);
    private static final int MAX_ARR_SIZE = 100; // arbitrary

    private static Grid resizeGrid(Grid grid) {
        long highestNonEmptyRow = getHighestNonEmptyRow(grid);

        long rowsToAdd = 7 - highestNonEmptyRow;
        if (rowsToAdd <= 0) {
            return grid;
        }

        int rowsToRemove = Math.max(0, grid.arr.length + (int) rowsToAdd - MAX_ARR_SIZE);

        char[][] newGrid = new char[grid.arr.length + (int) rowsToAdd - rowsToRemove][grid.arr[0].length];
        System.arraycopy(grid.arr, 0, newGrid, (int) rowsToAdd, grid.arr.length - rowsToRemove);
        for (int i = 0; i < rowsToAdd; i++) {
            System.arraycopy(EMPTY_ROW, 0, newGrid[i], 0, EMPTY_ROW.length);
        }

        return new Grid(newGrid, grid.offset + rowsToRemove, grid.nextJetIndex);
    }

    private record Grid(char[][] arr, long offset, int nextJetIndex) {
    }

    private static long getHighestNonEmptyRow(Grid grid) {
        int highestNonEmptyRow = -1;
        for (int i = 0; i < grid.arr.length; i++) {
            if (Arrays.mismatch(grid.arr[i], EMPTY_ROW) != -1) {
                highestNonEmptyRow = i;
                break;
            }
        }
        return highestNonEmptyRow;
    }

    private static long calculateHeight(Grid grid) {
        long highestNonEmptyRow = getHighestNonEmptyRow(grid);
        return grid.arr.length - highestNonEmptyRow + grid.offset;
    }

    private record CacheKey(char[][] arr, int nextJetIndex, char[][] rock) {
    }

    private record CacheValue(long index, long height) {
    }

    private static final HashFunction HASH_FUNCTION = Hashing.goodFastHash(128);
    private static final Funnel<char[][]> ARRAY_FUNNEL = (arr, sink) -> {
        for (char[] row : arr) {
            for (char c : row) {
                sink.putChar(c);
            }
        }
    };

    private static HashCode createHash(CacheKey cacheKey) {
        return HASH_FUNCTION.newHasher()
                .putObject(cacheKey.arr, ARRAY_FUNNEL)
                .putInt(cacheKey.nextJetIndex)
                .putObject(cacheKey.rock, ARRAY_FUNNEL)
                .hash();
    }

    // all rocks are resized to 4 high
    private static final List<char[][]> ROCKS = Stream.of(
                    """
                            ....
                            ....
                            ....
                            ####""",
                    """
                            ...
                            .#.
                            ###
                            .#.""",
                    """
                            ...
                            ..#
                            ..#
                            ###""",
                    """
                            #
                            #
                            #
                            #""",
                    """
                            ..
                            ..
                            ##
                            ##""")
            .map(block -> Splitter.on('\n').splitToStream(block)
                    .map(String::toCharArray)
                    .toArray(char[][]::new))
            .toList();
}
