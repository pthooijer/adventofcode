package nl.pthooijer.aoc2022.day21;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class Day21 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/21/sample-input1.txt";
        String input = "aoc/2022/day/21/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 152): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 301): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Map<String, Object> map = parseInput(resource);

        return resolveMonkey(map, "root");
    }

    private static final Splitter EQ_SPLITTER = Splitter.on(' ');

    private static Long resolveMonkey(Map<String, Object> map, String monkey) {
        Object value = map.get(monkey);
        if (value == null) {
            return null;
        } else if (value instanceof Long l) {
            return l;
        }
        String equation = (String) value;
        List<String> words = EQ_SPLITTER.splitToList(equation);
        Long monkey1 = resolveMonkey(map, words.get(0));
        Long monkey2 = resolveMonkey(map, words.get(2));

        if (monkey1 == null || monkey2 == null) {
            return null;
        }

        long result = switch (words.get(1)) {
            case "+" -> monkey1 + monkey2;
            case "-" -> monkey1 - monkey2;
            case "*" -> monkey1 * monkey2;
            case "/" -> monkey1 / monkey2;
            default -> throw new IllegalArgumentException(equation);
        };
        map.put(monkey, result);
        return result;
    }

    private static final Splitter SPLITTER = Splitter.on(": ");

    public static Map<String, Object> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(SPLITTER::splitToList)
                .collect(toMap(words -> words.get(0), words -> {
                    String word = words.get(1);
                    return Character.isDigit(word.charAt(0)) ? Long.parseLong(word) : word;
                }));
    }

    public static Object part2(String resource) {
        Map<String, Object> map = parseInput(resource);
        map.put("humn", null);

        List<String> words = EQ_SPLITTER.splitToList((String) map.get("root"));
        String name1 = words.get(0);
        String name2 = words.get(2);

        // first find which monkey is dependent on humn
        // this also resolves all equations we can already solve and puts them in the map
        Long value1 = resolveMonkey(map, name1);
        Long value2 = resolveMonkey(map, name2);

        //reverse all equations
        if (value1 == null) {
            return resolveMonkeyReversed(map, name1, value2);
        } else {
            return resolveMonkeyReversed(map, name2, value1);
        }
    }

    private static long resolveMonkeyReversed(Map<String, Object> map, String monkey, long value) {
        String equation = (String) map.get(monkey);
        List<String> words = EQ_SPLITTER.splitToList(equation);
        String name1 = words.get(0);
        String name2 = words.get(2);

        Object value1 = map.get(name1);
        Object value2 = map.get(name2);
        if (value2 instanceof Long l2) {
            long result1 = switch (words.get(1)) {
                case "+" -> value - l2;
                case "-" -> value + l2;
                case "*" -> value / l2;
                case "/" -> value * l2;
                default -> throw new IllegalArgumentException(equation);
            };
            return name1.equals("humn") ? result1 : resolveMonkeyReversed(map, name1, result1);
        } else if (value1 instanceof Long l1) {
            long result2 = switch (words.get(1)) {
                case "+" -> value - l1;
                case "-" -> l1 - value;
                case "*" -> value / l1;
                case "/" -> l1 / value;
                default -> throw new IllegalArgumentException(equation);
            };
            return name2.equals("humn") ? result2 : resolveMonkeyReversed(map, name2, result2);
        }
        throw new IllegalStateException();
    }
}
