package nl.pthooijer.aoc2022.day10;

import nl.pthooijer.util.ResourceHelper;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day10 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/10/sample-input1.txt";
        String input = "aoc/2022/day/10/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 13140): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer:\n" + part2(sampleInput1));
        System.out.println("Part 2 answer:\n" + part2(input));
    }

    public static Object part1(String resource) {
        Set<Integer> interestingCycles = Set.of(20, 60, 100, 140, 180, 220);
        int x = 1;
        int cycle = 0;
        int result = 0;
        for (String line : ResourceHelper.getLinesIt(resource)) {
            cycle++;
            if (interestingCycles.contains(cycle)) {
                result += cycle * x;
            }
            if (line.startsWith("addx")) {
                cycle++;
                if (interestingCycles.contains(cycle)) {
                    result += cycle * x;
                }
                x += Integer.parseInt(line.substring(5));
            }
        }

        return result;
    }

    public static Object part2(String resource) {
        char[] grid = new char[240];
        int cycle = 0;
        int x = 1;
        for (String line : ResourceHelper.getLinesIt(resource)) {
            grid[cycle] = Math.abs((cycle % 40) - x) <= 1 ? '#' : '.';
            cycle++;

            if (line.startsWith("addx")) {
                grid[cycle] = Math.abs((cycle % 40) - x) <= 1 ? '#' : '.';
                x += Integer.parseInt(line.substring(5));
                cycle++;
            }
        }

        return IntStream.range(0, 6)
                .mapToObj(i -> new String(grid, i * 40, 40))
                .collect(Collectors.joining("\n"));
    }
}
