package nl.pthooijer.aoc2022.day18;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point3D;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Day18 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/18/sample-input1.txt";
        String input = "aoc/2022/day/18/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 64): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Point3D[] NEIGHBOURS = new Point3D[]{
            new Point3D(1, 0, 0),
            new Point3D(-1, 0, 0),
            new Point3D(0, 1, 0),
            new Point3D(0, -1, 0),
            new Point3D(0, 0, 1),
            new Point3D(0, 0, -1)
    };

    public static Object part1(String resource) {
        Set<Point3D> input = ResourceHelper.getLinesStream(resource)
                .map(Day18::parseLine)
                .collect(Collectors.toSet());

        return input.stream()
                .mapToLong(p -> Arrays.stream(NEIGHBOURS)
                        .filter(nb -> !input.contains(p.plus(nb)))
                        .count())
                .sum();
    }

    private static final Splitter SPLITTER = Splitter.on(',');

    public static Point3D parseLine(String line) {
        int[] coords = SPLITTER.splitToStream(line)
                .mapToInt(Integer::parseInt)
                .toArray();
        return new Point3D(coords[0], coords[1], coords[2]);
    }

    public static Object part2(String resource) {
        Set<Point3D> input = ResourceHelper.getLinesStream(resource)
                .map(Day18::parseLine)
                .collect(Collectors.toSet());

        int minX = input.stream().mapToInt(Point3D::getX).min().orElseThrow() - 1;
        int minY = input.stream().mapToInt(Point3D::getY).min().orElseThrow() - 1;
        int minZ = input.stream().mapToInt(Point3D::getZ).min().orElseThrow() - 1;
        int maxX = input.stream().mapToInt(Point3D::getX).max().orElseThrow() + 1;
        int maxY = input.stream().mapToInt(Point3D::getY).max().orElseThrow() + 1;
        int maxZ = input.stream().mapToInt(Point3D::getZ).max().orElseThrow() + 1;

        Point3D start = new Point3D(minX, minY, minZ);
        if(input.contains(start)) {
            throw new IllegalStateException("Bad first guess");
        }

        Deque<Point3D> toCheck = new ArrayDeque<>();
        toCheck.add(start);
        Set<Point3D> seen = new HashSet<>();

        long surfaceCount = 0;
        while (!toCheck.isEmpty()) {
            Point3D current = toCheck.removeFirst();
            for (Point3D rnb : NEIGHBOURS) {
                Point3D nb = current.plus(rnb);
                if(input.contains(nb)) {
                    surfaceCount++;
                } else if (!seen.contains(nb)) {
                    if (nb.x >= minX && nb.x <= maxX && nb.y >= minY && nb.y <= maxY && nb.z >= minZ && nb.z <= maxZ) {
                        toCheck.addLast(nb);
                        seen.add(nb);
                    }
                }
            }
        }


        return surfaceCount;
    }
}
