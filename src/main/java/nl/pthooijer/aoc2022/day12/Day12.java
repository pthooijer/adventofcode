package nl.pthooijer.aoc2022.day12;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.stream.IntStream;

// Ugly brute forcing A*-algorithms today, can be neater using other path searching algorithms
public class Day12 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/12/sample-input1.txt";
        String input = "aoc/2022/day/12/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 31): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 29): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final List<Point> DIRECTIONS = List.of(new Point(1, 0), new Point(-1, 0), new Point(0, 1), new Point(0, -1));

    public static Object part1(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);
        Point start = find(grid, 'S');
        Point end = find(grid, 'E');

        Map<Point, Point> cameFrom = aStartAlgorithm(grid, start, end);
        return reconstructPath(Objects.requireNonNull(cameFrom), start, end);
    }

    public static Object part2(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);
        Point end = find(grid, 'E');

        List<Point> aPoints = new ArrayList<>(findAll(grid, 0));
        Map<Point, Long> aScores = new HashMap<>();

        while (!aPoints.isEmpty()) {
            Point aNext = aPoints.remove(0);
            Map<Point, Point> cameFrom = aStartAlgorithm(grid, aNext, end);
            if (cameFrom == null) {
                continue;
            }

            long count = 0;
            Point current = end;
            while (true) {
                count++;
                Point prev = cameFrom.get(current);
                if (height(grid, prev) == 0) {
                    aScores.put(prev, count);
                }
                if (prev.equals(aNext)) {
                    break;
                }
                current = prev;
            }

            aPoints.removeAll(aScores.keySet());
        }


        return aScores.values().stream()
                .min(Comparator.naturalOrder())
                .orElseThrow();
    }

    public static Map<Point, Point> aStartAlgorithm(char[][] grid, Point start, Point end) {
        Map<Point, Point> cameFrom = new HashMap<>();
        Map<Point, Long> gScore = new HashMap<>(); // default Long.MAX;
        Map<Point, Long> fScore = new HashMap<>();
        PriorityQueue<Point> openSet = new PriorityQueue<>(Comparator.comparingLong(fScore::get)); //lowest fScore first

        gScore.put(start, 0L);
        fScore.put(start, hFunction(grid, start));
        openSet.add(start);

        while (!openSet.isEmpty()) {
            Point current = openSet.remove();
            if (current.equals(end)) {
                return cameFrom;
            }

            for (Point direction : DIRECTIONS) {
                Point neighbour = current.plus(direction);
                if (ArrayUtils.isInBounds(grid, neighbour.x, neighbour.y)) {
                    int heightDiff = height(grid, neighbour) - height(grid, current);
                    if (heightDiff <= 1) {
                        long tentativeGScore = gScore.get(current) + 1L;
                        if (tentativeGScore < gScore.getOrDefault(neighbour, Long.MAX_VALUE)) {
                            cameFrom.put(neighbour, current);
                            gScore.put(neighbour, tentativeGScore);
                            fScore.put(neighbour, tentativeGScore + hFunction(grid, neighbour));
                            if (!openSet.contains(neighbour)) {
                                openSet.add(neighbour);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }


    public static int height(char[][] grid, Point p) {
        char height = grid[p.x][p.y];
        height = switch (height) {
            case 'S' -> 'a';
            case 'E' -> 'z';
            default -> height;
        };
        return height - 'a';
    }

    public static long hFunction(char[][] grid, Point p) {
        return height(grid, p) - 26;
    }

    public static Point find(char[][] grid, char toFind) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == toFind) {
                    return new Point(i, j);
                }
            }
        }
        throw new IllegalArgumentException();
    }

    public static List<Point> findAll(char[][] grid, int height) {
        return IntStream.range(0, grid.length).boxed()
                .flatMap(i -> IntStream.range(0, grid[i].length)
                        .mapToObj(j -> new Point(i, j)))
                .filter(p -> height(grid, p) == height)
                .toList();
    }

    public static long reconstructPath(Map<Point, Point> cameFrom, Point start, Point end) {
        long count = 0;
        Point current = end;
        while (true) {
            count++;
            Point prev = cameFrom.get(current);
            if (prev.equals(start)) {
                return count;
            }
            current = prev;
        }
    }
}
