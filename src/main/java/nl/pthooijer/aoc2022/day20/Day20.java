package nl.pthooijer.aoc2022.day20;

import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Day20 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/20/sample-input1.txt";
        String input = "aoc/2022/day/20/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 3): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Long> linesList = ResourceHelper.getLongsList(resource);
        List<Element> input = new ArrayList<>();
        for (int i = 0; i < linesList.size(); i++) {
            Element element1 = new Element(linesList.get(i), i);
            input.add(element1);
        }

        LinkedList<Element> list = new LinkedList<>(input);
        mixList(input, list);

        return calculateCoords(list);
    }

    private static long calculateCoords(List<Element> list) {
        int zeroPos = IntStream.range(0, list.size())
                .filter(i -> list.get(i).value == 0)
                .findFirst().orElseThrow();
        long nr1000 = list.get((zeroPos + 1000) % list.size()).value;
        long nr2000 = list.get((zeroPos + 2000) % list.size()).value;
        long nr3000 = list.get((zeroPos + 3000) % list.size()).value;

        return nr1000 + nr2000 + nr3000;
    }

    private static void mixList(List<Element> input, LinkedList<Element> list) {
        for (Element element : input) {
            int currentPos = list.indexOf(element);
            long newPos = currentPos + element.value;

            newPos = Math.floorMod(newPos, list.size() - 1);

            list.remove(currentPos);
            list.add((int) newPos, element);
        }
    }

    public record Element(long value, int orgIndex) {
    }

    public static Object part2(String resource) {
        long decryptionKey = 811589153;
        List<Long> linesList = ResourceHelper.getLongsList(resource);
        List<Element> input = new ArrayList<>();
        for (int i = 0; i < linesList.size(); i++) {
            Element element1 = new Element(linesList.get(i) * decryptionKey, i);
            input.add(element1);
        }

        LinkedList<Element> list = new LinkedList<>(input);
        for(int i=0; i<10; i++) {
            mixList( input, list);
        }

        return calculateCoords(list);
    }
}
