package nl.pthooijer.aoc2022.day8;

import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;

public class Day8 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/8/sample-input1.txt";
        String input = "aoc/2022/day/8/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 21): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 8): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        int[][] grid = ResourceHelper.getLinesStream(resource)
                .map(line -> line.codePoints()
                        .map(cp -> cp - '0')
                        .toArray())
                .toArray(int[][]::new);

        int[][] visible = new int[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            // left to right
            int top = -1;
            for (int j = 0; j < grid[i].length; j++) {
                int height = grid[i][j];
                if (height > top) {
                    top = height;
                    visible[i][j] = 1;
                }
            }

            // right to left
            top = -1;
            for (int j = grid[i].length - 1; j >= 0; j--) {
                int height = grid[i][j];
                if (height > top) {
                    top = height;
                    visible[i][j] = 1;
                }
            }
        }

        for (int j = 0; j < grid.length; j++) {
            // up to down
            int top = -1;
            for (int i = 0; i < grid[j].length; i++) {
                int height = grid[i][j];
                if (height > top) {
                    top = height;
                    visible[i][j] = 1;
                }
            }

            // down to up
            top = -1;
            for (int i = grid[j].length - 1; i >= 0; i--) {
                int height = grid[i][j];
                if (height > top) {
                    top = height;
                    visible[i][j] = 1;
                }
            }
        }

        return Arrays.stream(visible)
                .flatMapToInt(Arrays::stream)
                .sum();
    }

    public static Object part2(String resource) {
        int[][] grid = ResourceHelper.getLinesStream(resource)
                .map(line -> line.codePoints()
                        .map(cp -> cp - '0')
                        .toArray())
                .toArray(int[][]::new);

        int maxScore = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                int base = grid[i][j];

                // look up
                int leftScore = 0;
                for (int p = i - 1; p >= 0; p--) {
                    int height = grid[p][j];
                    leftScore++;
                    if (height >= base) {
                        break;
                    }
                }

                // look down
                int rightScore = 0;
                for (int p = i + 1; p < grid.length; p++) {
                    int height = grid[p][j];
                    rightScore++;
                    if (height >= base) {
                        break;
                    }
                }

                // look left
                int upScore = 0;
                for (int q = j - 1; q >= 0; q--) {
                    int height = grid[i][q];
                    upScore++;
                    if (height >= base) {
                        break;
                    }
                }

                // look right
                int downScore = 0;
                for (int q = j + 1; q < grid[0].length; q++) {
                    int height = grid[i][q];
                    downScore++;
                    if (height >= base) {
                        break;
                    }
                }

                int score = leftScore * rightScore * upScore * downScore;
                if(score > maxScore) {
                    maxScore = score;
                }
            }
        }


        return maxScore;
    }
}
