package nl.pthooijer.aoc2022.day2;

import nl.pthooijer.util.ResourceHelper;

public class Day2 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/2/sample-input1.txt";
        String input = "aoc/2022/day/2/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 15): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 12): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .mapToInt(line -> {
                    int opp = line.charAt(0) - 'A'; // 0 1 2
                    int me = line.charAt(2) - 'X'; // 0 1 2

                    int result = Math.floorMod(me - opp + 1, 3);
                    return 3 * result + (me + 1);
                })
                .sum();
    }

    public static Object part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .mapToInt(line -> {
                    int opp = line.charAt(0) - 'A'; // 0 1 2
                    int result = line.charAt(2) - 'X'; // 0 1 2

                    int me = Math.floorMod(result + opp - 1, 3);
                    return  3 * result + (me + 1);
                })
                .sum();
    }
}
