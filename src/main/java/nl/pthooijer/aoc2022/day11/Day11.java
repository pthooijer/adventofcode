package nl.pthooijer.aoc2022.day11;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import com.google.common.math.LongMath;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.MathUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.SeparatorPolicy;

import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.LongUnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day11 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/11/sample-input1.txt";
        String input = "aoc/2022/day/11/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 10605): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 2713310158): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Monkey> monkeyList = parseInput(resource);
        int nrOfRounds = 20;

        Map<Integer, Long> monkeyInspectionTally = new HashMap<>();
        for (int i = 0; i < nrOfRounds; i++) {
            for (Monkey monkey : monkeyList) {
                while (!monkey.items.isEmpty()) {
                    long item = monkey.items.remove();
                    long worry = monkey.operation.applyAsLong(item);
                    worry = LongMath.divide(worry, 3, RoundingMode.DOWN);
                    int target = (worry % monkey.testDivisible) == 0 ? monkey.targetTrue : monkey.targetFalse;
                    monkeyList.get(target).items.add(worry);
                    monkeyInspectionTally.merge(monkey.id, 1L, Long::sum);
                }
            }
        }

        return monkeyInspectionTally.values().stream()
                .sorted(Comparator.reverseOrder())
                .limit(2)
                .reduce(1L, LongMath::checkedMultiply);
    }

    public static Object part2(String resource) {
        List<Monkey> monkeyList = parseInput(resource);
        int nrOfRounds = 10_000;

        // Later noticed that all divisibility tests are prime, so lcd(divisors) = product(divisors)
        long lcmOfDiv = monkeyList.stream()
                .mapToLong(Monkey::testDivisible)
                .reduce(MathUtils::lcm)
                .orElseThrow();

        Map<Integer, Long> monkeyInspectionTally = new HashMap<>();
        for (int i = 0; i < nrOfRounds; i++) {
            for (Monkey monkey : monkeyList) {
                while (!monkey.items.isEmpty()) {
                    long item = monkey.items.remove();
                    long worry = monkey.operation.applyAsLong(item) % lcmOfDiv;
                    int target = (worry % monkey.testDivisible) == 0 ? monkey.targetTrue : monkey.targetFalse;
                    monkeyList.get(target).items.add(worry);
                    monkeyInspectionTally.merge(monkey.id, 1L, Long::sum);
                }
            }
        }

        return monkeyInspectionTally.values().stream()
                .sorted(Comparator.reverseOrder())
                .limit(2)
                .reduce(1L, LongMath::checkedMultiply);
    }

    private static List<Monkey> parseInput(String resource) {
        var joinIterator = new JoinIterator<>(ResourceHelper.getLinesIt(resource).iterator(),
                String::isBlank,
                SeparatorPolicy.DROP,
                Collectors.collectingAndThen(Collectors.joining("\n"), Day11::parseMonkey));
        return Streams.stream(joinIterator)
                .toList();
    }

    private static final Pattern MONKEY_PATTERN = Pattern.compile("""
            Monkey (\\d+):
              Starting items: (.*)
              Operation: (.*)
              Test: divisible by (\\d+)
                If true: throw to monkey (\\d+)
                If false: throw to monkey (\\d+)""");
    private static final Splitter LIST_SPLITTER = Splitter.on(", ");

    private static Monkey parseMonkey(String textBlock) {
        Matcher matcher = MONKEY_PATTERN.matcher(textBlock);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(textBlock);
        }

        int id = Integer.parseInt(matcher.group(1));
        Queue<Long> items = LIST_SPLITTER.splitToStream(matcher.group(2))
                .map(Long::parseLong)
                .collect(Collectors.toCollection(ArrayDeque::new));
        Operation operation = parseOperation(matcher.group(3));
        long testDivisible = Long.parseLong(matcher.group(4));
        int targetTrue = Integer.parseInt(matcher.group(5));
        int targetFalse = Integer.parseInt(matcher.group(6));

        return new Monkey(id, items, operation, testDivisible, targetTrue, targetFalse);
    }

    private static Operation parseOperation(String line) {
        if (line.charAt(10) == '+') {
            return new Operation(old -> old + Integer.parseInt(line.substring(12)), line);
        } else if (line.charAt(10) == '*') {
            if (line.substring(12).equals("old")) {
                return new Operation(old -> old * old, line);
            } else {
                return new Operation(old -> old * Integer.parseInt(line.substring(12)), line);
            }
        }
        throw new IllegalArgumentException(line);
    }

    private record Operation(LongUnaryOperator operator, String string) implements LongUnaryOperator {
        @Override
        public long applyAsLong(long operand) {
            return operator.applyAsLong(operand);
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public record Monkey(int id, Queue<Long> items, Operation operation,
                         long testDivisible, int targetTrue, int targetFalse) {
    }
}
