package nl.pthooijer.aoc2022.day6;

import nl.pthooijer.util.ResourceHelper;

public class Day6 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/6/sample-input1.txt";
        String input = "aoc/2022/day/6/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 11): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 26): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return findHeader(4, resource);
    }

    public static Object part2(String resource) {
        return findHeader(14, resource);
    }

    public static Object findHeader(int length, String resource) {
        String data = ResourceHelper.getLinesIt(resource).iterator().next();
        for (int i = length; i <= data.length(); i++) {
            if (data.substring(i - length, i).codePoints().distinct().count() == length) {
                return i;
            }
        }
        throw new IllegalArgumentException();
    }
}
