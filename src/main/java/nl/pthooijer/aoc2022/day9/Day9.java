package nl.pthooijer.aoc2022.day9;

import nl.pthooijer.util.point.Point;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Day9 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/9/sample-input1.txt";
        String sampleInput2 = "aoc/2022/day/9/sample-input2.txt";
        String input = "aoc/2022/day/9/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 13): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 1): " + part2(sampleInput1));
        System.out.println("Sample2 Part 2 answer (must be 36): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Point head = new Point(0, 0);
        Point tail = new Point(0, 0);
        Set<Point> tailHits = new HashSet<>();
        tailHits.add(tail);

        for (String line : ResourceHelper.getLinesIt(resource)) {
            Point moveVector = switch (line.charAt(0)) {
                case 'R' -> new Point(1, 0);
                case 'L' -> new Point(-1, 0);
                case 'D' -> new Point(0, 1);
                case 'U' -> new Point(0, -1);
                default -> throw new IllegalArgumentException();
            };

            int mag = Integer.parseInt(line.substring(2));
            for (int i = 0; i < mag; i++) {
                head = head.plus(moveVector);
                Point diff = head.minus(tail);

                int xMove = Integer.compare(diff.x, 0); // -1 0 or 1
                int yMove = Integer.compare(diff.y, 0); // -1 0 or 1
                Point newTail = tail.plus(new Point(xMove, yMove));
                if (!newTail.equals(head)) {
                    tailHits.add(newTail);
                    tail = newTail;
                }
            }
        }

        return tailHits.size();
    }

    public static Object part2(String resource) {
        int nrOfKnots = 9;
        Point[] knots = new Point[nrOfKnots + 1];
        Arrays.fill(knots, new Point(0, 0));

        Set<Point> tailHits = new HashSet<>();
        tailHits.add(knots[knots.length - 1]);

        for (String line : ResourceHelper.getLinesIt(resource)) {
            char dir = line.charAt(0);
            int mag = Integer.parseInt(line.substring(2));

            Point moveVector = switch (dir) {
                case 'R' -> new Point(1, 0);
                case 'L' -> new Point(-1, 0);
                case 'D' -> new Point(0, 1);
                case 'U' -> new Point(0, -1);
                default -> throw new IllegalArgumentException();
            };

            for (int i = 0; i < mag; i++) {
                knots[0] = knots[0].plus(moveVector);

                for (int k = 1; k < knots.length; k++) {
                    Point diff = knots[k - 1].minus(knots[k]);

                    int xMove = Integer.compare(diff.x, 0);
                    int yMove = Integer.compare(diff.y, 0);
                    Point newKnot = knots[k].plus(new Point(xMove, yMove));
                    if (!newKnot.equals(knots[k - 1])) {
                        knots[k] = newKnot;
                    }
                }
                tailHits.add(knots[knots.length - 1]);
            }
        }

        return tailHits.size();
    }
}
