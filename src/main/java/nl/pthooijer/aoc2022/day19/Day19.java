package nl.pthooijer.aoc2022.day19;

import com.google.common.math.IntMath;
import nl.pthooijer.util.ResourceHelper;

import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static java.lang.Integer.parseInt;

public class Day19 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/19/sample-input1.txt";
        String input = "aoc/2022/day/19/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 33): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 56): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Blueprint> blueprints = ResourceHelper.getLinesStream(resource)
                .map(Day19::parseLine)
                .toList();

        int score = 0;
        for (Blueprint blueprint : blueprints) {
            int[] resources = new int[]{0, 0, 0, 0};
            int[] bots = new int[]{1, 0, 0, 0};
            int geodes = calculateGeodes(blueprint, resources, bots, 24, new AtomicInteger(0));
            score += blueprint.id * geodes;
        }

        return score;
    }

    public static int calculateGeodes(Blueprint blueprint, int[] resources, int[] bots, int minLeft, AtomicInteger bestFound) {
        int maxGeodes = resources[3] + bots[3] * minLeft; // geodes if no more bots are build

        // prune this branch if you have more bots than the most expensive bot
        for (int i = 0; i < 4; i++) {
            if (bots[i] > blueprint.maxCost[i]) {
                return 0;
            }
        }

        // prune this branch if you can't beat the bestFound by building a geode bot every minute
        int geodesIfBotEveryMin = resources[3] + bots[3] * minLeft + (minLeft * (minLeft - 1)) / 2;
        if (geodesIfBotEveryMin <= bestFound.get()) {
            return 0;
        }

        for (int i = 0; i < 4; i++) { // for each bot
            int timeToBuild = timeTillResources(blueprint.buildCostArr[i], resources, bots) + 1; // + 1 = build time
            if (minLeft < timeToBuild) {
                continue;
            }

            // forward clock till you can build this bot
            int[] newResources = resources.clone();
            for (int j = 0; j < 4; j++) {
                newResources[j] += bots[j] * timeToBuild;
                newResources[j] -= blueprint.buildCostArr[i][j];
            }
            // build that bot
            int[] newBots = bots.clone();
            newBots[i]++;
            int newTime = minLeft - timeToBuild;

            // calculate next action
            int geodes = calculateGeodes(blueprint, newResources, newBots, newTime, bestFound);
            maxGeodes = Math.max(maxGeodes, geodes);
        }

        if (maxGeodes > bestFound.get()) {
            bestFound.set(maxGeodes);
        }

        return maxGeodes;
    }

    private static int timeTillResources(int[] cost, int[] resources, int[] bots) {
        int maxTime = 0;
        for (int i = 0; i < 3; i++) {
            if (cost[i] == 0) {
                continue;
            }
            if (bots[i] == 0) {
                return 1_000_000; // infinity
            }
            int resMissing = cost[i] - resources[i];
            if (resMissing <= 0) {
                continue;
            }
            int time = IntMath.divide(resMissing, bots[i], RoundingMode.UP);
            maxTime = Math.max(maxTime, time);
        }

        return maxTime;
    }

    public static Object part2(String resource) {
        List<Blueprint> blueprints = ResourceHelper.getLinesStream(resource)
                .map(Day19::parseLine)
                .toList();

        int score = 1;
        int nrOfBluePrints = resource.endsWith("input.txt") ? 3 : 1;
        for (int i = 0; i < nrOfBluePrints; i++) {
            Blueprint blueprint = blueprints.get(i);
            int[] resources = new int[]{0, 0, 0, 0};
            int[] bots = new int[]{1, 0, 0, 0};
            int geodes = calculateGeodes(blueprint, resources, bots, 32, new AtomicInteger(0));
            score *= geodes;
        }

        return score;
    }

    public static final Pattern PATTERN = Pattern.compile("" +
            "Blueprint (?<id>\\d+): " +
            "Each ore robot costs (?<o4o>\\d+) ore. " +
            "Each clay robot costs (?<o4c>\\d+) ore. " +
            "Each obsidian robot costs (?<o4b>\\d+) ore and (?<c4b>\\d+) clay. " +
            "Each geode robot costs (?<o4g>\\d+) ore and (?<b4g>\\d+) obsidian.");

    private record Blueprint(int id, int[][] buildCostArr, int[] maxCost) {
    }

    private static Blueprint parseLine(String line) {
        Matcher matcher = PATTERN.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalStateException(line);
        }

        int id = parseInt(matcher.group("id"));
        int[][] buildCostArr = new int[][]{
                new int[]{parseInt(matcher.group("o4o")), 0, 0, 0},
                new int[]{parseInt(matcher.group("o4c")), 0, 0, 0},
                new int[]{parseInt(matcher.group("o4b")), parseInt(matcher.group("c4b")), 0, 0},
                new int[]{parseInt(matcher.group("o4g")), 0, parseInt(matcher.group("b4g")), 0}
        };

        int maxOreCost = IntStream.of(buildCostArr[0][0], buildCostArr[1][0], buildCostArr[2][0], buildCostArr[3][0])
                .max().orElseThrow();
        int[] maxCost = new int[]{maxOreCost, buildCostArr[2][1], buildCostArr[3][2], 1_000_000};

        return new Blueprint(id, buildCostArr, maxCost);
    }

}
