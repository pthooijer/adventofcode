package nl.pthooijer.aoc2022.day5;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Day5 {


    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/5/sample-input1.txt";
        String input = "aoc/2022/day/5/input.txt";

        System.out.println("Sample1 Part 1 answer (must be CMZ): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final int NR_OF_STACKS = 9;

    public static Object part1(String resource) {
        List<Deque<Character>> stacks = parseStacks(resource);
        List<Move> moves = parseMoves(resource);

        for (Move move : moves) {
            for (int i = 0; i < move.amount; i++) {
                char c = stacks.get(move.from).removeLast();
                stacks.get(move.to).addLast(c);
            }
        }

        return result(stacks);
    }

    public static List<Deque<Character>> parseStacks(String resource) {
        List<Deque<Character>> stacks = new ArrayList<>();
        for (int i = 0; i <= NR_OF_STACKS; i++) {
            stacks.add(new LinkedList<>());
        }
        for (String line : ResourceHelper.getLinesIt(resource)) {
            for (int i = 1; i <= NR_OF_STACKS; i++) {
                int pos = i * 4 - 3;
                if (pos >= line.length()) {
                    break;
                }
                char c = line.charAt(pos);
                if (Character.isDigit(c)) {
                    return stacks;
                } else if (c != ' ') {
                    stacks.get(i).addFirst(c);
                }
            }
        }
        return stacks;
    }

    private static final Splitter SPLITTER = Splitter.on(' ');

    public static List<Move> parseMoves(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .dropWhile(s -> !s.isBlank())
                .skip(1)
                .map(SPLITTER::splitToList)
                .map(words -> new Move(
                        Integer.parseInt(words.get(1)),
                        Integer.parseInt(words.get(3)),
                        Integer.parseInt(words.get(5))))
                .toList();
    }

    public record Move(int amount, int from, int to) {
    }

    private static String result(List<Deque<Character>> stacks) {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i <= NR_OF_STACKS; i++) {
            Deque<Character> stack = stacks.get(i);
            if (!stack.isEmpty()) {
                builder.append(stack.peekLast());
            }
        }
        return builder.toString();
    }

    public static Object part2(String resource) {
        List<Deque<Character>> stacks = parseStacks(resource);
        List<Move> moves = parseMoves(resource);

        for (Move move : moves) {
            Deque<Character> toMove = new LinkedList<>();
            for (int i = 0; i < move.amount; i++) {
                char c = stacks.get(move.from).removeLast();
                toMove.addFirst(c);
            }
            stacks.get(move.to).addAll(toMove);
        }

        return result(stacks);
    }
}
