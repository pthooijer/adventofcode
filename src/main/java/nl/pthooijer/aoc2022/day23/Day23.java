package nl.pthooijer.aoc2022.day23;

import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day23 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/23/sample-input1.txt";
        String input = "aoc/2022/day/23/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 110): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 20): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Point N = new Point(-1, 0);
    private static final Point NE = new Point(-1, 1);
    private static final Point E = new Point(0, 1);
    private static final Point SE = new Point(1, 1);
    private static final Point S = new Point(1, 0);
    private static final Point SW = new Point(1, -1);
    private static final Point W = new Point(0, -1);
    private static final Point NW = new Point(-1, -1);

    private static final Point[] NEIGHBOURS = new Point[]{N, NE, E, SE, S, SW, W, NW};

    public static Object part1(String resource) {
        return parts(1, resource);
    }


    public static Object part2(String resource) {
        return parts(2, resource);
    }

    public static Object parts(int partNr, String resource) {
        Set<Point> elves = parseInput(resource);

        List<Point[]> propositions = new ArrayList<>();
        propositions.add(new Point[]{N, NE, NW});
        propositions.add(new Point[]{S, SE, SW});
        propositions.add(new Point[]{W, NW, SW});
        propositions.add(new Point[]{E, NE, SE});
        // 0th index is the movement direction

        int i=0;
        while (true){

            List<Point[]> newElves = new ArrayList<>(elves.size()); // array is from old point to new point

            elfLoop:
            for (Point elf : elves) {
                // check if it has neighbours
                boolean hasNeighbours = false;
                for (Point neighbour : NEIGHBOURS) {
                    Point nPoint = elf.plus(neighbour);
                    if (elves.contains(nPoint)) {
                        hasNeighbours = true;
                        break;
                    }
                }

                if (!hasNeighbours) {
                    newElves.add(new Point[]{elf, elf}); // don't move
                    continue;
                }

                for (Point[] proposition : propositions) {
                    boolean canMove = true;
                    for (Point point : proposition) {
                        Point nPoint = elf.plus(point);
                        if (elves.contains(nPoint)) {
                            canMove = false;
                            break;
                        }
                    }

                    if (canMove) {
                        newElves.add(new Point[]{elf, elf.plus(proposition[0])});
                        continue elfLoop;
                    }
                }

                // nothing is free: don't move
                newElves.add(new Point[]{elf, elf});
            }

            // resolve collisions
            Set<Point> collisionPoints = newElves.stream()
                    .map(arr -> arr[1])
                    .collect(Collectors.groupingBy(point -> point, Collectors.counting()))
                    .entrySet().stream()
                    .filter(entry -> entry.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());

            // fill the new positions
            Set<Point> nextElves = new HashSet<>(elves.size());
            for (Point[] newElf : newElves) {
                if (collisionPoints.contains(newElf[1])) {
                    nextElves.add(newElf[0]);
                } else {
                    nextElves.add(newElf[1]);
                }
            }

            // stop part 1
            if ((partNr == 1 && i == 9)) {
                IntSummaryStatistics xMinMax = nextElves.stream()
                        .mapToInt(Point::getX)
                        .summaryStatistics();
                IntSummaryStatistics yMinMax = nextElves.stream()
                        .mapToInt(Point::getY)
                        .summaryStatistics();

                return (xMinMax.getMax() + 1 - xMinMax.getMin()) * (yMinMax.getMax() + 1 - yMinMax.getMin()) - elves.size();
            }

            // stop part 2
            if (nextElves.equals(elves)) {
                return i + 1;
            }

            // set the new positions
            elves = nextElves;

            // rotate the propositions
            Point[] proposition = propositions.remove(0);
            propositions.add(proposition);

            i++;
        }
    }

    public static Set<Point> parseInput(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);
        return IntStream.range(0, grid.length)
                .mapToObj(x -> IntStream.range(0, grid[x].length)
                        .filter(y -> grid[x][y] == '#')
                        .mapToObj(y -> new Point(x, y)))
                .flatMap(Function.identity())
                .collect(Collectors.toSet());
    }
}
