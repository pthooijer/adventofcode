package nl.pthooijer.aoc2022.day13;

import com.google.common.collect.Lists;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.SeparatorPolicy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Day13 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/13/sample-input1.txt";
        String input = "aoc/2022/day/13/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 13): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 140): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Pair> input = parsePairs(resource);

        int sum = 0;
        for (int i = 0; i < input.size(); i++) {
            Pair p = input.get(i);
            if (compareList(p.left, p.right) == -1) {
                sum += i + 1;
            }
        }
        return sum;
    }

    public static Object part2(String resource) {
        List<List<Object>> list = ResourceHelper.getLinesStream(resource)
                .filter(line -> !line.isBlank())
                .map(Day13::parseLine)
                .collect(Collectors.toCollection(ArrayList::new));
        List<Object> div1 = parseLine("[[2]]");
        List<Object> div2 = parseLine("[[6]]");
        list.add(div1);
        list.add(div2);

        Comparator<Object> comparator = Day13::compareObj;
        list.sort(comparator);

        return (list.indexOf(div1) + 1) * (list.indexOf(div2) + 1);
    }

    // -1 = correct, +1 = incorrect
    @SuppressWarnings("unchecked")
    private static int compareObj(Object o1, Object o2) {
        if (o1 instanceof Integer i1) {
            return o2 instanceof Integer i2 ?
                    Integer.compare(i1, i2) :
                    compareList(List.of(i1), (List<Object>) o2);
        } else {
            List<Object> l1 = (List<Object>) o1;
            List<Object> l2 = o2 instanceof Integer i2 ? List.of(i2) : (List<Object>) o2;
            return compareList(l1, l2);
        }
    }

    private static int compareList(List<Object> left, List<Object> right) {
        for (int i = 0; i < left.size(); i++) {
            if (right.size() <= i) {
                return 1;
            }
            int compare = compareObj(left.get(i), right.get(i));
            if (compare != 0) {
                return compare;
            }
        }
        return left.size() < right.size() ? -1 : 0;
    }

    // Parsing

    private static List<Pair> parsePairs(String resource) {
        var joinIterator = new JoinIterator<>(ResourceHelper.getLinesIt(resource).iterator(),
                String::isBlank,
                SeparatorPolicy.DROP,
                Collectors.collectingAndThen(Collectors.toList(),
                        list -> new Pair(parseLine(list.get(0)), parseLine(list.get(1)))));

        return Lists.newArrayList(joinIterator);
    }

    private static List<Object> parseLine(String line) {
        return parseList(toCharIterator(line.substring(1)));
    }

    private static Iterator<Character> toCharIterator(String line) {
        return line.codePoints()
                .mapToObj(i -> (char) i)
                .iterator();
    }

    private static List<Object> parseList(Iterator<Character> lineIt) {
        List<Object> list = new ArrayList<>();
        StringBuilder digitBuilder = new StringBuilder();
        while (lineIt.hasNext()) {
            char c = lineIt.next();
            if (c == ']') {
                if (!digitBuilder.isEmpty()) {
                    list.add(Integer.parseInt(digitBuilder.toString()));
                }
                return list;
            } else if (Character.isDigit(c)) {
                digitBuilder.append(c);
            } else if (c == ',') {
                if (!digitBuilder.isEmpty()) {
                    list.add(Integer.parseInt(digitBuilder.toString()));
                    digitBuilder = new StringBuilder();
                }
            } else if (c == '[') {
                List<Object> innerList = parseList(lineIt);
                list.add(innerList);
            }
        }
        return list;
    }

    private record Pair(List<Object> left, List<Object> right) {
    }
}
