package nl.pthooijer.aoc2022.day7;

import lombok.Getter;
import lombok.ToString;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Day7 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/7/sample-input1.txt";
        String input = "aoc/2022/day/7/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 95437): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 24933642): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Pattern FILE_PATTERN = Pattern.compile("(\\d+) (.*)");

    public static Object part1(String resource) {
        Dir root = parseSystem(resource);

        return getAllDirsRecursively(root)
                .mapToLong(Dir::getSize)
                .filter(size -> size <= 100_000)
                .sum();
    }

    private static Dir parseSystem(String resource) {
        Iterator<String> it = ResourceHelper.getLinesIt(resource).iterator();

        Dir root = new Dir("/", null);
        Dir pointer = root;
        it.next(); // == '$ cd /'
        String line = it.next();
        while (it.hasNext()) {
            if (line.startsWith("$ ls")) {
                while (it.hasNext()) {
                    line = it.next();
                    if (line.charAt(0) == '$') {
                        break;
                    } else if (line.startsWith("dir")) {
                        String dirName = line.substring(4);
                        pointer.addDir(new Dir(dirName, pointer));
                    } else {
                        Matcher matcher = FILE_PATTERN.matcher(line);
                        matcher.matches();
                        File file = new File(Long.parseLong(matcher.group(1)), matcher.group(2));
                        pointer.addFile(file);
                    }
                }
            } else if (line.startsWith("$ cd")) {
                String dirName = line.substring(5);
                if (dirName.equals("..")) {
                    pointer = pointer.getParent();
                } else {
                    pointer = pointer.findDir(dirName);
                }
                if (it.hasNext()) {
                    line = it.next();
                }
            } else {
                throw new IllegalArgumentException();
            }
        }

        return root;
    }

    @Value
    private static class Dir {
        String name;
        List<Dir> dirs = new ArrayList<>();
        List<File> files = new ArrayList<>();
        @ToString.Exclude
        Dir parent;
        @Getter(lazy = true)
        long size = calculateSize();

        private Dir(String name, Dir parent) {
            this.name = name;
            this.parent = parent;
        }

        public void addFile(File file) {
            files.add(file);
        }

        public void addDir(Dir dir) {
            dirs.add(dir);
        }

        public Dir findDir(String name) {
            return dirs.stream()
                    .filter(dir -> dir.name.equals(name))
                    .findFirst()
                    .orElseThrow();
        }

        private long calculateSize() {
            long sizeFiles = files.stream()
                    .mapToLong(File::size)
                    .sum();
            long sizeDirs = dirs.stream()
                    .mapToLong(Dir::calculateSize)
                    .sum();
            return sizeFiles + sizeDirs;
        }
    }

    private static Stream<Dir> getAllDirsRecursively(Dir dir) {
        return Stream.concat(Stream.of(dir),
                dir.dirs.stream().flatMap(Day7::getAllDirsRecursively));
    }

    private record File(long size, String name) {
    }

    public static Object part2(String resource) {
        Dir root = parseSystem(resource);
        long sizeNeeded = 30_000_000 + root.calculateSize() - 70_000_000;
        return getAllDirsRecursively(root)
                .mapToLong(Dir::getSize)
                .filter(size -> size >= sizeNeeded)
                .min()
                .orElseThrow();
    }
}
