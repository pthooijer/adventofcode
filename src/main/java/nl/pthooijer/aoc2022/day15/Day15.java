package nl.pthooijer.aoc2022.day15;

import lombok.Value;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class Day15 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/15/sample-input1.txt";
        String input = "aoc/2022/day/15/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 26): " + part1(sampleInput1, 10));
        System.out.println("Part 1 answer: " + part1(input, 2_000_000));

        System.out.println("Sample1 Part 2 answer (must be 56000011): " + part2(sampleInput1, 20));
        System.out.println("Part 2 answer: " + part2(input, 4000000));
    }

    public static Object part1(String resource, int row) {
        List<Data> input = parseInput(resource);

        List<RangeIncl> rangesAtRow = new ArrayList<>();
        for (Data data : input) {
            int distance = data.distance;
            int widthAtRow = distance - Math.abs(data.sensor.y - row);
            if (widthAtRow >= 0) {
                int from = data.sensor.x - widthAtRow;
                int to = data.sensor.x + widthAtRow;
                RangeIncl range = new RangeIncl(from, to);
                rangesAtRow.add(range);
            }
        }

        int minRange = rangesAtRow.stream().mapToInt(RangeIncl::from).min().orElseThrow();
        int maxRange = rangesAtRow.stream().mapToInt(RangeIncl::to).max().orElseThrow();

        return IntStream.rangeClosed(minRange, maxRange)
                .filter(x -> {
                    Point p = new Point(x, row);
                    return input.stream().noneMatch(data -> data.beacon.equals(p));
                })
                .filter(x -> rangesAtRow.stream().anyMatch(range -> range.contains(x)))
                .count();
    }

    public static List<Data> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day15::parseLine)
                .toList();
    }

    private static final Pattern DATA_PATTERN = Pattern.compile(
            "Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)");

    public static Data parseLine(String line) {
        Matcher matcher = DATA_PATTERN.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(line);
        }
        Point sensor = new Point(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
        Point beacon = new Point(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)));
        return new Data(sensor, beacon);
    }

    @Value
    public static class Data {
        Point sensor;
        Point beacon;
        int distance;

        public Data(Point sensor, Point beacon) {
            this.sensor = sensor;
            this.beacon = beacon;
            this.distance = sensor.minus(beacon).taxiMagnitude();
        }
    }

    public record RangeIncl(int from, int to) {
        public boolean contains(int x) {
            return x >= from && x <= to;
        }
    }

    public static Object part2(String resource, int maxRange) {
        List<Data> input = parseInput(resource);

        Point result = new Point(0, 0); // dummy
        searchLoop:
        for (Data data : input) {
            List<Point> boundary = getBoundaryPoints(data.sensor, data.distance + 1).stream()
                    .filter(p -> p.x >= 0 && p.x <= maxRange && p.y >= 0 && p.y <= maxRange)
                    .toList();
            for (Point p : boundary) {
                boolean found = input.stream()
                        .filter(data2 -> !data.equals(data2))
                        .allMatch(data2 -> data2.sensor.minus(p).taxiMagnitude() > data2.distance);
                if (found) {
                    result = p;
                    break searchLoop;
                }
            }

        }

        return ((long) result.x) * 4000000L + result.y;
    }

    public static Set<Point> getBoundaryPoints(Point p, int distance) {
        Set<Point> result = new HashSet<>(distance * 4);
        // top right
        for (int i = 0; i <= distance; i++) {
            int x = p.x + i;
            int y = p.y - distance + i;
            result.add(new Point(x, y));
        }
        // bottom right
        for (int i = 0; i <= distance; i++) {
            int x = p.x + distance - i;
            int y = p.y + i;
            result.add(new Point(x, y));
        }
        // bottom left
        for (int i = 0; i <= distance; i++) {
            int x = p.x - i;
            int y = p.y + distance - i;
            result.add(new Point(x, y));
        }
        // top left
        for (int i = 0; i <= distance; i++) {
            int x = p.x - distance + i;
            int y = p.y - i;
            result.add(new Point(x, y));
        }
        return result;
    }
}
