package nl.pthooijer.aoc2022;

public class Day {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/1/sample-input1.txt";
        String input = "aoc/2022/day/1/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 0): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {

        return null;
    }

    public static Object part2(String resource) {

        return null;
    }
}
