package nl.pthooijer.aoc2022.day22;

import com.google.common.base.Strings;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.Arrays;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class Day22 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/22/sample-input1.txt";
        String sampleInput2 = "aoc/2022/day/22/sample-input2.txt";
        String input = "aoc/2022/day/22/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 6032): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be ???): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final Point[] FACING = new Point[]{new Point(0, 1), new Point(1, 0), new Point(0, -1), new Point(-1, 0)};

    public static Object part1(String resource) {
        char[][] grid = parseGrid(resource);
        List<String> instructions = parseInstructions(resource);

        Point pos = new Point(0, 0);
        for (int y = 0; y < grid[0].length; y++) {
            if (grid[0][y] == '.') {
                pos = new Point(0, y);
                break;
            }
        }
        int dir = 0; // right
        Point gridSize = new Point(grid.length, grid[0].length);

        for (String instruction : instructions) {
            if (Character.isDigit(instruction.charAt(0))) {
                long move = Long.parseLong(instruction);
                for (int i = 0; i < move; i++) {
                    Point newPos = pos;
                    newPos = newPos.plus(FACING[dir]);
                    if (!ArrayUtils.isInBounds(grid, newPos.x, newPos.y)) {
                        newPos = newPos.minus(FACING[dir].times(gridSize)); // ex: 10x20 -> (4,-1)-(0,-1)*(10,20)=(4,19)
                    }
                    char c = grid[newPos.x][newPos.y];
                    while (c != '.' && c != '#') {
                        newPos = newPos.plus(FACING[dir]);
                        if (!ArrayUtils.isInBounds(grid, newPos.x, newPos.y)) {
                            newPos = newPos.minus(FACING[dir].times(gridSize));
                        }
                        c = grid[newPos.x][newPos.y];
                    }

                    if (grid[newPos.x][newPos.y] == '#') {
                        break;
                    } else {
                        pos = newPos;
                    }

                }
            } else {
                dir += instruction.equals("R") ? 1 : -1;
                dir = Math.floorMod(dir, 4);
            }
        }

        return 1000 * (pos.x + 1) + 4 * (pos.y + 1) + dir;
    }

    private static char[][] parseGrid(String resource) {
        List<String> lines = ResourceHelper.getLinesStream(resource)
                .takeWhile(line -> !line.isBlank())
                .toList();

        // make the grid rectangular
        int longestLine = lines.stream()
                .mapToInt(String::length)
                .max()
                .orElseThrow();

        return lines.stream()
                .map(line -> Strings.padEnd(line, longestLine, ' '))
                .map(String::toCharArray)
                .toArray(char[][]::new);
    }

    private static final Pattern PATTERN = Pattern.compile("(\\d+|\\D+)");

    private static List<String> parseInstructions(String resource) {
        String line = ResourceHelper.getLinesStream(resource)
                .dropWhile(line2 -> !line2.isBlank())
                .skip(1)
                .iterator().next();

        return PATTERN.matcher(line)
                .results()
                .map(MatchResult::group)
                .toList();
    }

    public static Object part2(String resource) {
        char[][][] grids = parseGrids(resource);
        List<String> instructions = parseInstructions(resource);
        int sizeCube = grids[0].length;

        Point pos = new Point(0, 0);
        int dir = 0;
        int side = 0;
        for (String instruction : instructions) {
            if (Character.isDigit(instruction.charAt(0))) {
                long move = Long.parseLong(instruction);
                for (int i = 0; i < move; i++) {
                    Point newPos = pos;
                    int newSide = side;
                    int newDir = dir;

                    newPos = pos.plus(FACING[dir]);
                    if (!ArrayUtils.isInBounds(grids[side], newPos.x, newPos.y)) {
                        State state = new State(side, newPos, dir);
                        State newState = wrapCube(state, sizeCube);
                        newPos = newState.pos;
                        newSide = newState.side;
                        newDir = newState.dir;
                    }

                    if (grids[newSide][newPos.x][newPos.y] == '#') {
                        break;
                    } else {
                        pos = newPos;
                        side = newSide;
                        dir = newDir;
                    }
                }
            } else {
                dir += instruction.equals("R") ? 1 : -1;
                dir = Math.floorMod(dir, 4);
            }
        }

        return 1000 * (pos.x + SIDE_COORDS[side].x * sizeCube + 1) +
                4 * (pos.y + SIDE_COORDS[side].y * sizeCube + 1) +
                dir;
    }

    private static final Point[] SIDE_COORDS = new Point[]{new Point(0, 1), new Point(1, 1), new Point(2, 1),
            new Point(2, 0), new Point(3, 0), new Point(0, 2)};

    /*
     05
     1
    32
    4
    */
    public static char[][][] parseGrids(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);

        int sizeCube = grid.length / 4;

        char[][][] sides = new char[6][sizeCube][sizeCube];
        for (int s = 0; s < 6; s++) {

            Point cubeCoord = SIDE_COORDS[s];
            int gridY = cubeCoord.y * sizeCube;
            for (int x = 0; x < sizeCube; x++) {
                int gridX = cubeCoord.x * sizeCube + x;
                sides[s][x] = Arrays.copyOfRange(grid[gridX], gridY, gridY + sizeCube);
            }
        }

        return sides;
    }

    private static State wrapCube(State state, int sizeCube) {
        return switch (state.side) {
            case 0 -> switch (state.dir) {
                case 0 -> new State(5, state.x(), 0, 0);
                case 1 -> new State(1, 0, state.y(), 1);
                case 2 -> new State(3, sizeCube - state.x() - 1, 0, 0);
                case 3 -> new State(4, state.y(), 0, 0);
                default -> throw new IllegalStateException(state.toString());
            };
            case 1 -> switch (state.dir) {
                case 0 -> new State(5, sizeCube - 1, state.x(), 3);
                case 1 -> new State(2, 0, state.y(), 1);
                case 2 -> new State(3, 0, state.x(), 1);
                case 3 -> new State(0, sizeCube - 1, state.y(), 3);
                default -> throw new IllegalStateException(state.toString());
            };
            case 2 -> switch (state.dir) {
                case 0 -> new State(5, sizeCube - state.x() - 1, sizeCube - 1, 2);
                case 1 -> new State(4, state.y(), sizeCube - 1, 2);
                case 2 -> new State(3, state.x(), sizeCube - 1, 2);
                case 3 -> new State(1, sizeCube - 1, state.y(), 3);
                default -> throw new IllegalStateException(state.toString());
            };
            case 3 -> switch (state.dir) {
                case 0 -> new State(2, state.x(), 0, 0);
                case 1 -> new State(4, 0, state.y(), 1);
                case 2 -> new State(0, sizeCube - state.x() - 1, 0, 0);
                case 3 -> new State(1, state.y(), 0, 0);
                default -> throw new IllegalStateException(state.toString());
            };
            case 4 -> switch (state.dir) {
                case 0 -> new State(2, sizeCube - 1, state.x(), 3);
                case 1 -> new State(5, 0, state.y(), 1);
                case 2 -> new State(0, 0, state.x(), 1);
                case 3 -> new State(3, sizeCube - 1, state.y(), 3);
                default -> throw new IllegalStateException(state.toString());
            };
            case 5 -> switch (state.dir) {
                case 0 -> new State(2, sizeCube - state.x() - 1, sizeCube - 1, 2);
                case 1 -> new State(1, state.y(), sizeCube - 1, 2);
                case 2 -> new State(0, state.x(), sizeCube - 1, 2);
                case 3 -> new State(4, sizeCube - 1, state.y(), 3);
                default -> throw new IllegalStateException(state.toString());
            };
            default -> throw new IllegalStateException(state.toString());
        };
    }

    public record State(int side, Point pos, int dir) {

        public State(int side, int x, int y, int direction) {
            this(side, new Point(x, y), direction);
        }

        public int x() {
            return pos.x;
        }

        public int y() {
            return pos.y;
        }

    }
}
