package nl.pthooijer.aoc2022.day14;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class Day14 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/14/sample-input1.txt";
        String input = "aoc/2022/day/14/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 24): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<List<Point>> input = parseInput(resource);
        Point spawn = new Point(500, 0);

        Grid grid = createGridPt1(input);

        grid.set(spawn, '+');
        drawRocks(grid, input);

        simulateSand(grid, spawn);

        return ArrayUtils.countChars(grid.grid, 'o');
    }

    public static Object part2(String resource) {
        List<List<Point>> input = parseInput(resource);
        Point spawn = new Point(500, 0);

        Grid grid = createGridPt2(input, spawn);

        grid.set(spawn, '+');
        drawRocks(grid, input);
        // draw floor
        int ry = grid.grid[0].length - 1;
        for(int rx = 0; rx < grid.grid.length; rx++) {
            grid.grid[rx][ry] = '#';
        }

        simulateSand(grid, spawn);

        return ArrayUtils.countChars(grid.grid, 'o');
    }

    private static final Point[] DIRECTIONS = new Point[]{new Point(0, 1), new Point(-1, 1), new Point(1, 1)};

    private static void simulateSand(Grid grid, Point spawn) {
        while (true) {
            Point sand = spawn;

            oneSandLoop:
            while (true) {
                if(grid.get(spawn) == 'o') {
                    return;
                }
                for (Point direction : DIRECTIONS) {
                    Point newSand = sand.plus(direction);
                    if (!grid.isInBounds(newSand)) {
                        return;
                    } else if (grid.get(newSand) == '.') {
                        sand = newSand;
                        break; // of the for loop
                    } else if (direction == DIRECTIONS[DIRECTIONS.length - 1]) { // if final direction did not fill
                        grid.set(sand, 'o');
                        break oneSandLoop;
                    }
                }
            }
        }
    }

    private static Grid createGridPt1(List<List<Point>> input) {
        IntSummaryStatistics xStats = input.stream()
                .flatMap(List::stream)
                .mapToInt(Point::getX)
                .summaryStatistics();
        IntSummaryStatistics yStats = input.stream()
                .flatMap(List::stream)
                .mapToInt(Point::getY)
                .summaryStatistics();
        int xMin = xStats.getMin();
        int xMax = xStats.getMax() + 1;
        int yMin = min(yStats.getMin(), 0);
        int yMax = yStats.getMax() + 1;
        return new Grid(xMin, xMax, yMin, yMax, '.');
    }

    private static Grid createGridPt2(List<List<Point>> input, Point spawn) {
        IntSummaryStatistics xStats = input.stream()
                .flatMap(List::stream)
                .mapToInt(Point::getX)
                .summaryStatistics();
        IntSummaryStatistics yStats = input.stream()
                .flatMap(List::stream)
                .mapToInt(Point::getY)
                .summaryStatistics();
        int yMin = min(yStats.getMin(), 0);
        int yMax = yStats.getMax() + 3; // +2 for floor
        int yHeight = yMax - yMin;
        // largest structure is a triangle, so be sure a triangle fits:
        int xMin = min(xStats.getMin(), spawn.x - yHeight);
        int xMax = max(xStats.getMax() + 1, spawn.x + yHeight);
        return new Grid(xMin, xMax, yMin, yMax, '.');
    }

    private static void drawRocks(Grid grid, List<List<Point>> input) {
        for (List<Point> line : input) {
            Iterator<Point> iterator = line.iterator();
            Point p1;
            Point p2 = iterator.next();
            while (iterator.hasNext()) {
                p1 = p2;
                p2 = iterator.next();
                if (p1.x == p2.x) {
                    for (int ry = min(p1.y, p2.y); ry <= max(p1.y, p2.y); ry++) {
                        grid.set(p1.x, ry, '#');
                    }
                } else {
                    for (int rx = min(p1.x, p2.x); rx <= max(p1.x, p2.x); rx++) {
                        grid.set(rx, p1.y, '#');
                    }
                }
            }
        }
    }

    private static final Splitter ARROW_SPLITTER = Splitter.on(" -> ");
    private static final Splitter COMMA_SPLITTER = Splitter.on(",");

    public static List<List<Point>> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(line -> ARROW_SPLITTER.splitToStream(line)
                        .map(point -> COMMA_SPLITTER.splitToStream(point)
                                .map(Integer::parseInt)
                                .collect(collectingAndThen(toList(), list -> new Point(list.get(0), list.get(1)))))
                        .toList())
                .toList();
    }

    private static class Grid {
        private final char[][] grid;
        private final int xMin;
        private final int yMin;

        public Grid(int xMin, int xMax, int yMin, int yMax, char defaultChar) {
            this.grid = new char[xMax - xMin][yMax - yMin];
            for (char[] row : grid) {
                Arrays.fill(row, defaultChar);
            }
            this.xMin = xMin;
            this.yMin = yMin;
        }

        public void set(int x, int y, char c) {
            int x1 = getX(x);
            int y1 = getY(y);
            grid[x1][y1] = c;
        }

        public void set(Point p, char c) {
            set(p.x, p.y, c);
        }

        public char get(int x, int y) {
            int x1 = getX(x);
            int y1 = getY(y);
            return grid[x1][y1];
        }

        public char get(Point p) {
            return get(p.x, p.y);
        }

        public boolean isInBounds(int x, int y) {
            int x1 = getX(x);
            int y1 = getY(y);
            return ArrayUtils.isInBounds(grid, x1, y1);
        }

        public boolean isInBounds(Point p) {
            return isInBounds(p.x, p.y);
        }

        private int getX(int x) {
            return x - xMin;
        }

        private int getY(int y) {
            return y - yMin;
        }
    }
}
