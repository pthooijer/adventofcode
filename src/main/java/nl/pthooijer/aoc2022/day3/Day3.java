package nl.pthooijer.aoc2022.day3;

import nl.pthooijer.util.ResourceHelper;

import java.util.Iterator;

public class Day3 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/3/sample-input1.txt";
        String input = "aoc/2022/day/3/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 157): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 70): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        long totalScore = 0;
        for (String line : ResourceHelper.getLinesIt(resource)) {
            int length = line.length();
            String half2 = line.substring(length / 2);
            for (int i = 0; i < length / 2; i++) {
                char dupe = line.charAt(i);
                if (half2.indexOf(dupe) != -1) {
                    totalScore += score(dupe);
                    break;
                }
            }
        }

        return totalScore;
    }

    public static long score(char c) {
        if (Character.isLowerCase(c)) {
            return c - 'a' + 1;
        } else {
            return c - 'A' + 27;
        }
    }

    public static Object part2(String resource) {
        long totalScore = 0;
        Iterator<String> linesIt = ResourceHelper.getLinesIt(resource).iterator();
        while (linesIt.hasNext()) {
            String sack1 = linesIt.next();
            String sack2 = linesIt.next();
            String sack3 = linesIt.next();

            for (char dupe : sack1.toCharArray()) {
                if (sack2.indexOf(dupe) != -1 && sack3.indexOf(dupe) != -1) {
                    totalScore += score(dupe);
                    break;
                }
            }
        }

        return totalScore;
    }
}
