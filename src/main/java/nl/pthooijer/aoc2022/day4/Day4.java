package nl.pthooijer.aoc2022.day4;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.List;

public class Day4 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/4/sample-input1.txt";
        String input = "aoc/2022/day/4/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 2): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 4): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day4::parseLine)
                .filter(pair -> (pair.left.low >= pair.right.low && pair.left.high <= pair.right.high) ||
                        (pair.right.low >= pair.left.low && pair.right.high <= pair.left.high))
                .count();
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf("-,"));

    public static Pair parseLine(String line) {
        List<Integer> nrs = SPLITTER.splitToStream(line)
                .map(Integer::parseInt)
                .toList();
        return new Pair(new Range(nrs.get(0), nrs.get(1)), new Range(nrs.get(2), nrs.get(3)));
    }

    public static Object part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day4::parseLine)
                .filter(pair -> (pair.left.low <= pair.right.high && pair.right.low <= pair.left.high))
                .count();
    }

    public record Range(int low, int high) {
    }

    public record Pair(Range left, Range right) {
    }
}
