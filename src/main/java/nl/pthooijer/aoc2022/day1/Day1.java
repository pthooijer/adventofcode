package nl.pthooijer.aoc2022.day1;

import com.google.common.collect.Streams;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.SeparatorPolicy;

import java.util.Comparator;
import java.util.stream.Collectors;

public class Day1 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2022/day/1/sample-input1.txt";
        String input = "aoc/2022/day/1/input.txt";

        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        JoinIterator<String, ?, Long> joinIterator =
                new JoinIterator<>(ResourceHelper.getLinesIt(resource).iterator(),
                        String::isEmpty,
                        SeparatorPolicy.DROP,
                        Collectors.summingLong(Long::parseLong));
        return Streams.stream(joinIterator)
                .mapToLong(l -> l)
                .max()
                .orElseThrow();
    }

    public static Object part2(String resource) {
        JoinIterator<String, ?, Long> joinIterator =
                new JoinIterator<>(ResourceHelper.getLinesIt(resource).iterator(),
                        String::isEmpty,
                        SeparatorPolicy.DROP,
                        Collectors.summingLong(Long::parseLong));
        return Streams.stream(joinIterator)
                .sorted(Comparator.<Long>comparingLong(l -> l).reversed())
                .limit(3)
                .mapToLong(l -> l)
                .sum();
    }

}
