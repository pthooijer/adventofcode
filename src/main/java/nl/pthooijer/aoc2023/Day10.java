package nl.pthooijer.aoc2023;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Direction;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Day10 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/10/sample-input1.txt";
        String sampleInput2 = "aoc/2023/day/10/sample-input2.txt";
        String input = "aoc/2023/day/10/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 8): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be 8): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private record Connections(Direction c1, Direction c2) {
        public boolean contains(Direction direction) {
            return direction == c1 || direction == c2;
        }

        public Direction getOther(Direction dir) {
            if (dir == c1) {
                return c2;
            } else if (dir == c2) {
                return c1;
            } else {
                return null;
            }
        }
    }

    private final static Map<Character, Connections> CONNECTIONS_MAP = Map.of(
            '|', new Connections(Direction.NORTH, Direction.SOUTH),
            '-', new Connections(Direction.WEST, Direction.EAST),
            'L', new Connections(Direction.NORTH, Direction.EAST),
            'J', new Connections(Direction.NORTH, Direction.WEST),
            '7', new Connections(Direction.SOUTH, Direction.WEST),
            'F', new Connections(Direction.SOUTH, Direction.EAST));


    private record PosState(Point pos, Direction dir, List<Point> prevPos) {
    }

    ;

    public static Object part1(String resource) {
        char[][] maze = ResourceHelper.getChars2dArray(resource);
        Point start = findStart(maze);

        List<PosState> loop = findLoop(maze, start);
        return loop.get(0).prevPos().size() - 1;
    }

    private static List<PosState> findLoop(char[][] maze, Point start) {
        // start with a step in each direction
        List<PosState> stateList = List.of(
                new PosState(start, Direction.NORTH, List.of(start)),
                new PosState(start, Direction.EAST, List.of(start)),
                new PosState(start, Direction.SOUTH, List.of(start)),
                new PosState(start, Direction.WEST, List.of(start)));

        while (true) {
            // move a step
            List<PosState> newStateList = new ArrayList<>();
            for (PosState posState : stateList) {
                Point newPos = posState.pos.plus(posState.dir.move);
                Direction newDir = posState.dir.getOpposite();
                // check if connected
                if (ArrayUtils.isInBounds(maze, newPos.x, newPos.y)) {
                    char c = maze[newPos.x][newPos.y];
                    Connections connections = CONNECTIONS_MAP.get(c);
                    if (connections != null) {
                        newDir = connections.getOther(newDir);
                        if (newDir != null) {
                            List<Point> newPrevPos = new ArrayList<>(posState.prevPos());
                            newPrevPos.add(newPos);
                            newStateList.add(new PosState(newPos, newDir, newPrevPos));
                        }
                    }
                }
            }

            // check if any points match
            Set<Point> uniquePoints = newStateList.stream()
                    .map(PosState::pos)
                    .collect(Collectors.toSet());
            if (newStateList.size() != uniquePoints.size()) {
                return newStateList;
            }

            stateList = newStateList;
        }
    }

    private static Point findStart(char[][] maze) {
        for (int x = 0; x < maze.length; x++) {
            for (int y = 0; y < maze[x].length; y++) {
                if (maze[x][y] == 'S') {
                    return new Point(x, y);
                }
            }
        }
        throw new IllegalArgumentException("No S found");
    }

    public static Object part2(String resource) {
        char[][] maze = ResourceHelper.getChars2dArray(resource);
        Point start = findStart(maze);

        List<PosState> posStates = findLoop(maze, start);
        Set<Point> loop = new HashSet<>(posStates.get(0).prevPos());
        loop.addAll(posStates.get(1).prevPos()); // Set automatically removes duplicate start and end

        // replace start with a real pipe
        Point pos1 = posStates.get(0).prevPos().get(1);
        Point pos2 = posStates.get(1).prevPos().get(1);
        Direction dir1 = Direction.fromMove(pos1.minus(start));
        Direction dir2 = Direction.fromMove(pos2.minus(start));

        char c = CONNECTIONS_MAP.entrySet().stream()
                .filter(entry -> entry.getValue().contains(dir1) && entry.getValue().contains(dir2))
                .map(Map.Entry::getKey)
                .findFirst().orElseThrow();
        maze[start.x][start.y] = c;

        // ray tracing algorithm:
        // Whenever you cross a pipe, you switch from interior to exterior
        long interiorCount = 0L;
        // horizontal rays
        for (int x = 0; x < maze.length; x++) {
            // count 'crossing a pipe' both from north and south, this takes care of segments parallel to your ray
            long crossesNorth = 0L;
            long crossesSouth = 0L;
            for (int y = 0; y < maze[x].length; y++) {
                Point p = new Point(x, y);
                if (loop.contains(p)) {
                    char pipe = maze[x][y];
                    Connections connections = CONNECTIONS_MAP.get(pipe); // never null
                    if (connections.contains(Direction.NORTH)) {
                        crossesNorth++;
                    }
                    if (connections.contains(Direction.SOUTH)) {
                        crossesSouth++;
                    }
                } else {
                    if (crossesNorth % 2 == 1 && crossesSouth % 2 == 1) {
                        interiorCount++;
                    }
                }
            }

        }

        return interiorCount;
    }
}
