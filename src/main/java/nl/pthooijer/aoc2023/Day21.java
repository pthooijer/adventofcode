package nl.pthooijer.aoc2023;

import com.google.common.math.IntMath;
import com.google.common.math.LongMath;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Direction;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.IntStream;

public class Day21 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/21/sample-input1.txt";
        String input = "aoc/2023/day/21/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 16): " + part1(sampleInput1, 6));
        System.out.println("Part 1 answer: " + part1(input, 64));

        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource, int steps) {
        char[][] orgMap = ResourceHelper.getChars2dArray(resource);
        int[][] map = Arrays.stream(orgMap)
                .map(row -> ArrayUtils.stream(row)
                        .mapToInt(c -> c == '#' ? -2 : -1)
                        .toArray())
                .toArray(int[][]::new);
        Point start = IntStream.range(0, orgMap.length)
                .mapToObj(i -> IntStream.range(0, orgMap[i].length)
                        .filter(j -> orgMap[i][j] == 'S')
                        .mapToObj(j -> new Point(i, j)))
                .flatMap(stream -> stream)
                .findFirst().orElseThrow();
        map[start.x][start.y] = 0;

        Deque<Point> lifo = new LinkedList<>();
        lifo.addLast(start);
        while (!lifo.isEmpty()) {
            Point p = lifo.pollFirst();
            int value = map[p.x][p.y];
            if (value >= steps) {
                break;
            }
            for (Direction direction : Direction.values()) {
                Point neighbour = p.plus(direction.move);
                int nValue = map[neighbour.x][neighbour.y];
                if (nValue == -1) {
                    map[neighbour.x][neighbour.y] = value + 1;
                    lifo.addLast(neighbour);
                }
            }
        }

        return Arrays.stream(map)
                .flatMapToInt(Arrays::stream)
                .filter(i -> i >= 0 && i % 2 == 0)
                .count();
    }

    public static Object part2(String resource) {
        char[][] orgMap = ResourceHelper.getChars2dArray(resource);

        long evenCount = fillMap(orgMap, new Point(0, 0), 0);
        long oddCount = fillMap(orgMap, new Point(0, 0), 1);

        long cornerCountW = fillMap(orgMap, new Point(65, 130), 235);
        long cornerCountE = fillMap(orgMap, new Point(65, 0), 235);
        long cornerCountN = fillMap(orgMap, new Point(130, 65), 235);
        long cornerCountS = fillMap(orgMap, new Point(0, 65), 235);

        long oddEdgeSE = fillMap(orgMap, new Point(0, 0), 301);
        long evenEdgeSE = fillMap(orgMap, new Point(0, 0), 170);
        long oddEdgeSW = fillMap(orgMap, new Point(0, 130), 301);
        long evenEdgeSW = fillMap(orgMap, new Point(0, 130), 170);
        long oddEdgeNW = fillMap(orgMap, new Point(130, 130), 301);
        long evenEdgeNW = fillMap(orgMap, new Point(130, 130), 170);
        long oddEdgeNE = fillMap(orgMap, new Point(130, 0), 301);
        long evenEdgeNE = fillMap(orgMap, new Point(130, 0), 170);

        long n = LongMath.divide(26501365, 131, RoundingMode.DOWN); // nr of tiles from start to corner, excl. start
        long nEvenTmp = LongMath.divide(n-1, 2, RoundingMode.DOWN);
        long nrEven = 1 + 8 * nEvenTmp * (nEvenTmp + 1) / 2;
        long nOddTmp = LongMath.divide(n, 2, RoundingMode.DOWN);
        long nrOdd = 4 * nOddTmp * nOddTmp;

        long nrOddEdge = n;
        long nrEvenEdge = n - 1;

        return nrEven * evenCount +
                nrOdd * oddCount +
                nrOddEdge * (oddEdgeNE + oddEdgeNW + oddEdgeSE + oddEdgeSW) +
                nrEvenEdge * (evenEdgeNE + evenEdgeNW + evenEdgeSE + evenEdgeSW) +
                cornerCountE + cornerCountW + cornerCountN + cornerCountS;
    }

    private static long fillMap(char[][] orgMap, Point start, int startValue) {
        int[][] map = Arrays.stream(orgMap)
                .map(row -> ArrayUtils.stream(row)
                        .mapToInt(c -> c == '#' ? -2 : -1)
                        .toArray())
                .toArray(int[][]::new);
        map[start.x][start.y] = startValue;

        Deque<Point> lifo = new LinkedList<>();
        lifo.addLast(start);
        while (!lifo.isEmpty()) {
            Point p = lifo.pollFirst();

            int value = map[p.x][p.y];
            for (Direction direction : Direction.values()) {
                Point neighbour = p.plus(direction.move);
                if (ArrayUtils.isInBounds(map, neighbour.x, neighbour.y)) {
                    int nValue = map[neighbour.x][neighbour.y];
                    if (nValue == -1) {
                        map[neighbour.x][neighbour.y] = value + 1;
                        lifo.addLast(neighbour);
                    }
                }

            }
        }

        return Arrays.stream(map)
                .flatMapToInt(Arrays::stream)
                .filter(i -> i >= 0 && i <= 365 && i % 2 == 1)
                .count();
    }
}
