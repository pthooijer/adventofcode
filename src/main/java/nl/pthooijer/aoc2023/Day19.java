package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.math.LongMath;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day19 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/19/sample-input1.txt";
        String input = "aoc/2023/day/19/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 19114): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 167409079868000): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Map<String, Workflow> workflowMap = ResourceHelper.getLinesStream(resource)
                .takeWhile(line -> !line.isBlank())
                .map(Day19::parseWorkflow)
                .collect(Collectors.toMap(Workflow::name, workflow -> workflow));

        return ResourceHelper.getLinesStream(resource)
                .dropWhile(line -> !line.isBlank())
                .skip(1)
                .map(Day19::parsePart)
                .filter(part -> resolvePartAccepted(part, workflowMap))
                .mapToLong(part -> part.x + part.m + part.a + part.s)
                .sum();
    }

    private static boolean resolvePartAccepted(final Part part, final Map<String, Workflow> workflowMap) {
        Workflow workflow = workflowMap.get("in");
        while (true) {
            String target = workflow.instructions.stream()
                    .filter(instruction -> instruction.predicate.test(part))
                    .findFirst()
                    .map(Instruction::target)
                    .orElse(workflow.finalTarget);

            if (target.equals("A")) {
                return true;
            } else if (target.equals("R")) {
                return false;
            } else {
                workflow = workflowMap.get(target);
            }
        }
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf("{},")).omitEmptyStrings();

    private static Workflow parseWorkflow(String line) {
        List<String> parts1 = SPLITTER.splitToList(line);
        String name = parts1.get(0);
        String finalTarget = parts1.get(parts1.size() - 1);

        List<Instruction> instructions = parts1.subList(1, parts1.size() - 1).stream()
                .map(Day19::parseInstruction)
                .toList();

        return new Workflow(name, instructions, finalTarget);
    }

    private static final Pattern INSTR_PATTERN = Pattern.compile("^([xmas]+)([<>])(\\d+):([A-Za-z]+)$");

    private static Instruction parseInstruction(String string) {
        Matcher matcher = INSTR_PATTERN.matcher(string);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(string);
        }
        int typeIndex = switch (matcher.group(1).charAt(0)) {
            case 'x' -> 0;
            case 'm' -> 1;
            case 'a' -> 2;
            case 's' -> 3;
            default -> throw new IllegalArgumentException(string);
        };

        boolean lessThan = matcher.group(2).equals("<");
        int value = Integer.parseInt(matcher.group(3));
        String target = matcher.group(4);

        Predicate<Part> predicate = switch (typeIndex) {
            case 0 -> lessThan ? part -> part.x < value : part -> part.x > value;
            case 1 -> lessThan ? part -> part.m < value : part -> part.m > value;
            case 2 -> lessThan ? part -> part.a < value : part -> part.a > value;
            case 3 -> lessThan ? part -> part.s < value : part -> part.s > value;
            default -> throw new IllegalArgumentException(string);
        };

        return new Instruction(typeIndex, lessThan, value, target, predicate);
    }

    private static final Splitter.MapSplitter PART_SPLITTER = Splitter.on(CharMatcher.anyOf("{},")).omitEmptyStrings()
            .withKeyValueSeparator('=');

    private static Part parsePart(String line) {
        Map<String, String> map = PART_SPLITTER.split(line);
        int x = Integer.parseInt(map.get("x"));
        int m = Integer.parseInt(map.get("m"));
        int a = Integer.parseInt(map.get("a"));
        int s = Integer.parseInt(map.get("s"));
        return new Part(x, m, a, s);
    }

    private record Workflow(String name, List<Instruction> instructions, String finalTarget) {
    }

    private record Instruction(int typeIndex, boolean lessThan, int value, String target, Predicate<Part> predicate) {
    }

    private record Part(int x, int m, int a, int s) {
    }

    public static Object part2(String resource) {
        Map<String, Workflow> workflowMap = ResourceHelper.getLinesStream(resource)
                .takeWhile(line -> !line.isBlank())
                .map(Day19::parseWorkflow)
                .collect(Collectors.toMap(Workflow::name, workflow -> workflow));

        Range[] start = new Range[]{new Range(1, 4000), new Range(1, 4000), new Range(1, 4000), new Range(1, 4000)};
        Deque<QueueRecord> queue = new LinkedList<>();
        queue.add(new QueueRecord("in", start));
        long acceptedRanges = 0L;
        whileLoop:
        while (!queue.isEmpty()) {
            QueueRecord record = queue.pop();
            if (record.workflow.equals("A")) {
                acceptedRanges += Arrays.stream(record.ranges)
                        .mapToLong(Range::size)
                        .reduce(1L, LongMath::checkedMultiply);
                continue;
            } else if (record.workflow.equals("R")) {
                continue;
            }

            Workflow workflow = workflowMap.get(record.workflow);

            Range[] ranges = record.ranges; // modifiable
            for (Instruction instr : workflow.instructions) {
                Range outRange = instr.lessThan ?
                        new Range(ranges[instr.typeIndex].fromIncl, instr.value - 1) :
                        new Range(instr.value + 1, ranges[instr.typeIndex].toIncl);

                if (outRange.isValid()) {
                    Range[] outRanges = Arrays.copyOf(record.ranges, record.ranges.length);
                    outRanges[instr.typeIndex] = outRange;
                    queue.add(new QueueRecord(instr.target, outRanges));
                }

                Range inRange = instr.lessThan ?
                        new Range(instr.value, ranges[instr.typeIndex].toIncl) :
                        new Range(ranges[instr.typeIndex].fromIncl, instr.value);
                if (inRange.isValid()) {
                    ranges[instr.typeIndex] = inRange;
                } else {
                    continue whileLoop;
                }
            }

            queue.add(new QueueRecord(workflow.finalTarget, ranges));
        }


        return acceptedRanges;
    }

    private record Range(int fromIncl, int toIncl) {

        public boolean isValid() {
            return fromIncl < toIncl;
        }

        public long size() {
            return toIncl - fromIncl + 1;
        }
    }

    private record QueueRecord(String workflow, Range[] ranges) {
    }
}
