package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.math.LongMath;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day4 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/4/sample-input1.txt";
        String input = "aoc/2023/day/4/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 13): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 30): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day4::parseLine)
                .mapToInt(Day4::matches)
                .mapToLong(matches -> matches == 0 ? 0L : LongMath.pow(2L, matches - 1) )
                .sum();
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(":|"));

    private static Card parseLine(String line) {
        List<String> parts = SPLITTER.splitToList(line);
        List<Long> winningNrs = parseNumbers(parts.get(1));
        List<Long> myNrs = parseNumbers(parts.get(2));

        return new Card(winningNrs, myNrs);
    }

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ').omitEmptyStrings();

    private static List<Long> parseNumbers(String s) {
        return SPACE_SPLITTER.splitToStream(s)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    private static int matches(Card card) {
        List<Long> matches = new ArrayList<>(card.winningNrs());
        matches.retainAll(card.myNrs());
        return matches.size();
    }

    public static Object part2(String resource) {
        List<Card> cards = ResourceHelper.getLinesStream(resource)
                .map(Day4::parseLine)
                .toList();

        Map<Card, Long> totalCards = new HashMap<>();
        cards.forEach(card -> totalCards.put(card, 1L));

        for(int i=0; i<cards.size(); i++) {
            Card card = cards.get(i);
            long amount = totalCards.get(card);
            long matches = matches(card);
            for(int j=1; j <= matches; j++) {
                totalCards.merge(cards.get(i + j), amount, Long::sum);
            }
        }

        return totalCards.values().stream()
                .mapToLong(l -> l)
                .sum();
    }

    private record Card(List<Long> winningNrs, List<Long> myNrs) {
    }
}
