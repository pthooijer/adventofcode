package nl.pthooijer.aoc2023;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Day7 {
    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/7/sample-input1.txt";
        String sampleInput2 = "aoc/2023/day/7/sample-input2.txt";
        String input = "aoc/2023/day/7/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 6440): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 5905): " + part2(sampleInput1));
        System.out.println("Sample2 Part 2 answer (must be 5905): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    private static final String STRENGTH = "AKQJT98765432";
    private static final int SCORE_INDEX = 100;
    private static final Map<Character, Integer> STRENGTH_NR = IntStream.range(0, STRENGTH.length())
            .boxed()
            .collect(Collectors.toMap(STRENGTH::charAt, i -> SCORE_INDEX - i - 1));

    public static Object part1(String resource) {
        List<Hand> sortedHands = parseInput(resource).stream()
                .sorted(Comparator.comparingLong(hand -> score(hand.cards()))) // low to high
                .toList();

        return LongStream.range(0, sortedHands.size())
                .map(i -> sortedHands.get((int) i).bid() * (i + 1))
                .sum();
    }

    private static List<Hand> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day7::parseLine)
                .toList();
    }

    private static final Splitter SPLITTER = Splitter.on(' ');

    private static Hand parseLine(String line) {
        List<String> parts = SPLITTER.splitToList(line);
        return new Hand(parts.get(0), Long.parseLong(parts.get(1)));
    }

    private record Hand(String cards, long bid) {
    }

    private enum Type {
        FIVE_OF_A_KIND,
        FOUR_OF_A_KIND,
        FULL_HOUSE,
        THREE_OF_A_KIND,
        TWO_PAIR,
        ONE_PAIR,
        HIGH_CARD;
    }

    private static Type findType(String cardString) {
        Collection<Long> frequencies = cardString.codePoints()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .values();

        if (frequencies.contains(5L)) {
            return Type.FIVE_OF_A_KIND;
        } else if (frequencies.contains(4L)) {
            return Type.FOUR_OF_A_KIND;
        } else if (frequencies.contains(3L)) {
            return frequencies.contains(2L) ? Type.FULL_HOUSE : Type.THREE_OF_A_KIND;
        } else if (frequencies.contains(2L)) {
            return frequencies.stream().filter(f -> f == 2).count() == 2 ? Type.TWO_PAIR : Type.ONE_PAIR;
        } else {
            return Type.HIGH_CARD;
        }
    }

    private static long score(String hand) {
        long score = SCORE_INDEX - findType(hand).ordinal() - 1;
        for (char c : hand.toCharArray()) {
            score *= SCORE_INDEX;
            score += STRENGTH_NR.get(c);
        }

        return score;
    }

    // === Part 2

    private static final String STRENGTH2 = "AKQT98765432J";
    private static final Map<Character, Integer> STRENGTH_NR2 = IntStream.range(0, STRENGTH2.length())
            .boxed()
            .collect(Collectors.toMap(STRENGTH2::charAt, i -> SCORE_INDEX - i - 1));

    public static Object part2(String resource) {
        List<Hand> sortedHands = parseInput(resource).stream()
                .sorted(Comparator.comparingLong(hand -> score2(hand.cards()))) // low to high
                .toList();

        return LongStream.range(0, sortedHands.size())
                .map(i -> sortedHands.get((int) i).bid() * (i + 1))
                .sum();
    }

    private static long score2(String hand) {
        long score = SCORE_INDEX - findType2(hand).ordinal() - 1;
        for (char c : hand.toCharArray()) {
            score *= SCORE_INDEX;
            score += STRENGTH_NR2.get(c);
        }

        return score;
    }

    private static Type findType2(String cardString) {
        Collection<Long> freqsTmp = cardString.codePoints()
                .filter(c -> c != 'J') // remove jokers
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .values();
        List<Long> frequencies = new ArrayList<>(freqsTmp);
        frequencies.add(0L);

        long jFreq = cardString.codePoints()
                .filter(c -> c == 'J')
                .count();

        if (frequencies.contains(5L - jFreq)) {
            return Type.FIVE_OF_A_KIND;
        } else if (frequencies.contains(4L - jFreq)) {
            return Type.FOUR_OF_A_KIND;
        } else if (frequencies.contains(3L - jFreq)) {
            frequencies.remove(3L - jFreq);
            return frequencies.contains(2L) ? Type.FULL_HOUSE : Type.THREE_OF_A_KIND;
        } else if (frequencies.contains(2L - jFreq)) {
            frequencies.remove(2L - jFreq);
            return frequencies.contains(2L) ? Type.TWO_PAIR : Type.ONE_PAIR;
        } else {
            return Type.HIGH_CARD;
        }
    }
}
