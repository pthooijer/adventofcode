package nl.pthooijer.aoc2023;

import com.google.common.collect.Streams;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.SeparatorPolicy;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day13 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/13/sample-input1.txt";
        String input = "aoc/2023/day/13/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 405): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 400): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<char[][]> inputList = parseInput(resource);

        return inputList.stream()
                .mapToInt(map -> findMatch(map)
                        .map(i -> i * 100)
                        .orElseGet(() -> {
                            char[][] tMap = ArrayUtils.transpose(map);
                            return findMatch(tMap).orElseThrow();
                        }))
                .sum();
    }

    private static List<char[][]> parseInput(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator = new JoinIterator<>(ResourceHelper.getLinesIt(resource).iterator(),
                String::isBlank, SeparatorPolicy.DROP, Collectors.toList());

        return Streams.stream(joinIterator)
                .map(list -> list.stream()
                        .map(String::toCharArray)
                        .toArray(char[][]::new))
                .toList();
    }

    private static Optional<Integer> findMatch(char[][] map) {
        for (int i = 1; i < map.length; i++) {
            boolean matches = true;
            for (int j = 0; j < map.length; j++) {
                int p1 = i + j;
                int p2 = i - 1 - j;
                if (p1 >= map.length) {
                    break;
                }
                if (p2 < 0) {
                    break;
                }
                if (!Arrays.equals(map[p1], map[p2])) {
                    matches = false;
                    break;
                }
            }
            if (matches) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    private static int findMismatches(char[][] map, int i) {
        int mismatches = 0;
        for (int j = 0; j < map.length; j++) {
            int p1 = i + j;
            int p2 = i - 1 - j;
            if (p1 >= map.length) {
                break;
            }
            if (p2 < 0) {
                break;
            }
            for (int k = 0; k < map[j].length; k++) {
                if (map[p1][k] != map[p2][k]) {
                    mismatches++;
                }
            }
        }
        return mismatches;
    }


    public static Object part2(String resource) {
        List<char[][]> inputList = parseInput(resource);

        return inputList.stream()
                .mapToInt(map -> IntStream.range(1, map.length)
                        .filter(i -> findMismatches(map, i) == 1)
                        .map(i -> i * 100)
                        .findFirst()
                        .orElseGet(() -> {
                            char[][] tMap = ArrayUtils.transpose(map);
                            return IntStream.range(1, tMap.length)
                                    .filter(i -> findMismatches(tMap, i) == 1)
                                    .findFirst()
                                    .orElseThrow();
                        }))
                .sum();
    }
}
