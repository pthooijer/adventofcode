package nl.pthooijer.aoc2023;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.lang.Character.isDigit;
import static nl.pthooijer.util.ArrayUtils.isInBounds;

public class Day3 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/3/sample-input1.txt";
        String input = "aoc/2023/day/3/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 4361): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 467835): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);
        Map<Point, Long> serialPositionMap = new HashMap<>();

        for (int si = 0; si < grid.length; si++) {
            for (int sj = 0; sj < grid[0].length; sj++) {
                char sc = grid[si][sj];
                if (sc != '.' && !isDigit(sc)) {
                    serialPositionMap.putAll(findNeighbouringSerials(grid, si, sj));
                }
            }
        }

        return serialPositionMap.values().stream()
                .mapToLong(l -> l)
                .sum();
    }

    // Uses a Map to filter out duplicate serials, but not duplicate serial values:
    // Because the Key that is deduplicated is the position Point of the first digit.
    private static Map<Point, Long> findNeighbouringSerials(char[][] grid, int si, int sj) {
        Map<Point, Long> serialPositionMap = new HashMap<>();

        for (int ni = -1; ni <= 1; ni++) {
            for (int nj = -1; nj <= 1; nj++) {
                int pi = si + ni;
                int pj = sj + nj;
                if (isInBounds(grid, pi, pj) && isDigit(grid[pi][pj])) {

                    // find the start of the number
                    while (true) {
                        pj--;
                        if (!isInBounds(grid, pi, pj) || !isDigit(grid[pi][pj])) {
                            pj++;
                            break;
                        }
                    }

                    // parse the whole number
                    Point numberPos = new Point(pi, pj);
                    StringBuilder number = new StringBuilder();
                    do {
                        number.append(grid[pi][pj]);
                        pj++;
                    } while (isInBounds(grid, pi, pj) && isDigit(grid[pi][pj]));

                    serialPositionMap.put(numberPos, Long.parseLong(number.toString()));
                }
            }
        }

        return serialPositionMap;
    }

    public static Object part2(String resource) {
        char[][] grid = ResourceHelper.getChars2dArray(resource);
        long gearAnswer = 0L;

        for (int si = 0; si < grid.length; si++) {
            for (int sj = 0; sj < grid[0].length; sj++) {
                char sc = grid[si][sj];
                if (sc != '.' && !isDigit(sc)) {

                    // search all neighbours for digits
                    Map<Point, Long> serialPositionMap = findNeighbouringSerials(grid, si, sj);

                    // calculate gear number
                    if (serialPositionMap.size() >= 2) {
                        long gearNumber = serialPositionMap.values().stream()
                                .reduce(1L, Math::multiplyExact);
                        gearAnswer += gearNumber;
                    }

                }
            }
        }
        return gearAnswer;
    }
}
