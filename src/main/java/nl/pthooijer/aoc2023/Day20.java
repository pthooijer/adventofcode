package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.MathUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day20 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/20/sample-input1.txt";
        String sampleInput2 = "aoc/2023/day/20/sample-input2.txt";
        String input = "aoc/2023/day/20/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 32000000): " + part1(sampleInput1));
        System.out.println("Sample2 Part 1 answer (must be 11687500): " + part1(sampleInput2));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Map<String, Module> moduleMap = ResourceHelper.getLinesStream(resource)
                .map(Day20::parseLine)
                .collect(Collectors.toMap(Module::name, module -> module));
        moduleMap.put("output", new Module("output", false, Collections.emptyList()));

        Map<String, Boolean> flipFlopStates = new HashMap<>();
        Map<String, Map<String, Boolean>> conjuctionStates = moduleMap.values().stream()
                .filter(module -> !module.isFlipFlop)
                .filter(module -> !module.name.equals("roadcaster") && !module.name.equals("output"))
                .map(module -> Map.entry(module.name, moduleMap.values().stream()
                        .filter(module2 -> module2.targets.contains(module.name))
                        .collect(Collectors.<Module, String, Boolean>toMap(
                                Module::name, module2 -> false))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        long lowPulses = 0L;
        long highPulses = 0L;

        for (int i = 0; i < 1000; i++) {
            Deque<QueueRecord> deque = new LinkedList<>();
            lowPulses++; // the button press
            moduleMap.get("roadcaster").targets
                    .forEach(target -> deque.addLast(new QueueRecord("roadcaster", target, false)));
            lowPulses += moduleMap.get("roadcaster").targets.size();

            while (!deque.isEmpty()) {
                QueueRecord record = deque.pollFirst();
                Module module = moduleMap.get(record.target);

                if (module == null || module.name.equals("output")) {
                    continue;
                }

                if (module.isFlipFlop && !record.pulse) {
                    boolean newPulse = flipFlopStates.merge(module.name, true, (oldB, newB) -> !oldB);
                    for (String target : module.targets) {
                        deque.addLast(new QueueRecord(module.name, target, newPulse));
                    }
                    if (newPulse) {
                        highPulses += module.targets.size();
                    } else {
                        lowPulses += module.targets.size();
                    }
                } else if (!module.isFlipFlop) {
                    conjuctionStates.get(record.target).put(record.source, record.pulse);
                    boolean newPulse = !conjuctionStates.get(record.target).values().stream().allMatch(b -> b);
                    for (String target : module.targets) {
                        deque.addLast(new QueueRecord(module.name, target, newPulse));
                    }
                    if (newPulse) {
                        highPulses += module.targets.size();
                    } else {
                        lowPulses += module.targets.size();
                    }
                }
            }
        }

        return lowPulses * highPulses;
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(" ->,")).omitEmptyStrings();

    private static Module parseLine(String line) {
        List<String> parts = new ArrayList<>(SPLITTER.splitToList(line));
        boolean isFlipFlop = parts.get(0).charAt(0) == '%';
        String name = parts.get(0).substring(1);
        parts.remove(0);
        return new Module(name, isFlipFlop, parts);
    }

    private record Module(String name, boolean isFlipFlop, List<String> targets) {
    }

    private record QueueRecord(String source, String target, boolean pulse) {
    }


    public static Object part2(String resource) {
        /*
        Study the input: rx gets from &zh
        &zh gets from &sx, &jt, &kb and &ks
        Each of these only have 1 input: &pv -> sx, &vv -> jt, &mr -> kb and &bl -> ks

        So rx gets low if all to zh is high,
        All to zh sends high if their singular input is low
        All those send low if their inputs are all high

        So calculate the period of these and use LCM.
        If LCM is wrong, use Chinese Remainer Theorem.
        Post-edit: LCM was enough
         */

        Map<String, Module> moduleMap = ResourceHelper.getLinesStream(resource)
                .map(Day20::parseLine)
                .collect(Collectors.toMap(Module::name, module -> module));
        moduleMap.put("output", new Module("output", false, Collections.emptyList()));

        Map<String, Boolean> flipFlopStates = new HashMap<>();
        Map<String, Map<String, Boolean>> conjuctionStates = moduleMap.values().stream()
                .filter(module -> !module.isFlipFlop)
                .filter(module -> !module.name.equals("roadcaster") && !module.name.equals("output"))
                .map(module -> Map.entry(module.name, moduleMap.values().stream()
                        .filter(module2 -> module2.targets.contains(module.name))
                        .collect(Collectors.<Module, String, Boolean>toMap(
                                Module::name, module2 -> false))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Set<String> ofInterest = new HashSet<>(Set.of("sx", "jt", "kb", "ks"));
        Map<String, Long> periods = new HashMap<>();

        long counter = 0L;
        while (!ofInterest.isEmpty()) {
            counter++;

            Deque<QueueRecord> deque = new LinkedList<>();
            moduleMap.get("roadcaster").targets
                    .forEach(target -> deque.addLast(new QueueRecord("roadcaster", target, false)));

            while (!deque.isEmpty()) {
                QueueRecord record = deque.pollFirst();
                Module module = moduleMap.get(record.target);

                if (module == null || module.name.equals("output")) {
                    continue;
                }

                if (module.isFlipFlop && !record.pulse) {
                    boolean newPulse = flipFlopStates.merge(module.name, true, (oldB, newB) -> !oldB);
                    for (String target : module.targets) {
                        deque.addLast(new QueueRecord(module.name, target, newPulse));
                    }

                } else if (!module.isFlipFlop) {
                    conjuctionStates.get(record.target).put(record.source, record.pulse);
                    boolean newPulse = !conjuctionStates.get(record.target).values().stream().allMatch(b -> b);
                    for (String target : module.targets) {
                        deque.addLast(new QueueRecord(module.name, target, newPulse));
                    }
                    // new code here:
                    if (newPulse && ofInterest.contains(record.target)) {
                        ofInterest.remove(record.target);
                        periods.put(record.target, counter);
                    }

                }
            }
        }

        return periods.values().stream()
                .mapToLong(l -> l)
                .reduce(MathUtils::lcm).orElseThrow();
    }
}
