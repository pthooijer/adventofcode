package nl.pthooijer.aoc2023;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Day16 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/16/sample-input1.txt";
        String input = "aoc/2023/day/16/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 46): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 51): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);
        return energize(map, new PointDir(new Point(0, -1), Direction.EAST));
    }

    private record PointDir(Point point, Direction dir) {
    }

    @Getter
    @AllArgsConstructor
    private enum Direction {
        NORTH(new Point(-1, 0), 1),
        EAST(new Point(0, 1), 2),
        SOUTH(new Point(1, 0), 4),
        WEST(new Point(0, -1), 8);

        private final Point move;
        private final int code;
    }

    private static long energize(char[][] map, PointDir start) {
        int[][] stateMap = Arrays.stream(map)
                .map(row -> IntStream.range(0, row.length)
                        .map(j -> 0)
                        .toArray())
                .toArray(int[][]::new);

        Deque<PointDir> stack = new LinkedList<>();
        stack.add(start);
        while (!stack.isEmpty()) {
            PointDir pd = stack.pop();
            Point point = pd.point;
            Direction dir = pd.dir;

            point = point.plus(dir.move);
            while (ArrayUtils.isInBounds(map, point.x, point.y)) {
                int state = stateMap[point.x][point.y];
                if ((state & dir.code) == dir.code) {
                    break; // loop detected
                }
                stateMap[point.x][point.y] |= dir.code;

                char c = map[point.x][point.y];
                switch (c) {
                    case '/' -> dir = switch (dir) {
                        case NORTH -> Direction.EAST;
                        case EAST -> Direction.NORTH;
                        case SOUTH -> Direction.WEST;
                        case WEST -> Direction.SOUTH;
                    };
                    case '\\' -> dir = switch (dir) {
                        case NORTH -> Direction.WEST;
                        case WEST -> Direction.NORTH;
                        case SOUTH -> Direction.EAST;
                        case EAST -> Direction.SOUTH;
                    };
                    case '|' -> {
                        if (dir == Direction.EAST || dir == Direction.WEST) {
                            dir = Direction.NORTH;
                            stack.push(new PointDir(point, Direction.SOUTH));
                        }
                    }
                    case '-' -> {
                        if (dir == Direction.NORTH || dir == Direction.SOUTH) {
                            dir = Direction.EAST;
                            stack.push(new PointDir(point, Direction.WEST));
                        }
                    }
                }
                point = point.plus(dir.move);
            }

        }

        return Arrays.stream(stateMap)
                .flatMapToInt(Arrays::stream)
                .filter(value -> value != 0)
                .count();
    }

    public static Object part2(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);
        List<PointDir> starts = new ArrayList<>();
        for (int i = 0; i < map.length; i++) {
            starts.add(new PointDir(new Point(i, -1), Direction.EAST));
            starts.add(new PointDir(new Point(i, map[i].length), Direction.WEST));
        }
        for (int j = 0; j < map[0].length; j++) {
            starts.add(new PointDir(new Point(-1, j), Direction.SOUTH));
            starts.add(new PointDir(new Point(map.length, j), Direction.NORTH));
        }

        return starts.stream()
                .mapToLong(start -> energize(map, start))
                .max().orElseThrow();
    }
}
