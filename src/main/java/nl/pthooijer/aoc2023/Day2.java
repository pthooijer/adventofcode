package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.List;

import static nl.pthooijer.util.RegexUtils.parseRegex;

public class Day2 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/2/sample-input1.txt";
        String input = "aoc/2023/day/2/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 8): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 2286): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Round max = new Round(12, 13, 14);

        return ResourceHelper.getLinesStream(resource)
                .map(Day2::parseLine)
                .filter(game -> game.rounds().stream().allMatch(round ->
                        round.red() <= max.red() && round.green() <= max.green() && round.blue() <= max.blue()))
                .mapToLong(Game::id)
                .sum();
    }

    public static Object part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day2::parseLine)
                .mapToLong(Day2::power)
                .sum();
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(":;"));

    private static Game parseLine(String line) {
        List<String> roundStrings = SPLITTER.splitToList(line);

        long id = Long.parseLong(parseRegex(roundStrings.get(0), "Game ([0-9]+)").orElseThrow().group(1));

        List<Round> rounds = new ArrayList<>();
        for (int i = 1; i < roundStrings.size(); i++) {
            String round = roundStrings.get(i);

            long red = parseRegex(round, "^(.*[^0-9])?([0-9]+) red.*")
                    .map(result -> Long.parseLong(result.group(2))).orElse(0L);
            long green = parseRegex(round, "^(.*[^0-9])?([0-9]+) green.*")
                    .map(result -> Long.parseLong(result.group(2))).orElse(0L);
            long blue = parseRegex(round, "^(.*[^0-9])?([0-9]+) blue.*")
                    .map(result -> Long.parseLong(result.group(2))).orElse(0L);

            rounds.add(new Round(red, green, blue));
        }

        return new Game(id, rounds);
    }

    private static long power(Game game) {
        long maxRed = game.rounds().stream()
                .mapToLong(Round::red)
                .max().orElse(0L);
        long maxGreen = game.rounds().stream()
                .mapToLong(Round::green)
                .max().orElse(0L);
        long maxBlue = game.rounds().stream()
                .mapToLong(Round::blue)
                .max().orElse(0L);
        return maxRed * maxGreen * maxBlue;
    }

    private record Game(long id, List<Round> rounds) {
    }

    private record Round(long red, long green, long blue) {
    }
}
