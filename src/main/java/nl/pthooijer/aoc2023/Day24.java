package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day24 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/24/sample-input1.txt";
        String input = "aoc/2023/day/24/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 0): " + part1(sampleInput1, 7, 27));
        System.out.println("Part 1 answer: " + part1(input, 200000000000000L, 400000000000000L));

        System.out.println("Sample1 Part 2 answer (must be 47): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource, long minBox, long maxBox) {
        List<Hail> input = ResourceHelper.getLinesStream(resource)
                .map(Day24::parseLine)
                .toList();

        return input.stream()
                .flatMap(h1 -> input.stream()
                        .filter(h2 -> !h1.equals(h2))
                        .filter(h2 -> calculateIntersection(h1, h2, minBox, maxBox)))
                .count() / 2; // duplicates
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(" ,@")).omitEmptyStrings();

    private static Hail parseLine(String line) {
        long[] p = SPLITTER.splitToStream(line)
                .limit(3)
                .mapToLong(Long::parseLong)
                .toArray();
        long[] v = SPLITTER.splitToStream(line)
                .skip(3)
                .mapToLong(Long::parseLong)
                .toArray();
        return new Hail(p, v);
    }

    private static boolean calculateIntersection(Hail hail1, Hail hail2, long minBox, long maxBox) {
        double slope1 = (double) hail1.v[1] / (double) hail1.v[0];
        double slope2 = (double) hail2.v[1] / (double) hail2.v[0];
        if (slope1 == slope2) {
            return false;
        }

        double xIntersection = (slope2 * hail2.p[0] - slope1 * hail1.p[0] + hail1.p[1] - hail2.p[1]) /
                (slope2 - slope1);
        if (xIntersection < minBox || xIntersection > maxBox) {
            return false;
        }
        double yIntersection = slope1 * (xIntersection - hail1.p[0]) + hail1.p[1];
        if (yIntersection < minBox || yIntersection > maxBox) {
            return false;
        }

        boolean isFuture1 = isInFuture(hail1, xIntersection, yIntersection);
        boolean isFuture2 = isInFuture(hail2, xIntersection, yIntersection);
        return isFuture1 && isFuture2;
    }

    private static boolean isInFuture(Hail hail, double x, double y) {
        if (hail.v[0] < 0 && hail.p[0] < x) return false;
        if (hail.v[0] > 0 && hail.p[0] > x) return false;
        if (hail.v[1] < 0 && hail.p[1] < y) return false;
        if (hail.v[1] > 0 && hail.p[1] > y) return false;
        return true;
    }

    private record Hail(long[] p, long[] v) {
        @Override
        public String toString() {
            return "Hail{" +
                    "p=" + Arrays.toString(p) +
                    ", v=" + Arrays.toString(v) +
                    '}';
        }
    }

    public static Object part2(String resource) {
        List<Hail> input = ResourceHelper.getLinesStream(resource)
                .map(Day24::parseLine)
                .toList();

        // here is the query you can put in Wolfram alpha:
        String waQuery = IntStream.rangeClosed(0, 2) // this gives 3 * 3 = 9 equations, and we have 9 unknowns
                // (px, py, pz, vx, vy, vz; and with 3 hailstones we have t0, t1 and t2.
                .boxed()
                .flatMap(i -> IntStream.rangeClosed(0, 2)
                        .mapToObj(j -> String.format("p%d + t%d * v%d = %d + t%d * %d",
                                j, i, j, input.get(i).p[j], i, input.get(i).v[j])))
                .collect(Collectors.joining(", "));
        waQuery = "Solve " + waQuery + " for p0, p1, p2, v0, v1, v2, t0, t1, t2";

        // Wolfram alpha doesn't work, the query hit the character limit 😟

        // Same query for Mathematica
        String mathQuery = IntStream.rangeClosed(0, 2) // this gives 3 * 3 = 9 equations, and we have 9 unknowns
                // (px, py, pz, vx, vy, vz; and with 3 hailstones we have t0, t1 and t2.
                .boxed()
                .flatMap(i -> IntStream.rangeClosed(0, 2)
                        .mapToObj(j -> String.format("p%d + t%d * v%d == %d + t%d * %d",
                                j, i, j, input.get(i).p[j], i, input.get(i).v[j])))
                .collect(Collectors.joining(", "));
        mathQuery = "Solve[{" + mathQuery + "}, {p0, p1, p2, v0, v1, v2, t0, t1, t2}]";
        return mathQuery;
    }
}
