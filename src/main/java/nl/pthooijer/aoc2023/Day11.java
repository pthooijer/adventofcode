package nl.pthooijer.aoc2023;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day11 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/11/sample-input1.txt";
        String input = "aoc/2023/day/11/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 374): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);
        Empty empty = findEmpty(map);
        List<Point> galaxies = findGalaxies(map);

        return IntStream.range(0, galaxies.size() - 1)
                .mapToObj(n -> IntStream.range(n + 1, galaxies.size())
                        .mapToLong(m -> findDistance(galaxies.get(n), galaxies.get(m), empty, 2L)))
                .flatMapToLong(stream -> stream)
                .sum();
    }

    private static Empty findEmpty(char[][] map) {
        NavigableSet<Integer> rows = IntStream.range(0, map.length)
                .filter(i -> ArrayUtils.stream(map[i]).allMatch(c -> c == '.'))
                .boxed()
                .collect(Collectors.toCollection(TreeSet::new));

        NavigableSet<Integer> columns = new TreeSet<>();
        for(int j=0; j < map[0].length; j++) {
            boolean empty = true;
            for(int i=0; i < map.length; i++) {
                if (map[i][j] == '#') {
                    empty = false;
                    break;
                }
            }
            if(empty) {
                columns.add(j);
            }
        }

        return new Empty(rows, columns);
    }

    private static List<Point> findGalaxies(char[][] map) {
        return IntStream.range(0, map.length)
                .mapToObj(i -> IntStream.range(0, map[i].length)
                        .filter(j -> map[i][j] == '#')
                        .mapToObj(j -> new Point(i, j)))
                .flatMap(stream -> stream)
                .toList();
    }

    private static long findDistance(Point p1, Point p2, Empty empty, long scaleFactor) {
        long xDistance = findDistance(p1.x, p2.x, empty.rows(), scaleFactor);
        long yDistance = findDistance(p1.y, p2.y, empty.columns(), scaleFactor);

        return xDistance + yDistance;
    }

    private static long findDistance(int a, int b, NavigableSet<Integer> empties, long scaleFactor) {
        long distance = Math.abs(a - b);
        if (a < b) {
            distance += (scaleFactor - 1) * empties.subSet(a, true, b, true).size();
        } else if (a > b) {
            distance += (scaleFactor - 1) * empties.subSet(b, true, a, true).size();
        }
        return distance;
    }

    private record Empty(NavigableSet<Integer> rows, NavigableSet<Integer> columns) {
    }

    public static Object part2(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);
        Empty empty = findEmpty(map);
        List<Point> galaxies = findGalaxies(map);

        return IntStream.range(0, galaxies.size() - 1)
                .mapToObj(n -> IntStream.range(n + 1, galaxies.size())
                        .mapToLong(m -> findDistance(galaxies.get(n), galaxies.get(m), empty, 1_000_000L)))
                .flatMapToLong(stream -> stream)
                .sum();
    }
}
