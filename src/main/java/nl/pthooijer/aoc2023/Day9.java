package nl.pthooijer.aoc2023;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Day9 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/9/sample-input1.txt";
        String input = "aoc/2023/day/9/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 114): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 2): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return parseInput(resource).stream()
                .mapToLong(Day9::findNext)
                .sum();
    }

    private static final Splitter SPLITTER = Splitter.on(' ');

    private static List<long[]> parseInput(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(line -> SPLITTER.splitToStream(line)
                        .mapToLong(Long::parseLong)
                        .toArray())
                .toList();
    }

    private static long findNext(long[] input) {
        long[] currentArr = input;
        Deque<Long> lastNrs = new LinkedList<>();
        while (true) {
            lastNrs.addFirst(currentArr[currentArr.length - 1]);
            currentArr = getDifferences(currentArr);

            if (Arrays.stream(currentArr).allMatch(i -> i == 0)) {
                return lastNrs.stream()
                        .reduce(0L, (prev, next) -> next + prev);
            }
        }
    }

    private static long[] getDifferences(long[] arr) {
        long[] result = new long[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            result[i] = arr[i + 1] - arr[i];
        }
        return result;
    }

    public static Object part2(String resource) {
        return parseInput(resource).stream()
                .mapToLong(Day9::findPrevious)
                .sum();
    }

    private static long findPrevious(long[] input) {
        long[] currentArr = input;
        Deque<Long> firstNrs = new LinkedList<>();
        while (true) {
            firstNrs.addFirst(currentArr[0]);
            currentArr = getDifferences(currentArr);

            if (Arrays.stream(currentArr).allMatch(i -> i == 0)) {
                return firstNrs.stream()
                        .reduce(0L, (prev, next) -> next - prev);
            }
        }
    }
}
