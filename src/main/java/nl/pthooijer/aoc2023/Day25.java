package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import lombok.AllArgsConstructor;
import lombok.Value;
import nl.pthooijer.util.ResourceHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.random.RandomGenerator;

public class Day25 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/25/sample-input1.txt";
        String input = "aoc/2023/day/25/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 54): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be Happy Holidays): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<String> nodeList = parseNodes(resource);
        List<Edge> edgeList = parseEdges(resource);

        return kagerAlgorithm(nodeList, edgeList);
    }

    private static final RandomGenerator RANDOM = RandomGenerator.getDefault();

    public static int kagerAlgorithm(List<String> nodeList, List<Edge> edgeList) {
        while (true) {
            Map<String, Integer> nodeMap = new HashMap<>();
            nodeList.forEach(node -> nodeMap.put(node, 1));
            List<Edge> edges = new ArrayList<>(edgeList);
            while (nodeMap.size() > 2) {
                Edge edge = edges.remove(RANDOM.nextInt(edges.size()));
                List<Edge> newEdges = edges.stream()
                        .map(edge1 -> edge1.replaceNode(edge.from, edge.to))
                        .filter(edge1 -> !edge1.from.equals(edge1.to))
                        .toList();
                edges = new ArrayList<>(newEdges);

                Integer value = nodeMap.remove(edge.from);
                nodeMap.merge(edge.to, value, Integer::sum);
            }
            if (edges.size() <= 3) {
                return nodeMap.values().stream()
                        .reduce(1, (i, j) -> i * j);
            }
        }
    }

    private static final Splitter NODES_SPLITTER = Splitter.on(CharMatcher.anyOf(" :")).omitEmptyStrings();

    private static List<String> parseNodes(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .flatMap(NODES_SPLITTER::splitToStream)
                .distinct()
                .toList();
    }

    private static List<Edge> parseEdges(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(NODES_SPLITTER::splitToList)
                .flatMap(list -> list.stream()
                        .skip(1)
                        .map(n2 -> new Edge(list.get(0), n2)))
                .toList();
    }

    private record Edge(String from, String to) {
        public Edge replaceNode(String oldValue, String newValue) {
            String newFrom = oldValue.equals(from) ? newValue : from;
            String newTo = oldValue.equals(to) ? newValue : to;
            return new Edge(newFrom, newTo);
        }
    }

    public static Object part2(String resource) {
        return "Happy Holidays!";
    }
}
