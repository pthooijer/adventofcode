package nl.pthooijer.aoc2023;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import nl.pthooijer.util.JoinIterator;
import nl.pthooijer.util.SeparatorPolicy;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static nl.pthooijer.util.ResourceHelper.getLinesIt;

public class Day5 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/5/sample-input1.txt";
        String input = "aoc/2023/day/5/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 0): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 46): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Input input = parseInput(resource);

        return input.seeds().stream()
                .mapToLong(seed -> mapSeed(seed, input))
                .min().orElseThrow();
    }

    private static final Splitter SPLITTER = Splitter.on(' ');

    private static Input parseInput(String resource) {
        JoinIterator<String, ?, List<String>> joinIterator = new JoinIterator<>(getLinesIt(resource).iterator(),
                String::isEmpty, SeparatorPolicy.DROP, Collectors.toList());

        List<Long> seeds = SPLITTER.splitToStream(joinIterator.next().get(0))
                .skip(1) // == "seeds:"
                .map(Long::parseLong)
                .toList();

        var maps = Streams.stream(joinIterator)
                .map(section -> section.stream()
                        .skip(1) // == "x-to-y map:"
                        .map(Day5::parseLine)
                        .collect(Collectors.toMap(DestToSourceEntry::source, d -> d,
                                (u1, u2) -> u1, TreeMap::new)))
                .toList();

        return new Input(seeds, maps);
    }

    private static DestToSourceEntry parseLine(String line) {
        List<Long> nrs = SPLITTER.splitToStream(line)
                .map(Long::parseLong)
                .toList();

        return new DestToSourceEntry(nrs.get(0), nrs.get(1), nrs.get(2));
    }

    private static long mapSeed(long seed, Input input) {
        long currentId = seed;
        for (var map : input.maps()) {
            var entry = map.floorEntry(currentId);
            if (entry != null) {
                DestToSourceEntry dtsEntry = entry.getValue();
                if (currentId - dtsEntry.source() < dtsEntry.range()) {
                    currentId -= dtsEntry.source() - dtsEntry.destination();
                }
            }
        }
        return currentId;
    }

    public static Object part2(String resource) {
        Input input = parseInput(resource);

        // create a new map from Start of range to Offset
        List<NavigableMap<Long, Long>> offsetMaps = new ArrayList<>();
        for (var map : input.maps()) {
            NavigableMap<Long, Long> offsetMap = new TreeMap<>();

            // add the lowest range
            offsetMap.put(0L, 0L);
            for (var entry : map.entrySet()) {
                offsetMap.put(entry.getKey(), entry.getValue().offset());
                offsetMap.put(entry.getValue().sourceEnd() + 1, 0L); // after the range no offset. Ok if adjacent
            }
            offsetMaps.add(offsetMap);
        }

        List<Range> currentRanges = parseSeedRanges(input.seeds());
        for(var offsetMap : offsetMaps) {
            currentRanges = applyStep(currentRanges, offsetMap);
        }

        return currentRanges.stream()
                .mapToLong(Range::start)
                .min().orElseThrow();
    }

    private static List<Range> parseSeedRanges(List<Long> seeds) {
        List<Range> result = new ArrayList<>();
        for (int i = 0; i < seeds.size(); i += 2) {
            Range range = new Range(seeds.get(i), seeds.get(i + 1));
            result.add(range);
        }
        return result;
    }

    private static List<Range> applyStep(List<Range> ranges, NavigableMap<Long, Long> offsetMap) {
        List<Range> result = new ArrayList<>();

        // find all cutoff points in the range
        for (Range range : ranges) {
            var subMap = new TreeMap<>(offsetMap.subMap(range.start(), true, range.end(), true));

            // add the start of the range
            var floorEntry = offsetMap.floorEntry(range.start());
            subMap.put(range.start(), floorEntry.getValue());

            // apply all the mappings
            var entryList = new ArrayList<>(subMap.entrySet());
            for (int i = 0; i < entryList.size(); i++) {
                var entry = entryList.get(i);
                long length = 0;
                if (i == entryList.size() - 1) {
                    length = range.end() + 1 - entry.getKey();
                } else {
                    length = entryList.get(i + 1).getKey() - entry.getKey();
                }

                Range newRange = new Range(entry.getKey() + entry.getValue(), length);
                result.add(newRange);
            }
        }

        return result;
    }


    private record Input(List<Long> seeds, List<TreeMap<Long, DestToSourceEntry>> maps) {
    }

    private record DestToSourceEntry(long destination, long source, long range) {
        public long destEnd() {
            return destination + range - 1;
        }

        public long sourceEnd() {
            return source + range - 1;
        }

        public long offset() {
            return destination - source;
        }

    }

    private record Range(long start, long length) {
        public long end() {
            return start + length - 1;
        }
    }
}
