package nl.pthooijer.aoc2023;

import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Direction;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day23 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/23/sample-input1.txt";
        String input = "aoc/2023/day/23/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 94): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 154): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);

        Point start = new Point(0, 1);
        Point finish = new Point(map.length - 1, map.length - 2);

        List<Edge> edges = findEdges(map, start, finish, true);

        List<List<Edge>> finishPaths = findPaths(start, finish, edges);

        return finishPaths.stream()
                .mapToInt(list -> list.stream()
                        .mapToInt(Edge::length)
                        .sum())
                .max().orElseThrow();
    }

    private static List<Edge> findEdges(char[][] map, Point start, Point finish, boolean slopes) {
        List<Point> nodes = findAllIntersections(map);
        nodes.add(start);
        nodes.add(finish);

        List<Edge> edges = new ArrayList<>();
        for (Point node : nodes) {
            if (node.equals(finish)) {
                break;
            }

            List<Point> validNb = new ArrayList<>();
            for (Direction dir : Direction.values()) {
                Point nb = node.plus(dir.move);
                if (ArrayUtils.isInBounds(map, nb.x, nb.y)) {
                    char c = map[nb.x][nb.y];
                    if (c != '#') {
                        Direction slope = SLOPE_MAP.get(c);
                        if (!slopes || (slope == null || slope == dir)) {
                            validNb.add(nb);
                        }
                    }
                }
            }

            for (Point p2 : validNb) {
                int length = 1;
                Point cur = p2;
                Point prev = node;
                while (!nodes.contains(cur)) {
                    boolean changed = false;
                    for (Direction dir : Direction.values()) {
                        Point nb = cur.plus(dir.move);
                        if (nb.equals(prev)) {
                            continue;
                        }
                        char c = map[nb.x][nb.y];
                        if (c == '#') {
                            continue;
                        }
                        Direction slope = SLOPE_MAP.get(c);
                        if (slopes && slope != null && slope != dir) {
                            continue;
                        }
                        prev = cur;
                        cur = nb;
                        length++;
                        changed = true;
                        break;
                    }
                    if (!changed) {
                        break;
                    }
                }
                Edge edge = new Edge(node, cur, length);
                edges.add(edge);
            }
        }
        return edges;
    }

    private static List<List<Edge>> findPaths(Point start, Point finish, List<Edge> edges) {
        //DFT
        Map<Point, List<Edge>> fromNodeMap = edges.stream()
                .collect(Collectors.groupingBy(Edge::from));
        Deque<List<Edge>> deque = new LinkedList<>();
        deque.add(Collections.emptyList());
        List<List<Edge>> finishPaths = new ArrayList<>();
        while (!deque.isEmpty()) {
            List<Edge> edgePath = deque.pop();
            Point node = edgePath.isEmpty() ? start : edgePath.get(edgePath.size() - 1).to;
            for (Edge edge : fromNodeMap.get(node)) {
                if (!edgePath.contains(edge)) {
                    boolean nodeNotYetVisited = edgePath.stream()
                            .map(Edge::from)
                            .noneMatch(node2 -> node2.equals(edge.to));
                    if (nodeNotYetVisited) {
                        List<Edge> newEdgePath = new ArrayList<>(edgePath);
                        newEdgePath.add(edge);
                        if (edge.to.equals(finish)) {
                            finishPaths.add(newEdgePath);
                        } else {
                            deque.add(newEdgePath);
                        }
                    }
                }
            }
        }
        return finishPaths;
    }

    private static final Map<Character, Direction> SLOPE_MAP = Map.of(
            '<', Direction.WEST,
            '>', Direction.EAST,
            '^', Direction.NORTH,
            'v', Direction.SOUTH);

    private static List<Point> findAllIntersections(char[][] map) {
        List<Point> intersections = new ArrayList<>();
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                if (map[i][j] == '.') {
                    Point p = new Point(i, j);

                    List<Point> nbList = new ArrayList<>();
                    for (Direction direction : Direction.values()) {
                        Point nb = p.plus(direction.move);
                        if (ArrayUtils.isInBounds(map, nb.x, nb.y) && map[nb.x][nb.y] != '#') {
                            nbList.add(nb);
                        }
                    }

                    if (nbList.size() > 2) {
                        intersections.add(p);
                    }
                }
            }
        }

        return intersections;
    }

    private record Edge(Point from, Point to, int length) {
    }

    public static Object part2(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);

        Point start = new Point(0, 1);
        Point finish = new Point(map.length - 1, map.length - 2);

        List<Edge> edges = findEdges(map, start, finish, false);

        List<List<Edge>> finishPaths = findPaths(start, finish, edges);

        return finishPaths.stream()
                .mapToInt(list -> list.stream()
                        .mapToInt(Edge::length)
                        .sum())
                .max().orElseThrow();
    }
}
