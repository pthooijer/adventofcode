package nl.pthooijer.aoc2023;

public class Day6 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/6/sample-input1.txt";
        String input = "aoc/2023/day/6/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 0): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 0): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return "Pen and paper: 211904";
    }

    private static double abcFormulaPlus(double a, double b, double c) {
        return (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
    }

    private static double abcFormulaMinus(double a, double b, double c) {
        return (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
    }

    public static Object part2(String resource) {
        return "Pen and paper: 43364472";
    }
}
