package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.StreamUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day15 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/15/sample-input1.txt";
        String input = "aoc/2023/day/15/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 1320): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 145): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return SPLITTER.splitToStream(ResourceHelper.getLinesList(resource).get(0))
                .mapToInt(Day15::hash)
                .sum();
    }

    private static final Splitter SPLITTER = Splitter.on(',');
    private static final Splitter LABEL_SPLITTER = Splitter.on(CharMatcher.anyOf("=-")).omitEmptyStrings();

    private static int hash(String s) {
        return s.codePoints()
                .reduce(0, (res, i) -> ((res + i) * 17) % 256);
    }

    public static Object part2(String resource) {
        var map1 = SPLITTER.splitToStream(ResourceHelper.getLinesList(resource).get(0))
                .map(Day15::parseInstruction)
                .collect(Collectors.groupingBy(Instruction::hash,
                        Collector.of(LinkedHashMap<String, Integer>::new,
                                (map, instr) -> {
                                    if (instr.isSubtract()) {
                                        map.remove(instr.label());
                                    } else {
                                        map.put(instr.label(), instr.lens());
                                    }
                                },
                                StreamUtils.throwingMerger())));


        return map1.entrySet().stream()
                .flatMapToInt(entry -> {
                    List<Integer> lensList = new ArrayList<>(entry.getValue().values());
                    return IntStream.range(0, lensList.size())
                            .map(i -> (entry.getKey() + 1) * (i + 1) * lensList.get(i));
                })
                .sum();
    }

    private static Instruction parseInstruction(String instruction) {
        List<String> parts = LABEL_SPLITTER.splitToList(instruction);
        Integer lens = parts.size() > 1 ? Integer.parseInt(parts.get(1)) : null;
        return new Instruction(parts.get(0), lens, hash(parts.get(0)));
    }

    private record Instruction(String label, Integer lens, int hash) {
        private boolean isSubtract() {
            return lens == null;
        }
    }
}
