package nl.pthooijer.aoc2023;

import nl.pthooijer.util.AStarPathfinding;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.Direction;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Day17 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/17/sample-input1.txt";
        String input = "aoc/2023/day/17/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 102): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 94): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        long[][] map = ResourceHelper.getLongs2dArray(resource);

        Pathfinding1 pathfinding = new Pathfinding1(map);

        State1 start = new State1(0, 0, 0, null);
        AStarPathfinding.Result<State1> result = pathfinding.aStartAlgorithm(start);
        return result.cost();
    }

    private record State1(int i, int j, int straight, Direction direction) {

        public Point getPoint() {
            return new Point(i, j);
        }
    }

    private static class Pathfinding1 extends AStarPathfinding<State1> {

        private final long[][] map;
        private final Point goal;

        public Pathfinding1(long[][] map) {
            this.map = map;
            goal = new Point(map.length - 1, map.length - 1);
        }

        @Override
        public boolean isGoal(State1 node) {
            return goal.equals(node.getPoint());
        }

        @Override
        public Collection<NeighbourCost<State1>> getNeighboursAndCost(State1 node) {
            List<NeighbourCost<State1>> neighbours = new ArrayList<>();
            for (Direction newDir : Direction.values()) {
                if (node.direction == null || newDir != node.direction.getOpposite()) {
                    Point p = node.getPoint().plus(newDir.move);
                    if (ArrayUtils.isInBounds(map, p.x, p.y)) {
                        int straight = newDir == node.direction ? node.straight + 1 : 1;
                        if (straight <= 3) {
                            State1 alt = new State1(p.x, p.y, straight, newDir);
                            long cost = map[p.x][p.y];
                            neighbours.add(new NeighbourCost<>(alt, cost));
                        }
                    }
                }
            }
            return neighbours;
        }

        @Override
        public long heuristicToGoal(State1 fromNode) {
            return fromNode.getPoint().minus(goal).taxiMagnitude();
        }
    }

    // === Part 2 ===

    public static Object part2(String resource) {
        long[][] map = ResourceHelper.getLongs2dArray(resource);

        Pathfinding2 pathfinding = new Pathfinding2(map);

        State2 start = new State2(0, 0, null);
        AStarPathfinding.Result<State2> result = pathfinding.aStartAlgorithm(start);
        return result.cost();
    }

    private record State2(int i, int j, Direction direction) {

        public Point getPoint() {
            return new Point(i, j);
        }
    }

    private static class Pathfinding2 extends AStarPathfinding<State2> {

        private final long[][] map;
        private final Point goal;

        public Pathfinding2(long[][] map) {
            this.map = map;
            goal = new Point(map.length - 1, map.length - 1);
        }

        @Override
        public boolean isGoal(State2 node) {
            return goal.equals(node.getPoint());
        }

        @Override
        public Collection<NeighbourCost<State2>> getNeighboursAndCost(State2 node) {
            List<NeighbourCost<State2>> neighbours = new ArrayList<>();
            for (Direction newDir : Direction.values()) {
                long cost = 0L;
                if (node.direction == null || (newDir != node.direction.getOpposite() && newDir != node.direction)) {
                    for (int steps = 1; steps <= 10; steps++) {
                        Point p = node.getPoint().plus(newDir.move.times(steps));
                        if (ArrayUtils.isInBounds(map, p.x, p.y)) {
                            cost += map[p.x][p.y];
                            if (steps >= 4) {
                                State2 alt = new State2(p.x, p.y, newDir);
                                neighbours.add(new NeighbourCost<>(alt, cost));
                            }
                        } else {
                            break;
                        }
                    }
                }

            }
            return neighbours;
        }

        @Override
        public long heuristicToGoal(State2 fromNode) {
            return fromNode.getPoint().minus(goal).taxiMagnitude();
        }
    }
}
