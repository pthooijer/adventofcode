package nl.pthooijer.aoc2023;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day12 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/12/sample-input1.txt";
        String input = "aoc/2023/day/12/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 21): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 525152): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day12::parseRow)
                .mapToLong(row -> findArrangementsRec(new State(row, 0, 0, 0), new HashMap<>()))
                .sum();
    }

    private static final Splitter SPACE_SPLITTER = Splitter.on(' ');
    private static final Splitter COMMA_SPLITTER = Splitter.on(',');

    private static Row parseRow(String line) {
        List<String> parts = SPACE_SPLITTER.splitToList(line);

        char[] conditions = parts.get(0).toCharArray();
        List<Integer> groups = COMMA_SPLITTER.splitToStream(parts.get(1))
                .mapToInt(Integer::parseInt)
                .boxed()
                .toList();

        return new Row(conditions, groups);
    }

    private static long findArrangements(Row row) {
        Set<Integer> unknownPos = IntStream.range(0, row.conditions().length)
                .filter(i -> row.conditions()[i] == '?')
                .boxed()
                .collect(Collectors.toSet());

        return Sets.powerSet(unknownPos).stream()
                .filter(permutation -> checkValidRow(row, unknownPos, permutation))
                .count();
    }

    private static final Splitter DOT_SPLITTER = Splitter.on('.').omitEmptyStrings();

    private static boolean checkValidRow(Row row, Set<Integer> unknownPos, Set<Integer> permutation) {
        char[] filledRow = Arrays.copyOf(row.conditions(), row.conditions().length);
        for (int i : unknownPos) {
            filledRow[i] = permutation.contains(i) ? '#' : '.';
        }

        List<Integer> groups = DOT_SPLITTER.splitToStream(new String(filledRow))
                .map(String::length)
                .toList();

        return row.groups().equals(groups);
    }

    private record State(Row row, int rowIndex, int groupIndex, int currentChainLength) {
    }

    private static long findArrangementsRec(State state, Map<State, Long> cache) {
        if (!cache.containsKey(state)) {

            long result;
            if (state.rowIndex > state.row.conditions().length) {
                result = state.groupIndex != state.row.groups().size() ? 0 : 1; // check if all groups have passed
            } else if (state.rowIndex == state.row.conditions().length) { // reaching the end, add an extra dot
                result = processDot(state, cache);
            } else {
                result = switch (state.row.conditions()[state.rowIndex]) {
                    case '.' -> processDot(state, cache);
                    case '#' -> processHash(state, cache);
                    case '?' -> processDot(state, cache) +
                            processHash(state, cache);
                    default -> throw new IllegalArgumentException();
                };
            }
            cache.put(state, result);
        }
        return cache.get(state);
    }


    private static long processDot(State state, Map<State, Long> cache) {
        if (state.currentChainLength != 0) { // end of chain
            if (state.currentChainLength == state.row.groups().get(state.groupIndex)) { // valid chain
                State newState = new State(state.row, state.rowIndex + 1,
                        state.groupIndex + 1, 0);
                return findArrangementsRec(newState, cache);
            } else {
                return 0;
            }
        } else {
            State newState = new State(state.row, state.rowIndex + 1,
                    state.groupIndex, state.currentChainLength);
            return findArrangementsRec(newState, cache);
        }
    }

    private static long processHash(State state, Map<State, Long> cache) {
        if (state.groupIndex >= state.row.groups().size() ||
                state.currentChainLength > state.row.groups().get(state.groupIndex)) {
            return 0; // too many groups OR chain too long
        } else {
            State newState = new State(state.row, state.rowIndex + 1,
                    state.groupIndex, state.currentChainLength + 1);
            return findArrangementsRec(newState, cache);
        }
    }

    private record Row(char[] conditions, List<Integer> groups) {

        @Override
        public String toString() {
            return "Row{" +
                    "conditions=" + Arrays.toString(conditions) +
                    ", groups=" + groups +
                    '}';
        }
    }

    public static Object part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day12::parseRow)
                .map(Day12::expandRow)
                .mapToLong(row -> findArrangementsRec(new State(row, 0, 0, 0), new HashMap<>()))
                .sum();
    }

    private static Row expandRow(Row row) {
        int copies = 5;
        char[] newConditions = new char[row.conditions().length * copies + (copies - 1)];
        for (int i = 0; i < copies; i++) {
            System.arraycopy(row.conditions(), 0,
                    newConditions, (row.conditions().length + 1) * i,
                    row.conditions().length);
        }

        for (int i = 1; i < copies; i++) {
            newConditions[(row.conditions().length + 1) * i - 1] = '?';
        }

        List<Integer> newGroups = IntStream.range(0, 5)
                .mapToObj(i -> row.groups())
                .flatMap(Collection::stream)
                .toList();

        return new Row(newConditions, newGroups);
    }
}
