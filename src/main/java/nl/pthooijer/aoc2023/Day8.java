package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import nl.pthooijer.util.MathUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day8 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/8/sample-input1.txt";
        String sampleInput2 = "aoc/2023/day/8/sample-input2.txt";
        String input = "aoc/2023/day/8/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 2): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample2 Part 2 answer (must be 6): " + part2(sampleInput2));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        Input input = parseInput(resource);

        return findTarget("AAA", "ZZZ", input);
    }

    private static long findTarget(String startNode, String targetEndOfString, Input input) {
        long instrSize = input.instructions().length();
        String currentNode = startNode;
        long step = 0;
        while (true) {
            Node node = input.nodeMap().get(currentNode);
            char direction = input.instructions().charAt((int) (step % instrSize));

            currentNode = direction == 'L' ? node.left() : node.right();
            step++;

            if(currentNode.endsWith(targetEndOfString)) {
                return step;
            }
        }
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf("= (,)")).omitEmptyStrings();

    private static Input parseInput(String resource) {
        Iterator<String> iterator = ResourceHelper.getLinesIt(resource).iterator();

        String instructions = iterator.next();
        iterator.next();
        Map<String, Node> nodeMap = Streams.stream(iterator)
                .map(Day8::parseLine)
                .collect(Collectors.toMap(Node::id, node -> node));

        return new Input(instructions, nodeMap);
    }

    private static Node parseLine(String line) {
        List<String> parts = SPLITTER.splitToList(line);
        return new Node(parts.get(0), parts.get(1), parts.get(2));
    }

    private record Input(String instructions, Map<String, Node> nodeMap) {
    }

    private record Node(String id, String left, String right) {}

    public static Object part2(String resource) {
        Input input = parseInput(resource);

        return input.nodeMap.values().stream()
                .filter(node -> node.id().charAt(2) == 'A')
                .mapToLong(node -> findTarget(node.id(), "Z", input))
                .reduce(MathUtils::lcm) // This only works because after you reach ??Z the first time, you return to your start position. I was lucky
                .orElseThrow();
    }
}
