package nl.pthooijer.aoc2023;

import com.google.common.base.Splitter;
import nl.pthooijer.util.ArrayUtils;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day14 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/14/sample-input1.txt";
        String input = "aoc/2023/day/14/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 136): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 64): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);

        char[][] tMap = ArrayUtils.rotateCW(map);
        char[][] tiltedMap = tilt(tMap);
        return calculateWeight(tiltedMap);
    }

    private static final Splitter HASH_SPLITTER = Splitter.on('#');

    private static char[][] tilt(char[][] map) {
        return Arrays.stream(map)
                .map(row -> HASH_SPLITTER.splitToStream(new String(row))
                        .map(section -> section.codePoints()
                                .sorted()
                                .collect(StringBuilder::new,
                                        StringBuilder::appendCodePoint,
                                        StringBuilder::append))
                        .collect(Collectors.joining("#"))
                        .toCharArray())
                .toArray(char[][]::new);
    }

    private static long calculateWeight(char[][] mapWithNorthRight) {
        return Arrays.stream(mapWithNorthRight)
                .flatMapToInt(row -> IntStream.range(0, row.length)
                        .filter(i -> row[i] == 'O')
                        .map(i -> i + 1))
                .sum();
    }

    private static char[][] doStep(char[][] map) {
        char[][] newMap = map;
        for (int i = 0; i < 4; i++) {
            newMap = tilt(newMap);
            newMap = ArrayUtils.rotateCW(newMap);
        }
        return newMap;
    }


    public static Object part2(String resource) {
        char[][] map = ResourceHelper.getChars2dArray(resource);
        char[][] newMap = ArrayUtils.rotateCW(map);

        Map<MapKey, Long> firstOccurenceMap = new HashMap<>();
        for (long i = 0; i < 1_000_000_000L; i++) {
            MapKey mapKey = new MapKey(newMap);
            if (firstOccurenceMap.containsKey(mapKey)) {
                // loop found!
                long loopStart = firstOccurenceMap.get(mapKey);
                long loopLength = i - loopStart;
                long indexToSearch = ((1_000_000_000L - loopStart) % loopLength) + loopStart;
                return firstOccurenceMap.entrySet().stream()
                        .filter(entry -> entry.getValue() == indexToSearch)
                        .findFirst()
                        .map(entry -> calculateWeight(entry.getKey().map()))
                        .orElseThrow();
            }
            firstOccurenceMap.put(mapKey, i);
            newMap = doStep(newMap);
        }

        return null;
    }

    // because arrays cannot have equals and hashcode besides identity
    private record MapKey(char[][] map) {

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MapKey mapKey = (MapKey) o;

            return Arrays.deepEquals(map, mapKey.map);
        }

        @Override
        public int hashCode() {
            return Arrays.deepHashCode(map);
        }
    }
}
