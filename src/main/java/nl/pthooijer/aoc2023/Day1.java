package nl.pthooijer.aoc2023;

import nl.pthooijer.util.ResourceHelper;

public class Day1 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/1/sample-input1.txt";
        String input = "aoc/2023/day/1/input.txt";

       //  System.out.println("Sample1 Part 1 answer (must be 0): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 281): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(line -> line.replaceAll("[A-Za-z]", ""))
                .map(line -> "" + line.charAt(0) + line.charAt(line.length() - 1))
                .mapToLong(Long::valueOf)
                .sum();
    }

    public static Object part2(String resource) {
        return ResourceHelper.getLinesStream(resource)
                .map(Day1::replace)
                .map(line -> "" + line.charAt(0) + line.charAt(line.length() - 1))
                .mapToLong(Long::valueOf)
                .sum();
    }

    private static final String[] NUMBERS = new String[]{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    private static String replace(String line) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (c >= '0' && c <= '9') {
                result.append(c);
            } else {
                for (int n = 1; n <= 9; n++) {
                    String number = NUMBERS[n - 1];
                    if (line.substring(i).startsWith(number)) {
                        result.append(n);
                    }
                }
            }
        }

        return result.toString();
    }
}
