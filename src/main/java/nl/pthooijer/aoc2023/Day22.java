package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.ResourceHelper;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day22 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/22/sample-input1.txt";
        String input = "aoc/2023/day/22/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 5): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 7): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Brick> snapshot = ResourceHelper.getLinesStream(resource)
                .map(Day22::parseLine)
                .sorted(Comparator.comparingInt(brick -> Math.min(brick.from[2], brick.to[2])))
                .toList();

        Map<Integer, Set<Integer>> supportMap = calculateSupportMap(snapshot);

        // the support bricks are the ones that are not the sole element of the set
        Set<Integer> onlySupports = supportMap.values().stream()
                .filter(set -> set.size() == 1)
                .map(set -> set.iterator().next())
                .collect(Collectors.toSet());
        Set<Integer> hasDuplicateSupports = new HashSet<>(supportMap.keySet());
        hasDuplicateSupports.removeAll(onlySupports);

        return hasDuplicateSupports.size();
    }

    private static Map<Integer, Set<Integer>> calculateSupportMap(List<Brick> snapshot) {
        int maxX = snapshot.stream()
                .flatMapToInt(brick -> IntStream.of(brick.from[0], brick.to[0]))
                .max().orElseThrow();
        int maxY = snapshot.stream()
                .flatMapToInt(brick -> IntStream.of(brick.from[1], brick.to[1]))
                .max().orElseThrow();

        int[][] heightMap = IntStream.rangeClosed(0, maxX)
                .mapToObj(i -> IntStream.rangeClosed(0, maxY)
                        .map(j -> 0)
                        .toArray())
                .toArray(int[][]::new);
        int[][] brickMap = IntStream.rangeClosed(0, maxX)
                .mapToObj(i -> IntStream.rangeClosed(0, maxY)
                        .map(j -> -1)
                        .toArray())
                .toArray(int[][]::new);

        Map<Integer, Set<Integer>> supportMap = new TreeMap<>();

        for (int n = 0; n < snapshot.size(); n++) {
            Brick brick = snapshot.get(n);
            switch (brick.dir) {
                case 0 -> {
                    int fromX = Math.min(brick.from[0], brick.to[0]);
                    int toX = Math.max(brick.from[0], brick.to[0]);
                    int y = brick.from[1];
                    int maxHeightX = IntStream.rangeClosed(fromX, toX)
                            .map(i -> heightMap[i][y])
                            .max().orElseThrow();

                    Set<Integer> supports = new HashSet<>();
                    for (int i = fromX; i <= toX; i++) {
                        if (heightMap[i][y] == maxHeightX) {
                            supports.add(brickMap[i][y]);
                        }
                        heightMap[i][y] = maxHeightX + 1;
                        brickMap[i][y] = n;
                    }
                    supportMap.put(n, supports);
                }
                case 1 -> {
                    int fromY = Math.min(brick.from[1], brick.to[1]);
                    int toY = Math.max(brick.from[1], brick.to[1]);
                    int x = brick.from[0];
                    int maxHeightY = IntStream.rangeClosed(fromY, toY)
                            .map(j -> heightMap[x][j])
                            .max().orElseThrow();

                    Set<Integer> supports = new HashSet<>();
                    for (int j = fromY; j <= toY; j++) {
                        if (heightMap[x][j] == maxHeightY) {
                            supports.add(brickMap[x][j]);
                        }
                        heightMap[x][j] = maxHeightY + 1;
                        brickMap[x][j] = n;
                    }
                    supportMap.put(n, supports);
                }
                case 2 -> {
                    int length = Math.abs(brick.from[2] - brick.to[2]) + 1;
                    int x = brick.from[0];
                    int y = brick.from[1];

                    Set<Integer> supports = Set.of(brickMap[x][y]);
                    heightMap[x][y] += length;
                    brickMap[x][y] = n;
                    supportMap.put(n, supports);
                }
                default -> throw new IllegalArgumentException(brick.toString());
            }
        }
        return supportMap;
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(",~"));

    private static Brick parseLine(String line) {
        List<String> parts = SPLITTER.splitToList(line);
        int[] from = parts.stream()
                .limit(3)
                .mapToInt(Integer::parseInt)
                .toArray();
        int[] to = parts.stream()
                .skip(3)
                .mapToInt(Integer::parseInt)
                .toArray();

        int dir = from[0] != to[0] ? 0 : from[1] != to[1] ? 1 : 2;
        return new Brick(from, to, dir);
    }

    private record Brick(int[] from, int[] to, int dir) {

        @Override
        public String toString() {
            return "Brick{" +
                    "from=" + Arrays.toString(from) +
                    ", to=" + Arrays.toString(to) +
                    ", dir=" + dir +
                    '}';
        }
    }

    public static Object part2(String resource) {
        List<Brick> snapshot = ResourceHelper.getLinesStream(resource)
                .map(Day22::parseLine)
                .sorted(Comparator.comparingInt(brick -> Math.min(brick.from[2], brick.to[2])))
                .toList();

        Map<Integer, Set<Integer>> supportMap = calculateSupportMap(snapshot);

        // instead get all bricks that are the sole support
        List<Integer> onlySupports = supportMap.values().stream()
                .filter(set -> set.size() == 1)
                .map(set -> set.iterator().next())
                .filter(i -> i != -1) // ignore the ground
                .sorted(Comparator.<Integer>naturalOrder().reversed()) // highest block first
                .distinct()
                .toList();

        int counter = 0;
        for (int n : onlySupports) {
            Map<Integer, Set<Integer>> supportMapCopy = supportMap.entrySet().stream()
                    .map(entry -> Map.entry(entry.getKey(), new HashSet<>(entry.getValue())))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            Deque<Integer> fallingBlocksLifo = new LinkedList<>();
            fallingBlocksLifo.addLast(n);
            while (!fallingBlocksLifo.isEmpty()) {
                Integer block = fallingBlocksLifo.pollFirst();
                for (var supportsEntry : supportMapCopy.entrySet()) {
                    if (supportsEntry.getValue().remove(block) && supportsEntry.getValue().isEmpty()) {
                        fallingBlocksLifo.addLast(supportsEntry.getKey());
                        counter++;
                    }
                }
            }
        }

        return counter;
    }
}
