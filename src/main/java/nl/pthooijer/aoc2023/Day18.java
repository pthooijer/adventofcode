package nl.pthooijer.aoc2023;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import nl.pthooijer.util.Direction;
import nl.pthooijer.util.ResourceHelper;
import nl.pthooijer.util.point.Point;

import java.util.HexFormat;
import java.util.List;

public class Day18 {

    public static void main(String[] args) {
        String sampleInput1 = "aoc/2023/day/18/sample-input1.txt";
        String input = "aoc/2023/day/18/input.txt";

        System.out.println("Sample1 Part 1 answer (must be 62): " + part1(sampleInput1));
        System.out.println("Part 1 answer: " + part1(input));

        System.out.println("Sample1 Part 2 answer (must be 952408144115): " + part2(sampleInput1));
        System.out.println("Part 2 answer: " + part2(input));
    }

    public static Object part1(String resource) {
        List<Instruction> inputList = ResourceHelper.getLinesStream(resource)
                .map(Day18::parseLine1)
                .toList();
        return calculateWithTriangles(inputList);
    }

    private static final Splitter SPLITTER = Splitter.on(CharMatcher.anyOf(" ()#")).omitEmptyStrings();

    private record Instruction(Direction direction, int amount) {
    }

    private static Instruction parseLine1(String line) {
        List<String> parts = SPLITTER.splitToList(line);
        Direction direction = switch (parts.get(0).charAt(0)) {
            case 'U' -> Direction.NORTH;
            case 'D' -> Direction.SOUTH;
            case 'R' -> Direction.EAST;
            case 'L' -> Direction.WEST;
            default -> throw new IllegalArgumentException();
        };

        return new Instruction(direction, Integer.parseInt(parts.get(1)));
    }

    private static Instruction parseLine2(String line) {
        String color = SPLITTER.splitToList(line).get(2);
        Direction direction = switch (color.charAt(5)) {
            case '0' -> Direction.EAST;
            case '1' -> Direction.SOUTH;
            case '2' -> Direction.WEST;
            case '3' -> Direction.NORTH;
            default -> throw new IllegalArgumentException(color);
        };
        int amount = HexFormat.fromHexDigits(color, 0, 5);
        return new Instruction(direction, amount);
    }

    private static long calculateWithTriangles(List<Instruction> inputList) {
        Point cur = new Point(0, 0);
        long triangleTimes2 = 0L;
        long circumference = 0L;

        for (Instruction instr : inputList) {
            Point next = cur.plus(instr.direction.move.times(instr.amount));
            triangleTimes2 += (long) next.x * (long) cur.y - (long) cur.x * (long) next.y;
            circumference += instr.amount;
            cur = next;
        }

        return (triangleTimes2 + circumference) / 2 + 1;
    }

    public static Object part2(String resource) {
        List<Instruction> inputList = ResourceHelper.getLinesStream(resource)
                .map(Day18::parseLine2)
                .toList();
        return calculateWithTriangles(inputList);
    }
}
