package nl.pthooijer.util;

import org.junit.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import static org.assertj.core.api.Assertions.*;

public class JoinIteratorTest {

    private static List<String> applyJoinIterator(SeparatorPolicy separatorPolicy) {
        List<String> testSet = Arrays.asList("a1", "a2", "break", "b", "break", "c1", "c2", "c3");
        Predicate<String> separatorTest = "break"::equals;

        JoinIterator<String, ?, String> joinIterator = new JoinIterator<>(testSet.iterator(), separatorTest,
                separatorPolicy, Collectors.joining(";"));
        List<String> result = new ArrayList<>();
        joinIterator.forEachRemaining(result::add);
        return result;
    }

    @Test
    public void shouldJoinWithPrevious() {
        List<String> result = applyJoinIterator(SeparatorPolicy.JOIN_WITH_PREVIOUS);
        assertThat(result).containsExactly("a1;a2;break", "b;break", "c1;c2;c3");
    }

    @Test
    public void shouldJoinWithNext() {
        List<String> result = applyJoinIterator(SeparatorPolicy.JOIN_WITH_NEXT);
        assertThat(result).containsExactly("a1;a2", "break;b", "break;c1;c2;c3");
    }

    @Test
    public void shouldDrop() {
        List<String> result = applyJoinIterator(SeparatorPolicy.DROP);
        assertThat(result).containsExactly("a1;a2", "b", "c1;c2;c3");
    }

}